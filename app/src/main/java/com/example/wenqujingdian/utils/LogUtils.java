package com.example.wenqujingdian.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.RandomAccessFile;

/**
 * Created by to-explore-future on 2018/10/20
 */
public class LogUtils {
    // 生成日志 打印日志 log TxT 专用
    public static void printLogMsg(String jsonObj) {

        String filePath = Environment.getExternalStorageDirectory().getPath() + "/wenQuBookLog";
//        String filePath = this.getExternalStorageDirectory();

        String fileName = "USBInfoLog" + ".txt";

        writeTxtToFile(jsonObj + "\r\n\r\n", filePath, fileName);


    }

    // 将字符串写入到文本文件中
    public static void writeTxtToFile(String strcontent, String filePath, String fileName) {
        //生成文件夹之后，再生成文件，不然会出错
        makeFilePath(filePath, fileName);

        String strFilePath = filePath + fileName;
        // 每次写入时，都换行写
        String strContent = strcontent + "\r\n";
        try {
            File file = new File(strFilePath);
            if (!file.exists()) {
                Log.d("TestFile", "Create the file:" + strFilePath);
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            RandomAccessFile raf = new RandomAccessFile(file, "rwd");
            raf.seek(file.length());
            raf.write(strContent.getBytes());
            raf.close();
        } catch (Exception e) {
            Log.e("TestFile", "Error on write File:" + e);
        }
    }

    // 生成文件
    public static File makeFilePath(String filePath, String fileName) {
        File file = null;
        makeRootDirectory(filePath);
        try {
            file = new File(filePath + fileName);
            if (!file.exists()) {
                boolean newFile = file.createNewFile();
                Log.d("newFile", newFile + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    // 生成文件夹
    public static void makeRootDirectory(String filePath) {
        File file = null;
        try {
            file = new File(filePath);
            if (!file.exists()) {
                boolean mkdir = file.mkdirs();
                Log.d("mkdir", mkdir + "");
            }
        } catch (Exception e) {
            Log.i("error:", e + "");
        }
    }
}

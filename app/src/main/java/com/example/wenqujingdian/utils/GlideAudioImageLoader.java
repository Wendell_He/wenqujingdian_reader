package com.example.wenqujingdian.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.youth.banner.loader.ImageLoader;

import java.io.File;

/**
 * Create by kxliu on 2018/11/22
 */
public class GlideAudioImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        if (NetWorkUtils.isNetworkAvailable(context)) {
            Glide.with(context).load(path).into(imageView);
        } else {
            Glide.with(context).load(new File(path + "")).fitCenter().into(imageView);
        }
    }
}

package com.example.wenqujingdian.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Create by kxliu on 2018/11/18
 */
public class SPUtils {
    private static final String S_PREFS_NAME = "syncState";
    private static final String S_PREFS_STATE = "state";
    private static SharedPreferences getSp(Context context) {
        return context.getSharedPreferences(S_PREFS_NAME, Context.MODE_PRIVATE);
    }
    public static boolean isSync(Context context) {
        return getSp(context).getBoolean(S_PREFS_STATE, false);
    }
    public static boolean setSyncState(Context context, boolean value) {
        return getSp(context).edit().putBoolean(S_PREFS_STATE, value).commit();
    }

}

package com.example.wenqujingdian.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

/**
 * Created by 29083 on 2018/3/30.
 */

public class VersionUtils {

    /**
     * 获取应用程序版本名称信息
     *
     * @param context 上下文
     * @return 当前应用的版本名称（失败返回null）
     */
    public static String getVersionName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            return packageInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获得版本更新的内容
     *
     * @param message 请求回来的版本实体类中的更新消息
     * @return dialog内容
     */
    public static String dialogContent(String message) {
        StringBuilder msg = new StringBuilder();
        msg.append("\r发现最新版本:\n");
        if (TextUtils.isEmpty(message))return msg.toString();
        String[] infoArr = message.trim().split("；");
        for (int i = 0; i < infoArr.length; i++) {
            msg.append(infoArr[i] + "\n");
        }
        return msg.toString();
    }

}

package com.example.wenqujingdian;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyViewPager extends ViewPager {

	private boolean enable;

	public MyViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.enable = false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		if (enable) {
			return super.onTouchEvent(arg0);
		}
		return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		if (enable) {
			try {
	            return super.onInterceptTouchEvent(arg0);
	        } catch (IllegalArgumentException e) {
	            e.printStackTrace();
	            return false;
	        }
		}
		return false;
	}
	
	
	/**
	 * @param enable
	 * 通过参数设置viewpager是否可滑动
	 */
	public void setViewPagerEnable(boolean enable) {
		this.enable = enable;
	}
	
}

package com.example.wenqujingdian.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.wenqujingdian.activity.HomeActivity;

import java.util.List;

/**
 * Create by kxliu on 2019/1/20
 */
public class BootReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.intent.action.BOOT_COMPLETED")) {
               Intent intent1 = new Intent(context, HomeActivity.class);
               intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               context.startActivity(intent1);
//           }
        }
    }
    private String getProcessName(Context context,int pid) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (am == null) return null;
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        if (processInfos == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
            if (processInfo.pid == pid) {
                return processInfo.processName;
            }
        }
        return null;
    }
}

package com.example.wenqujingdian.model;


import java.util.List;

public class VideoSectionChildBean {
    private int id;
    private String img;
    private String name;
    private boolean isChoose;//item是否选中
    private List<VideoHomeBean.DataBean.VideoListBean> list;

    public VideoSectionChildBean(int id, String img, String name, boolean isChoose, List<VideoHomeBean.DataBean.VideoListBean> list) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.isChoose = isChoose;
        this.list = list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        isChoose = choose;
    }

    public List<VideoHomeBean.DataBean.VideoListBean> getList() {
        return list;
    }

    public void setList(List<VideoHomeBean.DataBean.VideoListBean> list) {
        this.list = list;
    }
}

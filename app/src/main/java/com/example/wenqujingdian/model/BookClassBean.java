package com.example.wenqujingdian.model;

/**
 * Create by kxliu on 2018/11/19
 */
public class BookClassBean {


    /**
     * ClassId : 17
     * ClassName : 哲学、宗教
     * ParentId : 1
     * SortId : 90
     */

    private String ClassId;
    private String ClassName;
    private String ParentId;
    private String SortId;

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String ClassId) {
        this.ClassId = ClassId;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String ClassName) {
        this.ClassName = ClassName;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String ParentId) {
        this.ParentId = ParentId;
    }

    public String getSortId() {
        return SortId;
    }

    public void setSortId(String SortId) {
        this.SortId = SortId;
    }
}

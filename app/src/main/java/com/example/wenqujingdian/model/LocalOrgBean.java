package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Create by kxliu on 2018/11/21
 */
public class LocalOrgBean {


    /**
     * OrgName : 雷达九旅先锋书吧
     * BannerList : [{"BannerId":5,"OrgId":1009,"BannerTitle":"文曲经典","BannerAddress":"javascript:void(0)","BannerImg":"/image/banner/banner1.jpg","IsExternal":false,"Status":true,"CreateDate":"2018-07-03T09:37:20","CreateId":null,"CreateName":""},{"BannerId":6,"OrgId":1009,"BannerTitle":"文曲经典","BannerAddress":"javascript:void(0)","BannerImg":"/image/banner/banner2.jpg","IsExternal":false,"Status":true,"CreateDate":"2018-07-03T09:38:04","CreateId":null,"CreateName":""},{"BannerId":7,"OrgId":1009,"BannerTitle":"书香校园","BannerAddress":"javascript:void(0)","BannerImg":"/image/banner/banner3.jpg","IsExternal":false,"Status":true,"CreateDate":"2018-07-03T09:38:33","CreateId":null,"CreateName":""}]
     * ModuleList : [{"OrgModuleRelationId":56,"OrgId":1009,"ModuleId":1,"ModuleName":"本地图书","ClassName":"tech1","Img":"","ImgUrl":"/image/tech1.png","AndroidLinkAddress":"1","LinkAddress":"/pages/book/?ParentId=1&ParentName=本地图书","IsExternal":false,"CreateDate":"2018-09-08T16:20:05","CreateId":null,"CreateName":"","SortCode":13},{"OrgModuleRelationId":62,"OrgId":1009,"ModuleId":7,"ModuleName":"国学新读","ClassName":"tech1","Img":"","ImgUrl":"/image/tech1.png","AndroidLinkAddress":"7","LinkAddress":"/pages/book/list/?ParentId=79&CurrentId=79&ParentName=国学新读","IsExternal":false,"CreateDate":"2018-09-08T16:20:05","CreateId":null,"CreateName":"","SortCode":7},{"OrgModuleRelationId":64,"OrgId":1009,"ModuleId":9,"ModuleName":"新教育","ClassName":"tech1","Img":"","ImgUrl":"/image/tech1.png","AndroidLinkAddress":"9","LinkAddress":"/pages/book/list/?ParentId=80&CurrentId=80&ParentName=新教育","IsExternal":false,"CreateDate":"2018-09-08T16:20:05","CreateId":null,"CreateName":"","SortCode":9},{"OrgModuleRelationId":65,"OrgId":1009,"ModuleId":10,"ModuleName":"健康养生","ClassName":"tech1","Img":"","ImgUrl":"/image/tech1.png","AndroidLinkAddress":"10","LinkAddress":"/pages/book/list/?ParentId=66&CurrentId=66&ParentName=健康养生","IsExternal":false,"CreateDate":"2018-09-08T16:20:05","CreateId":null,"CreateName":"","SortCode":10},{"OrgModuleRelationId":66,"OrgId":1009,"ModuleId":11,"ModuleName":"党政专题","ClassName":"tech1","Img":"","ImgUrl":"/image/tech1.png","AndroidLinkAddress":"11","LinkAddress":"/pages/book/list/?ParentId=65&CurrentId=65&ParentName=党政专题","IsExternal":false,"CreateDate":"2018-09-08T16:20:05","CreateId":null,"CreateName":"","SortCode":11},{"OrgModuleRelationId":67,"OrgId":1009,"ModuleId":12,"ModuleName":"创业创新","ClassName":"tech1","Img":"","ImgUrl":"/image/tech1.png","AndroidLinkAddress":"12","LinkAddress":"/pages/book/list/?ParentId=67&CurrentId=67&ParentName=创业创新","IsExternal":false,"CreateDate":"2018-09-08T16:20:05","CreateId":null,"CreateName":"","SortCode":12}]
     */

    private String OrgName;
    private List<BannerListBean> BannerList;
    private List<ModuleListBean> ModuleList;

    public String getOrgName() {
        return OrgName;
    }

    public void setOrgName(String OrgName) {
        this.OrgName = OrgName;
    }

    public List<BannerListBean> getBannerList() {
        return BannerList;
    }

    public void setBannerList(List<BannerListBean> BannerList) {
        this.BannerList = BannerList;
    }

    public List<ModuleListBean> getModuleList() {
        return ModuleList;
    }

    public void setModuleList(List<ModuleListBean> ModuleList) {
        this.ModuleList = ModuleList;
    }

    public static class BannerListBean {
        /**
         * BannerId : 5
         * OrgId : 1009
         * BannerTitle : 文曲经典
         * BannerAddress : javascript:void(0)
         * BannerImg : /image/banner/banner1.jpg
         * IsExternal : false
         * Status : true
         * CreateDate : 2018-07-03T09:37:20
         * CreateId : null
         * CreateName :
         */

        private int BannerId;
        private int OrgId;
        private String BannerTitle;
        private String BannerAddress;
        private String BannerImg;
        private boolean IsExternal;
        private boolean Status;
        private String CreateDate;
        private Object CreateId;
        private String CreateName;

        public int getBannerId() {
            return BannerId;
        }

        public void setBannerId(int BannerId) {
            this.BannerId = BannerId;
        }

        public int getOrgId() {
            return OrgId;
        }

        public void setOrgId(int OrgId) {
            this.OrgId = OrgId;
        }

        public String getBannerTitle() {
            return BannerTitle;
        }

        public void setBannerTitle(String BannerTitle) {
            this.BannerTitle = BannerTitle;
        }

        public String getBannerAddress() {
            return BannerAddress;
        }

        public void setBannerAddress(String BannerAddress) {
            this.BannerAddress = BannerAddress;
        }

        public String getBannerImg() {
            return BannerImg;
        }

        public void setBannerImg(String BannerImg) {
            this.BannerImg = BannerImg;
        }

        public boolean isIsExternal() {
            return IsExternal;
        }

        public void setIsExternal(boolean IsExternal) {
            this.IsExternal = IsExternal;
        }

        public boolean isStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        public Object getCreateId() {
            return CreateId;
        }

        public void setCreateId(Object CreateId) {
            this.CreateId = CreateId;
        }

        public String getCreateName() {
            return CreateName;
        }

        public void setCreateName(String CreateName) {
            this.CreateName = CreateName;
        }
    }

    public static class ModuleListBean {
        /**
         * OrgModuleRelationId : 56
         * OrgId : 1009
         * ModuleId : 1
         * ModuleName : 本地图书
         * ClassName : tech1
         * Img :
         * ImgUrl : /image/tech1.png
         * AndroidLinkAddress : 1
         * LinkAddress : /pages/book/?ParentId=1&ParentName=本地图书
         * IsExternal : false
         * CreateDate : 2018-09-08T16:20:05
         * CreateId : null
         * CreateName :
         * SortCode : 13
         */

        private int OrgModuleRelationId;
        private int OrgId;
        private int ModuleId;
        private String ModuleName;
        private String ClassName;
        private String Img;
        private String ImgUrl;
        private String DzImgUrl;
        private String AndroidLinkAddress;
        private String LinkAddress;
        private boolean IsExternal;
        private String CreateDate;
        private Object CreateId;
        private String CreateName;
        private int SortCode;
        private int ClassId;

        public int getOrgModuleRelationId() {
            return OrgModuleRelationId;
        }

        public void setOrgModuleRelationId(int OrgModuleRelationId) {
            this.OrgModuleRelationId = OrgModuleRelationId;
        }

        public int getOrgId() {
            return OrgId;
        }

        public void setOrgId(int OrgId) {
            this.OrgId = OrgId;
        }

        public int getModuleId() {
            return ModuleId;
        }

        public void setModuleId(int ModuleId) {
            this.ModuleId = ModuleId;
        }

        public String getModuleName() {
            return ModuleName;
        }

        public void setModuleName(String ModuleName) {
            this.ModuleName = ModuleName;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String ClassName) {
            this.ClassName = ClassName;
        }

        public String getImg() {
            return Img;
        }

        public void setImg(String Img) {
            this.Img = Img;
        }

        public String getImgUrl() {
            return ImgUrl;
        }

        public void setImgUrl(String ImgUrl) {
            this.ImgUrl = ImgUrl;
        }

        public String getAndroidLinkAddress() {
            return AndroidLinkAddress;
        }

        public void setAndroidLinkAddress(String AndroidLinkAddress) {
            this.AndroidLinkAddress = AndroidLinkAddress;
        }

        public String getLinkAddress() {
            return LinkAddress;
        }

        public void setLinkAddress(String LinkAddress) {
            this.LinkAddress = LinkAddress;
        }

        public boolean isIsExternal() {
            return IsExternal;
        }

        public void setIsExternal(boolean IsExternal) {
            this.IsExternal = IsExternal;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        public Object getCreateId() {
            return CreateId;
        }

        public void setCreateId(Object CreateId) {
            this.CreateId = CreateId;
        }

        public String getCreateName() {
            return CreateName;
        }

        public void setCreateName(String CreateName) {
            this.CreateName = CreateName;
        }

        public int getSortCode() {
            return SortCode;
        }

        public void setSortCode(int SortCode) {
            this.SortCode = SortCode;
        }

        public int getClassId() {
            return ClassId;
        }

        public void setClassId(int classId) {
            ClassId = classId;
        }

        public String getDzImgUrl() {
            return DzImgUrl;
        }

        public void setDzImgUrl(String dzImgUrl) {
            DzImgUrl = dzImgUrl;
        }
    }
}

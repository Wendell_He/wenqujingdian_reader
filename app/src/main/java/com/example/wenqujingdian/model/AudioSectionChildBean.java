package com.example.wenqujingdian.model;


import java.util.List;

public class AudioSectionChildBean {
    private int id;
    private String img;
    private String name;
    private boolean isChoose;//item是否选中
    private List<AudioClassBean.DataBean.AudioListBean> list;

    public AudioSectionChildBean(int id, String img, String name, boolean isChoose, List<AudioClassBean.DataBean.AudioListBean> list) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.isChoose = isChoose;
        this.list = list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        isChoose = choose;
    }

    public List<AudioClassBean.DataBean.AudioListBean> getList() {
        return list;
    }

    public void setList(List<AudioClassBean.DataBean.AudioListBean> list) {
        this.list = list;
    }
}

package com.example.wenqujingdian.model;

import java.util.List;

/**
 * 5个图书模块实体类
 * Create by kxliu on 2018/12/18
 */
public class BookModelBean {

    /**
     * result : true
     * msg :
     * data : [{"pageCount":3,"recordCount":55,"list":[{"BookId":123,"BookTitle":"变革社会中的政治信任","Publisher":"学林出版社","Author":"上官酒瑞","Cover":"/Public/editor/attached/image/20180214/20180214150605_84737.jpg","BookUrl":"/Public/editor/attached/file/20180214/20180214150558_88118.pdf","BookExtension":".pdf","BookSize":"2850792","ISBN":"9787548606086","PublishDate":"2013/12/1"}]}]
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pageCount : 3
         * recordCount : 55
         * list : [{"BookId":123,"BookTitle":"变革社会中的政治信任","Publisher":"学林出版社",
         * "Author":"上官酒瑞","Cover":"/Public/editor/attached/image/20180214/20180214150605_84737.jpg",
         * "BookUrl":"/Public/editor/attached/file/20180214/20180214150558_88118.pdf","BookExtension":".pdf",
         * "BookSize":"2850792","ISBN":"9787548606086","PublishDate":"2013/12/1"}]
         */

        private int pageCount;
        private int recordCount;
        private List<ListBean> list;

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * BookId : 123
             * BookTitle : 变革社会中的政治信任
             * Publisher : 学林出版社
             * Author : 上官酒瑞
             * Cover : /Public/editor/attached/image/20180214/20180214150605_84737.jpg
             * BookUrl : /Public/editor/attached/file/20180214/20180214150558_88118.pdf
             * BookExtension : .pdf
             * BookSize : 2850792
             * ISBN : 9787548606086
             * PublishDate : 2013/12/1
             */

            private int BookId;
            private String BookTitle;
            private String Publisher;
            private String Author;
            private String Cover;
            private String BookUrl;
            private String BookExtension;
            private String BookSize;
            private String ISBN;
            private String PublishDate;
            private String ClassId;

            public int getBookId() {
                return BookId;
            }

            public void setBookId(int BookId) {
                this.BookId = BookId;
            }

            public String getBookTitle() {
                return BookTitle;
            }

            public void setBookTitle(String BookTitle) {
                this.BookTitle = BookTitle;
            }

            public String getPublisher() {
                return Publisher;
            }

            public void setPublisher(String Publisher) {
                this.Publisher = Publisher;
            }

            public String getAuthor() {
                return Author;
            }

            public void setAuthor(String Author) {
                this.Author = Author;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }

            public String getBookUrl() {
                return BookUrl;
            }

            public void setBookUrl(String BookUrl) {
                this.BookUrl = BookUrl;
            }

            public String getBookExtension() {
                return BookExtension;
            }

            public void setBookExtension(String BookExtension) {
                this.BookExtension = BookExtension;
            }

            public String getBookSize() {
                return BookSize;
            }

            public void setBookSize(String BookSize) {
                this.BookSize = BookSize;
            }

            public String getISBN() {
                return ISBN;
            }

            public void setISBN(String ISBN) {
                this.ISBN = ISBN;
            }

            public String getPublishDate() {
                return PublishDate;
            }

            public void setPublishDate(String PublishDate) {
                this.PublishDate = PublishDate;
            }

            public String getClassId() {
                return ClassId;
            }

            public void setClassId(String classId) {
                ClassId = classId;
            }
        }
    }
}

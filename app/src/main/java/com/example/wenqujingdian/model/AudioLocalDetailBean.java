package com.example.wenqujingdian.model;

/**
 * Create by kxliu on 2019/1/9
 */
public class AudioLocalDetailBean {

    /**
     * AudioId : 228
     * AudioTitle : 第二集
     * AudioUrl : /Public/editor/attached/file/20180327/20180327165323_72679.mp3
     * ParentId : 211
     */

    private String AudioId;
    private String AudioTitle;
    private String AudioUrl;
    private String ParentId;
    private String cover;

    public String getAudioId() {
        return AudioId;
    }

    public void setAudioId(String AudioId) {
        this.AudioId = AudioId;
    }

    public String getAudioTitle() {
        return AudioTitle;
    }

    public void setAudioTitle(String AudioTitle) {
        this.AudioTitle = AudioTitle;
    }

    public String getAudioUrl() {
        return AudioUrl;
    }

    public void setAudioUrl(String AudioUrl) {
        this.AudioUrl = AudioUrl;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String ParentId) {
        this.ParentId = ParentId;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}

package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Create by kxliu on 2018/12/25
 */
public class VideoHomeBean {

    /**
     * data : [{"ClassId":11,"ClassName":"唐诗宋词","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":3,"Remark":null,"VideoList":[{"VideoId":3,"VideoTitle":"唐诗","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312150011_33988.jpg","VideoUrl":null,"ParentId":0},{"VideoId":4,"VideoTitle":"宋词","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314093818_41412.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":33,"ClassName":"幼儿教育","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":82,"Remark":null,"VideoList":[{"VideoId":5,"VideoTitle":"幼儿教育-小班","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314131307_53110.jpg","VideoUrl":null,"ParentId":0},{"VideoId":6,"VideoTitle":"幼儿教育-中班","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314133851_71699.jpg","VideoUrl":null,"ParentId":0},{"VideoId":7,"VideoTitle":"幼儿教育-大班","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314141109_94144.jpg","VideoUrl":null,"ParentId":0},{"VideoId":21,"VideoTitle":" 幼学声律启蒙（第二部）1-15集","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180528/20180528131707_53566.jpg","VideoUrl":null,"ParentId":0},{"VideoId":22,"VideoTitle":"幼学声律启蒙（第一部）1-15集","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180525/20180525172234_38155.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":70,"ClassName":"科普知识","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":447,"Remark":null,"VideoList":[{"VideoId":1,"VideoTitle":"十万个为什么-动物世界","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314151308_26504.jpg","VideoUrl":null,"ParentId":0},{"VideoId":2,"VideoTitle":"十万个为什么-安全知识","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314144841_72119.jpg","VideoUrl":null,"ParentId":0},{"VideoId":24,"VideoTitle":"科普中国","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180531/20180531101951_16792.jpg","VideoUrl":null,"ParentId":0},{"VideoId":991,"VideoTitle":"科学家的故事","VideoContent":null,"VideoSummary":null,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180929/20180929161923_57120.jpg","VideoUrl":null,"ParentId":0},{"VideoId":992,"VideoTitle":"植物科普知识","VideoContent":null,"VideoSummary":null,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180929/20180929171414_51097.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":71,"ClassName":"美术音乐","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":458,"Remark":null,"VideoList":[{"VideoId":8,"VideoTitle":"国画花鸟","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314153818_18525.jpg","VideoUrl":null,"ParentId":0},{"VideoId":9,"VideoTitle":"国画入门","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314161255_97816.jpg","VideoUrl":null,"ParentId":0},{"VideoId":10,"VideoTitle":"国画山水","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314163025_35765.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":72,"ClassName":"院校公开课","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":459,"Remark":null,"VideoList":[{"VideoId":11,"VideoTitle":"麻省理工学院公开课电影哲学","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180315/20180315103334_27553.png","VideoUrl":null,"ParentId":0},{"VideoId":12,"VideoTitle":"牛津大学公开课康德的纯粹理性批判","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180315/20180315105555_74124.jpg","VideoUrl":null,"ParentId":0},{"VideoId":13,"VideoTitle":"牛津大学公开课尼采的心灵与自然","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180315/20180315112722_81282.jpg","VideoUrl":null,"ParentId":0},{"VideoId":14,"VideoTitle":"牛津大学公开课批判性推理入门","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180315/20180315122544_15537.jpg","VideoUrl":null,"ParentId":0},{"VideoId":15,"VideoTitle":"牛津大学公开课哲学概论","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180315/20180315124900_77469.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":82,"ClassName":"名师讲座","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":473,"Remark":null,"VideoList":[{"VideoId":27,"VideoTitle":"魏书生中山报告会","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180601/20180601101645_90768.jpg","VideoUrl":null,"ParentId":0},{"VideoId":28,"VideoTitle":"如何当好班主任","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180601/20180601125257_18662.jpg","VideoUrl":null,"ParentId":0},{"VideoId":29,"VideoTitle":"魏书生作文示范课","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180601/20180601132822_81336.jpg","VideoUrl":null,"ParentId":0},{"VideoId":32,"VideoTitle":"朱永新讲座","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180604/20180604092147_26147.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":83,"ClassName":"书法教学","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":474,"Remark":null,"VideoList":[{"VideoId":23,"VideoTitle":"名师书法讲座硬笔书法-写字学古诗","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180531/20180531100022_32272.jpg","VideoUrl":null,"ParentId":0},{"VideoId":30,"VideoTitle":"学写字","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180601/20180601134806_81897.jpg","VideoUrl":null,"ParentId":0},{"VideoId":994,"VideoTitle":"书法","VideoContent":null,"VideoSummary":null,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180911/20180911151054_25743.jpg","VideoUrl":null,"ParentId":0},{"VideoId":995,"VideoTitle":"硬笔书法","VideoContent":null,"VideoSummary":null,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20181009/20181009111615_49995.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":84,"ClassName":"党政学习","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":475,"Remark":null,"VideoList":[{"VideoId":31,"VideoTitle":"习主席十九大报告","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180601/20180601154212_72243.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":85,"ClassName":"汉字动漫","ParentId":2,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":476,"Remark":null,"VideoList":[{"VideoId":25,"VideoTitle":"汉字演变","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180531/20180531130414_56148.jpg","VideoUrl":null,"ParentId":0},{"VideoId":810,"VideoTitle":"动漫经典18个汉字","VideoContent":null,"VideoSummary":null,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180910/20180910164902_70663.jpg","VideoUrl":null,"ParentId":0},{"VideoId":811,"VideoTitle":"嘟嘟小巴士汉字城堡","VideoContent":null,"VideoSummary":null,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180907/20180907112112_51108.jpg","VideoUrl":null,"ParentId":0},{"VideoId":996,"VideoTitle":"亲宝识字系列","VideoContent":null,"VideoSummary":null,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20181008/20181008162907_21412.jpg","VideoUrl":null,"ParentId":0},{"VideoId":997,"VideoTitle":"动漫汉字","VideoContent":null,"VideoSummary":null,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180910/20180910164902_70663.jpg","VideoUrl":null,"ParentId":0}],"Childs":{}}]
     * result : true
     * msg : success
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean{
        /**
         * ClassId : 11
         * ClassName : 唐诗宋词
         * ParentId : 2
         * ClassCode : null
         * ClassImg : null
         * ClassType : null
         * Cover : null
         * SortId : 3
         * Remark : null
         * VideoList : [{"VideoId":3,"VideoTitle":"唐诗","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312150011_33988.jpg","VideoUrl":null,"ParentId":0},{"VideoId":4,"VideoTitle":"宋词","VideoContent":"","VideoSummary":"","Hits":0,"Author":"","StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180314/20180314093818_41412.jpg","VideoUrl":null,"ParentId":0}]
         * Childs : {}
         */

        private int ClassId;
        private String ClassName;
        private int ParentId;
        private int SortId;
        private List<VideoListBean> VideoList;

        public int getClassId() {
            return ClassId;
        }

        public void setClassId(int ClassId) {
            this.ClassId = ClassId;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String ClassName) {
            this.ClassName = ClassName;
        }

        public int getParentId() {
            return ParentId;
        }

        public void setParentId(int ParentId) {
            this.ParentId = ParentId;
        }

        public int getSortId() {
            return SortId;
        }

        public void setSortId(int SortId) {
            this.SortId = SortId;
        }

        public List<VideoListBean> getVideoList() {
            return VideoList;
        }

        public void setVideoList(List<VideoListBean> VideoList) {
            this.VideoList = VideoList;
        }

        public static class VideoListBean{
            /**
             * VideoId : 3
             * VideoTitle : 唐诗
             * VideoContent :
             * VideoSummary :
             * Hits : 0
             * Author :
             * StoreUpCount : null
             * Cover : /Public/editor/attached/image/20180312/20180312150011_33988.jpg
             * VideoUrl : null
             * ParentId : 0
             */

            private int VideoId;
            private String VideoTitle;
            private String VideoContent;
            private String VideoSummary;
            private int Hits;
            private String Author;
            private String Cover;
            private String VideoUrl;
            private int ParentId;

            public int getVideoId() {
                return VideoId;
            }

            public void setVideoId(int VideoId) {
                this.VideoId = VideoId;
            }

            public String getVideoTitle() {
                return VideoTitle;
            }

            public void setVideoTitle(String VideoTitle) {
                this.VideoTitle = VideoTitle;
            }

            public String getVideoContent() {
                return VideoContent;
            }

            public void setVideoContent(String VideoContent) {
                this.VideoContent = VideoContent;
            }

            public String getVideoSummary() {
                return VideoSummary;
            }

            public void setVideoSummary(String VideoSummary) {
                this.VideoSummary = VideoSummary;
            }

            public int getHits() {
                return Hits;
            }

            public void setHits(int Hits) {
                this.Hits = Hits;
            }

            public String getAuthor() {
                return Author;
            }

            public void setAuthor(String Author) {
                this.Author = Author;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }

            public String getVideoUrl() {
                return VideoUrl;
            }

            public void setVideoUrl(String VideoUrl) {
                this.VideoUrl = VideoUrl;
            }

            public int getParentId() {
                return ParentId;
            }

            public void setParentId(int ParentId) {
                this.ParentId = ParentId;
            }
        }
    }
}

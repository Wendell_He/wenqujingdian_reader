package com.example.wenqujingdian.model;

/**
 * Create by kxliu on 2019/1/8
 */
public class VideoLocalListBean {

    /**
     * VideoId : 1
     * VideoTitle : 十万个为什么-动物世界
     * VideoContent :
     * VideoSummary :
     * CreateDate :
     * VideoCount : 16
     * Hits : 0
     * Author :
     * StoreUpCount :
     * Cover : /Public/editor/attached/image/20180314/20180314151308_26504.jpg
     * VideoUrl :
     * CLassId : 70
     * ClassName : 科普知识
     */

    private String VideoId;
    private String VideoTitle;
    private String Cover;
    private String VideoUrl;
    private String CLassId;
    private String ClassName;

    public String getVideoId() {
        return VideoId;
    }

    public void setVideoId(String VideoId) {
        this.VideoId = VideoId;
    }

    public String getVideoTitle() {
        return VideoTitle;
    }

    public void setVideoTitle(String VideoTitle) {
        this.VideoTitle = VideoTitle;
    }

    public String getCover() {
        return Cover;
    }

    public void setCover(String Cover) {
        this.Cover = Cover;
    }

    public String getVideoUrl() {
        return VideoUrl;
    }

    public void setVideoUrl(String VideoUrl) {
        this.VideoUrl = VideoUrl;
    }

    public String getCLassId() {
        return CLassId;
    }

    public void setCLassId(String CLassId) {
        this.CLassId = CLassId;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String ClassName) {
        this.ClassName = ClassName;
    }
}

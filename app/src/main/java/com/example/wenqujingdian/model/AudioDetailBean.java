package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Create by kxliu on 2018/12/26
 */
public class AudioDetailBean {

    /**
     * result : true
     * msg :
     * data : [{"pageCount":1,"recordCount":13,"list":[{"AudioId":2993,"AudioTitle":"第一集","AudioContent":"","AudioSummary":"","AudioType":"","add_time":1520560604,"CreateDate":null,"AudioCount":0,"Hits":38,"Author":"","StoreUpCount":null,"Cover":"","AudioUrl":"/Public/editor/attached/file/20180309/20180309095641_93215.mp3","PId":2,"ROWSTAT":null}
     * ,{"AudioId":2994,"AudioTitle":"第二集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560634,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095711_10381.mp3","PId":2,"ROWSTAT":null},{"AudioId":2995,"AudioTitle":"第三集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560667,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095740_58801.mp3","PId":2,"ROWSTAT":null},{"AudioId":2996,"AudioTitle":"第四集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560697,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095814_86994.mp3","PId":2,"ROWSTAT":null},{"AudioId":2997,"AudioTitle":"第五集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560726,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095843_31198.mp3","PId":2,"ROWSTAT":null},{"AudioId":2998,"AudioTitle":"第六集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560756,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095912_28564.mp3","PId":2,"ROWSTAT":null},{"AudioId":2999,"AudioTitle":"第七集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560788,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095943_66304.mp3","PId":2,"ROWSTAT":null},{"AudioId":3000,"AudioTitle":"第八集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560828,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100020_78019.mp3","PId":2,"ROWSTAT":null},{"AudioId":3001,"AudioTitle":"第九集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560886,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100054_93457.mp3","PId":2,"ROWSTAT":null},{"AudioId":3002,"AudioTitle":"第十集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560916,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100153_92976.mp3","PId":2,"ROWSTAT":null},{"AudioId":3003,"AudioTitle":"第十一集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560965,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100242_72929.mp3","PId":2,"ROWSTAT":null},{"AudioId":3004,"AudioTitle":"第十二集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560994,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100311_63242.mp3","PId":2,"ROWSTAT":null},{"AudioId":3005,"AudioTitle":"第十三集","AudioContent":"","AudioSummary":"","AudioType":"","add_time":1520561021,"CreateDate":null,"AudioCount":0,"Hits":1,"Author":"","StoreUpCount":null,"Cover":"","AudioUrl":"/Public/editor/attached/file/20180309/20180309100338_63432.mp3","PId":2,"ROWSTAT":null}]}]
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pageCount : 1
         * recordCount : 13
         * list : [{"AudioId":2993,"AudioTitle":"第一集","AudioContent":"","AudioSummary":"","AudioType":"","add_time":1520560604,"CreateDate":null,"AudioCount":0,"Hits":38,"Author":"","StoreUpCount":null,"Cover":"","AudioUrl":"/Public/editor/attached/file/20180309/20180309095641_93215.mp3","PId":2,"ROWSTAT":null},{"AudioId":2994,"AudioTitle":"第二集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560634,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095711_10381.mp3","PId":2,"ROWSTAT":null},{"AudioId":2995,"AudioTitle":"第三集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560667,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095740_58801.mp3","PId":2,"ROWSTAT":null},{"AudioId":2996,"AudioTitle":"第四集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560697,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095814_86994.mp3","PId":2,"ROWSTAT":null},{"AudioId":2997,"AudioTitle":"第五集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560726,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095843_31198.mp3","PId":2,"ROWSTAT":null},{"AudioId":2998,"AudioTitle":"第六集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560756,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095912_28564.mp3","PId":2,"ROWSTAT":null},{"AudioId":2999,"AudioTitle":"第七集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560788,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309095943_66304.mp3","PId":2,"ROWSTAT":null},{"AudioId":3000,"AudioTitle":"第八集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560828,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100020_78019.mp3","PId":2,"ROWSTAT":null},{"AudioId":3001,"AudioTitle":"第九集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560886,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100054_93457.mp3","PId":2,"ROWSTAT":null},{"AudioId":3002,"AudioTitle":"第十集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560916,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100153_92976.mp3","PId":2,"ROWSTAT":null},{"AudioId":3003,"AudioTitle":"第十一集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560965,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100242_72929.mp3","PId":2,"ROWSTAT":null},{"AudioId":3004,"AudioTitle":"第十二集","AudioContent":null,"AudioSummary":null,"AudioType":null,"add_time":1520560994,"CreateDate":null,"AudioCount":0,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":null,"AudioUrl":"/Public/editor/attached/file/20180309/20180309100311_63242.mp3","PId":2,"ROWSTAT":null},{"AudioId":3005,"AudioTitle":"第十三集","AudioContent":"","AudioSummary":"","AudioType":"","add_time":1520561021,"CreateDate":null,"AudioCount":0,"Hits":1,"Author":"","StoreUpCount":null,"Cover":"","AudioUrl":"/Public/editor/attached/file/20180309/20180309100338_63432.mp3","PId":2,"ROWSTAT":null}]
         */

        private int pageCount;
        private int recordCount;
        private List<ListBean> list;

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * AudioId : 2993
             * AudioTitle : 第一集
             * AudioContent :
             * AudioSummary :
             * AudioType :
             * add_time : 1520560604
             * CreateDate : null
             * AudioCount : 0
             * Hits : 38
             * Author :
             * StoreUpCount : null
             * Cover :
             * AudioUrl : /Public/editor/attached/file/20180309/20180309095641_93215.mp3
             * PId : 2
             * ROWSTAT : null
             */

            private int AudioId;
            private String AudioTitle;
            private int add_time;
            private int AudioCount;
            private int Hits;
            private String Author;
            private String AudioUrl;
            private String cover;

            public int getAudioId() {
                return AudioId;
            }

            public void setAudioId(int AudioId) {
                this.AudioId = AudioId;
            }

            public String getAudioTitle() {
                return AudioTitle;
            }

            public void setAudioTitle(String AudioTitle) {
                this.AudioTitle = AudioTitle;
            }

            public int getAdd_time() {
                return add_time;
            }

            public void setAdd_time(int add_time) {
                this.add_time = add_time;
            }

            public int getAudioCount() {
                return AudioCount;
            }

            public void setAudioCount(int AudioCount) {
                this.AudioCount = AudioCount;
            }

            public int getHits() {
                return Hits;
            }

            public void setHits(int Hits) {
                this.Hits = Hits;
            }

            public String getAuthor() {
                return Author;
            }

            public void setAuthor(String Author) {
                this.Author = Author;
            }

            public String getAudioUrl() {
                return AudioUrl;
            }

            public void setAudioUrl(String AudioUrl) {
                this.AudioUrl = AudioUrl;
            }

            public String getCover() {
                return cover;
            }

            public void setCover(String cover) {
                this.cover = cover;
            }
        }
    }
}

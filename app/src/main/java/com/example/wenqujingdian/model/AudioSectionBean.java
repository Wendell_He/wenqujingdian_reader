package com.example.wenqujingdian.model;


import com.chad.library.adapter.base.entity.SectionEntity;

public class AudioSectionBean extends SectionEntity<AudioSectionChildBean> {
    private boolean isMore;
    private String id;

    public AudioSectionBean(boolean isHeader, String header, String id, boolean isMroe) {
        super(isHeader, header);
        this.isMore = isMroe;
        this.id = id;
    }

    public AudioSectionBean(AudioSectionChildBean t) {
        super(t);
    }

    public boolean isMore() {
        return isMore;
    }

    public void setMore(boolean mroe) {
        isMore = mroe;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

package com.example.wenqujingdian.model;

/**
 * Create by kxliu on 2019/1/9
 */
public class AudioLocalMoreBean {

    /**
     * AudioId : 1
     * AudioTitle : 百年浙商（下）
     * AudioContent :
     * AudioSummary :
     * AudioType : 原创
     * CreateDate :
     * AudioCount : 27
     * Hits : 0
     * Author :
     * StoreUpCount :
     * Cover : /Public/editor/attached/image/20180309/20180309093830_25600.jpg
     * AudioUrl :
     * CLassId : 19
     * ClassName : 文化
     */

    private String AudioId;
    private String AudioTitle;
    private String Cover;
    private String AudioUrl;
    private String CLassId;
    private String ClassName;

    public String getAudioId() {
        return AudioId;
    }

    public void setAudioId(String AudioId) {
        this.AudioId = AudioId;
    }

    public String getAudioTitle() {
        return AudioTitle;
    }

    public void setAudioTitle(String AudioTitle) {
        this.AudioTitle = AudioTitle;
    }

    public String getCover() {
        return Cover;
    }

    public void setCover(String Cover) {
        this.Cover = Cover;
    }

    public String getAudioUrl() {
        return AudioUrl;
    }

    public void setAudioUrl(String AudioUrl) {
        this.AudioUrl = AudioUrl;
    }

    public String getCLassId() {
        return CLassId;
    }

    public void setCLassId(String CLassId) {
        this.CLassId = CLassId;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String ClassName) {
        this.ClassName = ClassName;
    }
}

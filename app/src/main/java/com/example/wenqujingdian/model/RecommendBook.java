package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Create by kxliu on 2018/11/24
 */
public class RecommendBook {

    /**
     * result : true
     * code :
     * msg :
     * draw : 1
     * recordsTotal : 16
     * recordsFiltered : 16
     * data : [{"BookId":679,"BookTitle":"清华大学","BookContent":"","BookSummary":"","Publisher":"清华大学出版社","Author":"朱文一，滕静茹，陈瑾羲编著","Cover":"/Public/editor/attached/image/20180330/20180330225655_80216.jpg","BookUrl":"/Public/editor/attached/file/20180330/20180330225620_85728.pdf","BookExtension":".pdf","BookSize":"7894881","ParentId":0,"Childs":0,"ISBN":"9787302254577","PublishDate":"2011-05-01","Hits":91,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":18870,"BookTitle":"莎士比亚名著故事\u2014\u2014罗密欧与朱丽叶","BookContent":"","BookSummary":"","Publisher":"清华大学出版社","Author":"(英)查尔斯·兰姆，(英)玛丽·兰姆改编","Cover":"/Public/editor/attached/image/20180327/20180327155653_27899.jpg","BookUrl":"/Public/editor/attached/file/20180327/20180327155850_32752.pdf","BookExtension":".pdf","BookSize":"3730785","ParentId":0,"Childs":0,"ISBN":"9787302285953","PublishDate":"2012-07-01","Hits":48,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":17266,"BookTitle":"屠格涅夫散文诗","BookContent":"","BookSummary":"","Publisher":"花城出版社","Author":"(俄)伊凡·屠格涅夫(Иван Тургенев)著","Cover":"/Public/editor/attached/image/20180402/20180402152301_52118.jpg","BookUrl":"/Public/editor/attached/file/20180402/20180402152736_78659.pdf","BookExtension":".pdf","BookSize":"19451320","ParentId":0,"Childs":0,"ISBN":"9787536067288","PublishDate":"2013-04-01","Hits":20,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":45061,"BookTitle":"富兰克林","BookContent":"","BookSummary":"","Publisher":"浙江少年儿童出版社","Author":"郭梅，陈庆乾","Cover":"/Public/editor/attached/image/20180405/20180405124323_94927.jpg","BookUrl":"/Public/editor/attached/file/20180405/20180405124345_30681.pdf","BookExtension":".pdf","BookSize":"1605597","ParentId":0,"Childs":0,"ISBN":"9787534252426","PublishDate":"2013-12-01","Hits":14,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":19687,"BookTitle":"宋史十二讲","BookContent":"","BookSummary":"","Publisher":"清华大学出版社","Author":"陈智超著","Cover":"/Public/editor/attached/image/20180522/20180522154802_63962.jpg","BookUrl":"/Public/editor/attached/file/20180522/20180522154744_70295.pdf","BookExtension":".pdf","BookSize":"4099496","ParentId":0,"Childs":0,"ISBN":"9787302217312","PublishDate":"2010-02-01","Hits":14,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":24786,"BookTitle":"初中文言150实词例释","BookContent":"","BookSummary":"","Publisher":"","Author":"","Cover":"/Public/editor/attached/image/20180214/20180214163147_65656.jpg","BookUrl":"/Public/editor/attached/file/20180214/20180214163141_27784.pdf","BookExtension":".pdf","BookSize":"2840400","ParentId":0,"Childs":0,"ISBN":"9787532558964G.531","PublishDate":"2013/3/1","Hits":12,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":48838,"BookTitle":"汤姆叔叔的小屋","BookContent":"","BookSummary":"","Publisher":"电子工业出版社","Author":"(美)斯托夫人著","Cover":"/Public/editor/attached/image/20180523/20180523203446_31005.jpg","BookUrl":"/Public/editor/attached/file/20180523/20180523203501_38545.pdf","BookExtension":".pdf","BookSize":"3312167","ParentId":0,"Childs":0,"ISBN":"9787121138331","PublishDate":"2011-09-01","Hits":11,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":12389,"BookTitle":"信息化十讲","BookContent":"","BookSummary":"","Publisher":"电子工业出版社","Author":"邹生主编","Cover":"/Public/editor/attached/image/20180221/20180221205349_34345.jpg","BookUrl":"/Public/editor/attached/file/20180221/20180221205520_63322.pdf","BookExtension":".pdf","BookSize":"40189504","ParentId":0,"Childs":0,"ISBN":"9787121075445","PublishDate":"2009年01月","Hits":11,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":4983,"BookTitle":"90后，有点苦有点涩","BookContent":"","BookSummary":"","Publisher":"","Author":"","Cover":"/Public/editor/attached/image/20180220/20180220174053_99033.jpg","BookUrl":"/Public/editor/attached/file/20180404/20180404103009_27295.pdf","BookExtension":".pdf","BookSize":"1041389","ParentId":0,"Childs":0,"ISBN":"","PublishDate":"","Hits":8,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":5332,"BookTitle":"去香港上大学","BookContent":"","BookSummary":"","Publisher":"广西人民出版社","Author":"李子迟","Cover":"/Public/editor/attached/image/20180401/20180401144940_40690.jpg","BookUrl":"/Public/editor/attached/file/20180401/20180401144925_55705.pdf","BookExtension":".pdf","BookSize":"1375325","ParentId":0,"Childs":0,"ISBN":"97872190582989","PublishDate":"2007-04-01","Hits":7,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsBlurb":true,"IsHot":false,"ROWSTAT":null}]
     * bookUrlPre : http://www.wenqujingdian.com
     */

    private boolean result;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * BookId : 679
         * BookTitle : 清华大学
         * BookContent :
         * BookSummary :
         * Publisher : 清华大学出版社
         * Author : 朱文一，滕静茹，陈瑾羲编著
         * Cover : /Public/editor/attached/image/20180330/20180330225655_80216.jpg
         * BookUrl : /Public/editor/attached/file/20180330/20180330225620_85728.pdf
         * BookExtension : .pdf
         * BookSize : 7894881
         * ParentId : 0
         * Childs : 0
         * ISBN : 9787302254577
         * PublishDate : 2011-05-01
         * Hits : 91
         * StoreUpCount : 0
         * IsBold : 0
         * IsTop : false
         * IsNew : false
         * IsBlurb : true
         * IsHot : false
         * ROWSTAT : null
         */

        private int BookId;
        private String BookTitle;
        private String BookContent;
        private String BookSummary;
        private String Publisher;
        private String Author;
        private String Cover;
        private String BookUrl;
        private String BookExtension;
        private String BookSize;
        private int ParentId;
        private String PublishDate;

        public int getBookId() {
            return BookId;
        }

        public void setBookId(int BookId) {
            this.BookId = BookId;
        }

        public String getBookTitle() {
            return BookTitle;
        }

        public void setBookTitle(String BookTitle) {
            this.BookTitle = BookTitle;
        }

        public String getBookContent() {
            return BookContent;
        }

        public void setBookContent(String BookContent) {
            this.BookContent = BookContent;
        }

        public String getBookSummary() {
            return BookSummary;
        }

        public void setBookSummary(String BookSummary) {
            this.BookSummary = BookSummary;
        }

        public String getPublisher() {
            return Publisher;
        }

        public void setPublisher(String Publisher) {
            this.Publisher = Publisher;
        }

        public String getAuthor() {
            return Author;
        }

        public void setAuthor(String Author) {
            this.Author = Author;
        }

        public String getCover() {
            return Cover;
        }

        public void setCover(String Cover) {
            this.Cover = Cover;
        }

        public String getBookUrl() {
            return BookUrl;
        }

        public void setBookUrl(String BookUrl) {
            this.BookUrl = BookUrl;
        }

        public String getBookExtension() {
            return BookExtension;
        }

        public void setBookExtension(String BookExtension) {
            this.BookExtension = BookExtension;
        }

        public String getBookSize() {
            return BookSize;
        }

        public void setBookSize(String BookSize) {
            this.BookSize = BookSize;
        }

        public int getParentId() {
            return ParentId;
        }

        public void setParentId(int ParentId) {
            this.ParentId = ParentId;
        }

        public String getPublishDate() {
            return PublishDate;
        }

        public void setPublishDate(String PublishDate) {
            this.PublishDate = PublishDate;
        }
    }
}

package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Create by kxliu on 2018/12/15
 */
public class VideoChildBean {

    /**
     * result : true
     * msg :
     * data : [{"pageCount":1,"recordCount":2,"list":[{"VideoId":3,"VideoTitle":"唐诗","VideoCount":50,"Cover":"/Public/editor/attached/image/20180312/20180312150011_33988.jpg"}]}]
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pageCount : 1
         * recordCount : 2
         * list : [{"VideoId":3,"VideoTitle":"唐诗","VideoCount":50,"Cover":"/Public/editor/attached/image/20180312/20180312150011_33988.jpg"}]
         */

        private int pageCount;
        private int recordCount;
        private List<ListBean> list;

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * VideoId : 3
             * VideoTitle : 唐诗
             * VideoCount : 50
             * Cover : /Public/editor/attached/image/20180312/20180312150011_33988.jpg
             */

            private int VideoId;
            private String VideoTitle;
            private int VideoCount;
            private String Cover;

            public int getVideoId() {
                return VideoId;
            }

            public void setVideoId(int VideoId) {
                this.VideoId = VideoId;
            }

            public String getVideoTitle() {
                return VideoTitle;
            }

            public void setVideoTitle(String VideoTitle) {
                this.VideoTitle = VideoTitle;
            }

            public int getVideoCount() {
                return VideoCount;
            }

            public void setVideoCount(int VideoCount) {
                this.VideoCount = VideoCount;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }
        }
    }
}

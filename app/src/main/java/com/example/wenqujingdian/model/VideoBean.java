package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Create by kxliu on 2018/12/4
 */
public class VideoBean {

    /**
     * result : true
     * msg :
     * data : [{"pageCount":3,"recordCount":49,"list":[{"VideoId":1,"VideoTitle":"十万个为什么-动物世界","Cover":"/Public/editor/attached/image/20180314/20180314151308_26504.jpg"}]}]
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pageCount : 3
         * recordCount : 49
         * list : [{"VideoId":1,"VideoTitle":"十万个为什么-动物世界","Cover":"/Public/editor/attached/image/20180314/20180314151308_26504.jpg"}]
         */

        private int pageCount;
        private int recordCount;
        private List<ListBean> list;

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * VideoId : 1
             * VideoTitle : 十万个为什么-动物世界
             * Cover : /Public/editor/attached/image/20180314/20180314151308_26504.jpg
             */

            private int VideoId;
            private String VideoTitle;
            private String Cover;

            public int getVideoId() {
                return VideoId;
            }

            public void setVideoId(int VideoId) {
                this.VideoId = VideoId;
            }

            public String getVideoTitle() {
                return VideoTitle;
            }

            public void setVideoTitle(String VideoTitle) {
                this.VideoTitle = VideoTitle;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }
        }
    }
}

package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Create by kxliu on 2018/12/26
 */
public class AudioClassBean {

    /**
     * data : [{"ClassId":15,"ClassName":"评书","ParentId":3,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":424,"Remark":null,"AudioList":[{"AudioId":2,"AudioTitle":"包拯：断案如神的青天老爷","AudioContent":"","AudioSummary":"","AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309095459_81666.jpg","AudioUrl":null,"ParentId":0},{"AudioId":10,"AudioTitle":"曹操：乱世奸雄治世能臣","AudioContent":"","AudioSummary":"","AudioCount":15,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309100431_88307.jpg","AudioUrl":null,"ParentId":0},{"AudioId":11,"AudioTitle":"曹雪芹：清朝才子的红楼一梦","AudioContent":"","AudioSummary":"","AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309101447_45131.jpg","AudioUrl":null,"ParentId":0},{"AudioId":16,"AudioTitle":"春秋争霸","AudioContent":"","AudioSummary":"","AudioCount":26,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309111513_43890.jpg","AudioUrl":null,"ParentId":0},{"AudioId":35,"AudioTitle":"汉武帝：铁血雄风大汉天子","AudioContent":"","AudioSummary":"","AudioCount":14,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312085857_85948.jpg","AudioUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":18,"ClassName":"小说","ParentId":3,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":428,"Remark":null,"AudioList":[{"AudioId":6,"AudioTitle":"80后的火红年代（下）","AudioContent":"","AudioSummary":"","AudioCount":21,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180308/20180308170009_83539.jpg","AudioUrl":null,"ParentId":0},{"AudioId":7,"AudioTitle":"80后的火红年代（上）","AudioContent":"","AudioSummary":"","AudioCount":20,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180308/20180308164139_42096.jpg","AudioUrl":null,"ParentId":0},{"AudioId":17,"AudioTitle":"刺客列传","AudioContent":"","AudioSummary":"","AudioCount":25,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309113234_96033.jpg","AudioUrl":null,"ParentId":0},{"AudioId":22,"AudioTitle":"谍上谍（上）","AudioContent":"","AudioSummary":"","AudioCount":27,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309134145_60781.jpg","AudioUrl":null,"ParentId":0},{"AudioId":23,"AudioTitle":"谍上谍（下）","AudioContent":"","AudioSummary":"","AudioCount":27,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309140502_38229.jpg","AudioUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":46,"ClassName":"相声曲艺","ParentId":3,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":430,"Remark":null,"AudioList":[{"AudioId":148,"AudioTitle":"豫剧名家金不换·宝贝","AudioContent":"","AudioSummary":"","AudioCount":5,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180326/20180326144146_40116.jpg","AudioUrl":null,"ParentId":0},{"AudioId":149,"AudioTitle":"豫剧名家金不换·草根秀才","AudioContent":"","AudioSummary":"","AudioCount":4,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180326/20180326144544_98873.jpg","AudioUrl":null,"ParentId":0},{"AudioId":150,"AudioTitle":"豫剧名家金不换·打金枝","AudioContent":"","AudioSummary":"","AudioCount":3,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180326/20180326144822_66225.jpg","AudioUrl":null,"ParentId":0},{"AudioId":151,"AudioTitle":"豫剧名家金不换·窦娥冤","AudioContent":"","AudioSummary":"","AudioCount":4,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180326/20180326145105_73380.jpg","AudioUrl":null,"ParentId":0},{"AudioId":152,"AudioTitle":"豫剧名家金不换·恩仇联姻","AudioContent":"","AudioSummary":"","AudioCount":2,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180326/20180326145959_65261.jpg","AudioUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":87,"ClassName":"经管课程","ParentId":3,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":431,"Remark":null,"AudioList":[{"AudioId":12,"AudioTitle":"禅商：企业家修炼篇","AudioContent":"","AudioSummary":"","AudioCount":10,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309102512_35567.jpg","AudioUrl":null,"ParentId":0},{"AudioId":14,"AudioTitle":"禅与企业管理","AudioContent":"","AudioSummary":"","AudioCount":12,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309104438_89244.jpg","AudioUrl":null,"ParentId":0},{"AudioId":20,"AudioTitle":"帝王之学：企业家的心性修炼","AudioContent":"","AudioSummary":"","AudioCount":12,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309131243_94375.jpg","AudioUrl":null,"ParentId":0},{"AudioId":24,"AudioTitle":"东方文化和谐商道","AudioContent":"","AudioSummary":"","AudioCount":10,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309142001_98742.jpg","AudioUrl":null,"ParentId":0},{"AudioId":47,"AudioTitle":"集团公司管控十大问题","AudioContent":"","AudioSummary":"","AudioCount":12,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180313/20180313115117_51731.jpg","AudioUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":86,"ClassName":"故事会","ParentId":3,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":432,"Remark":null,"AudioList":[{"AudioId":5,"AudioTitle":"99%的人不知道的历史真相","AudioContent":"","AudioSummary":"","AudioCount":23,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180308/20180308171555_76247.jpg","AudioUrl":null,"ParentId":0},{"AudioId":19,"AudioTitle":"邓小平的三起三落","AudioContent":"","AudioSummary":"","AudioCount":34,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309125343_54956.jpg","AudioUrl":null,"ParentId":0},{"AudioId":25,"AudioTitle":"佛教的故事","AudioContent":"","AudioSummary":"","AudioCount":17,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309142711_34656.jpg","AudioUrl":null,"ParentId":0},{"AudioId":37,"AudioTitle":"和珅：跌倒的权力玩家","AudioContent":"","AudioSummary":"","AudioCount":16,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312093819_52857.jpg","AudioUrl":null,"ParentId":0},{"AudioId":38,"AudioTitle":"红顶商人胡雪岩","AudioContent":"","AudioSummary":"","AudioCount":12,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312095021_37773.jpg","AudioUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":19,"ClassName":"文化","ParentId":3,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":433,"Remark":null,"AudioList":[{"AudioId":1,"AudioTitle":"百年浙商（下）","AudioContent":"","AudioSummary":"","AudioCount":27,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309093830_25600.jpg","AudioUrl":null,"ParentId":0},{"AudioId":3,"AudioTitle":"百年浙商（中）","AudioContent":"","AudioSummary":"","AudioCount":28,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309093724_78845.jpg","AudioUrl":null,"ParentId":0},{"AudioId":4,"AudioTitle":"百年浙商（上）","AudioContent":"","AudioSummary":"","AudioCount":24,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309084438_27286.jpg","AudioUrl":null,"ParentId":0},{"AudioId":8,"AudioTitle":"《道德经》的大智慧\u2014\u2014老子·心解","AudioContent":"","AudioSummary":"","AudioCount":20,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180306/20180306104859_34320.jpg","AudioUrl":null,"ParentId":0},{"AudioId":9,"AudioTitle":"《鬼谷子》绝学","AudioContent":"","AudioSummary":"","AudioCount":14,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180308/20180308155447_26468.jpg","AudioUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":69,"ClassName":"医疗,养生","ParentId":3,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":446,"Remark":null,"AudioList":[{"AudioId":48,"AudioTitle":"肩颈腰的自我调理","AudioContent":"","AudioSummary":"","AudioCount":12,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180313/20180313122631_19725.jpg","AudioUrl":null,"ParentId":0},{"AudioId":50,"AudioTitle":"经穴健康一点通\u2014\u2014头部篇","AudioContent":"","AudioSummary":"","AudioCount":15,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180313/20180313125156_85437.jpg","AudioUrl":null,"ParentId":0},{"AudioId":60,"AudioTitle":"孔伯华养生十二字诀","AudioContent":"","AudioSummary":"","AudioCount":12,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180313/20180313164704_16512.jpg","AudioUrl":null,"ParentId":0},{"AudioId":74,"AudioTitle":"企业家养生之道","AudioContent":"","AudioSummary":"","AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180320/20180320150042_45581.jpg","AudioUrl":null,"ParentId":0}],"Childs":{}},{"ClassId":76,"ClassName":"讲唱古文","ParentId":3,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":466,"Remark":null,"AudioList":[{"AudioId":118,"AudioTitle":"讲唱唐诗","AudioContent":"","AudioSummary":"","AudioCount":30,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180323/20180323122235_95339.gif","AudioUrl":null,"ParentId":0},{"AudioId":119,"AudioTitle":"讲唱唐诗2","AudioContent":"","AudioSummary":"","AudioCount":30,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180323/20180323140947_62676.jpg","AudioUrl":null,"ParentId":0},{"AudioId":120,"AudioTitle":"讲唱唐诗3","AudioContent":"","AudioSummary":"","AudioCount":30,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180323/20180323142103_26092.jpg","AudioUrl":null,"ParentId":0},{"AudioId":121,"AudioTitle":"讲唱唐诗4","AudioContent":"","AudioSummary":"","AudioCount":17,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180323/20180323150408_63220.jpg","AudioUrl":null,"ParentId":0},{"AudioId":122,"AudioTitle":"讲唱诗经","AudioContent":"","AudioSummary":"","AudioCount":11,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180323/20180323151754_29465.gif","AudioUrl":null,"ParentId":0}],"Childs":{}}]
     * result : true
     * msg : success
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ClassId : 15
         * ClassName : 评书
         * ParentId : 3
         * SortId : 424
         * AudioList : [{"AudioId":2,"AudioTitle":"包拯：断案如神的青天老爷","AudioContent":"","AudioSummary":"","AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309095459_81666.jpg","AudioUrl":null,"ParentId":0},{"AudioId":10,"AudioTitle":"曹操：乱世奸雄治世能臣","AudioContent":"","AudioSummary":"","AudioCount":15,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309100431_88307.jpg","AudioUrl":null,"ParentId":0},{"AudioId":11,"AudioTitle":"曹雪芹：清朝才子的红楼一梦","AudioContent":"","AudioSummary":"","AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309101447_45131.jpg","AudioUrl":null,"ParentId":0},{"AudioId":16,"AudioTitle":"春秋争霸","AudioContent":"","AudioSummary":"","AudioCount":26,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309111513_43890.jpg","AudioUrl":null,"ParentId":0},{"AudioId":35,"AudioTitle":"汉武帝：铁血雄风大汉天子","AudioContent":"","AudioSummary":"","AudioCount":14,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312085857_85948.jpg","AudioUrl":null,"ParentId":0}]
         */

        private int ClassId;
        private String ClassName;
        private int ParentId;
        private int SortId;
        private List<AudioListBean> AudioList;

        public int getClassId() {
            return ClassId;
        }

        public void setClassId(int ClassId) {
            this.ClassId = ClassId;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String ClassName) {
            this.ClassName = ClassName;
        }

        public int getParentId() {
            return ParentId;
        }

        public void setParentId(int ParentId) {
            this.ParentId = ParentId;
        }
        public int getSortId() {
            return SortId;
        }

        public void setSortId(int SortId) {
            this.SortId = SortId;
        }

        public List<AudioListBean> getAudioList() {
            return AudioList;
        }

        public void setAudioList(List<AudioListBean> AudioList) {
            this.AudioList = AudioList;
        }

        public static class ChildsBean {
        }

        public static class AudioListBean {
            /**
             * AudioId : 2
             * AudioTitle : 包拯：断案如神的青天老爷
             * AudioContent :
             * AudioSummary :
             * AudioCount : 13
             * Hits : 0
             * Author : null
             * StoreUpCount : null
             * Cover : /Public/editor/attached/image/20180309/20180309095459_81666.jpg
             * AudioUrl : null
             * ParentId : 0
             */

            private int AudioId;
            private String AudioTitle;
            private String AudioContent;
            private String AudioSummary;
            private int AudioCount;
            private int Hits;
            private String Cover;
            private String AudioUrl;
            private int ParentId;

            public int getAudioId() {
                return AudioId;
            }

            public void setAudioId(int AudioId) {
                this.AudioId = AudioId;
            }

            public String getAudioTitle() {
                return AudioTitle;
            }

            public void setAudioTitle(String AudioTitle) {
                this.AudioTitle = AudioTitle;
            }

            public String getAudioContent() {
                return AudioContent;
            }

            public void setAudioContent(String AudioContent) {
                this.AudioContent = AudioContent;
            }

            public String getAudioSummary() {
                return AudioSummary;
            }

            public void setAudioSummary(String AudioSummary) {
                this.AudioSummary = AudioSummary;
            }

            public int getAudioCount() {
                return AudioCount;
            }

            public void setAudioCount(int AudioCount) {
                this.AudioCount = AudioCount;
            }

            public int getHits() {
                return Hits;
            }

            public void setHits(int Hits) {
                this.Hits = Hits;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }

            public String getAudioUrl() {
                return AudioUrl;
            }

            public void setAudioUrl(String AudioUrl) {
                this.AudioUrl = AudioUrl;
            }

            public int getParentId() {
                return ParentId;
            }

            public void setParentId(int ParentId) {
                this.ParentId = ParentId;
            }
        }
    }
}

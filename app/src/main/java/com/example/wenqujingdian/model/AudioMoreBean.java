package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Create by kxliu on 2018/12/28
 */
public class AudioMoreBean {

    /**
     * result : true
     * msg :
     * data : [{"pageCount":1,"recordCount":10,"list":[{"AudioId":2,"AudioTitle":"包拯：断案如神的青天老爷","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520560503,"CreateDate":null,"AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309095459_81666.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":10,"AudioTitle":"曹操：乱世奸雄治世能臣","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520561075,"CreateDate":null,"AudioCount":15,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309100431_88307.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":11,"AudioTitle":"曹雪芹：清朝才子的红楼一梦","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520561690,"CreateDate":null,"AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309101447_45131.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":16,"AudioTitle":"春秋争霸","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520565320,"CreateDate":null,"AudioCount":26,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309111513_43890.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":35,"AudioTitle":"汉武帝：铁血雄风大汉天子","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520816340,"CreateDate":null,"AudioCount":14,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312085857_85948.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":36,"AudioTitle":"汉武王朝","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520816972,"CreateDate":null,"AudioCount":30,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312090927_63530.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":39,"AudioTitle":"洪武王朝","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520839488,"CreateDate":null,"AudioCount":36,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312152437_57015.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":58,"AudioTitle":"康乾盛世","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520928566,"CreateDate":null,"AudioCount":40,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180313/20180313160922_50289.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":59,"AudioTitle":"康熙：君临天下六十年","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520930253,"CreateDate":null,"AudioCount":15,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180313/20180313163729_18742.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":81,"AudioTitle":"三国风云","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1521596332,"CreateDate":null,"AudioCount":33,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180321/20180321093849_35265.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null}]}]
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pageCount : 1
         * recordCount : 10
         * list : [{"AudioId":2,"AudioTitle":"包拯：断案如神的青天老爷","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520560503,"CreateDate":null,"AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309095459_81666.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":10,"AudioTitle":"曹操：乱世奸雄治世能臣","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520561075,"CreateDate":null,"AudioCount":15,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309100431_88307.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":11,"AudioTitle":"曹雪芹：清朝才子的红楼一梦","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520561690,"CreateDate":null,"AudioCount":13,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309101447_45131.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":16,"AudioTitle":"春秋争霸","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520565320,"CreateDate":null,"AudioCount":26,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180309/20180309111513_43890.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":35,"AudioTitle":"汉武帝：铁血雄风大汉天子","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520816340,"CreateDate":null,"AudioCount":14,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312085857_85948.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":36,"AudioTitle":"汉武王朝","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520816972,"CreateDate":null,"AudioCount":30,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312090927_63530.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":39,"AudioTitle":"洪武王朝","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520839488,"CreateDate":null,"AudioCount":36,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180312/20180312152437_57015.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":58,"AudioTitle":"康乾盛世","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520928566,"CreateDate":null,"AudioCount":40,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180313/20180313160922_50289.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":59,"AudioTitle":"康熙：君临天下六十年","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1520930253,"CreateDate":null,"AudioCount":15,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180313/20180313163729_18742.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null},{"AudioId":81,"AudioTitle":"三国风云","AudioContent":"","AudioSummary":"","AudioType":"原创","add_time":1521596332,"CreateDate":null,"AudioCount":33,"Hits":0,"Author":null,"StoreUpCount":null,"Cover":"/Public/editor/attached/image/20180321/20180321093849_35265.jpg","AudioUrl":null,"PId":0,"ROWSTAT":null}]
         */

        private int pageCount;
        private int recordCount;
        private List<ListBean> list;

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * AudioId : 2
             * AudioTitle : 包拯：断案如神的青天老爷
             * AudioContent :
             * AudioSummary :
             * AudioType : 原创
             * add_time : 1520560503
             * CreateDate : null
             * AudioCount : 13
             * Hits : 0
             * Author : null
             * StoreUpCount : null
             * Cover : /Public/editor/attached/image/20180309/20180309095459_81666.jpg
             * AudioUrl : null
             * PId : 0
             * ROWSTAT : null
             */

            private int AudioId;
            private String AudioTitle;
            private String AudioContent;
            private String AudioSummary;
            private String AudioType;
            private int add_time;
            private int AudioCount;
            private int Hits;
            private String Cover;

            public int getAudioId() {
                return AudioId;
            }

            public void setAudioId(int AudioId) {
                this.AudioId = AudioId;
            }

            public String getAudioTitle() {
                return AudioTitle;
            }

            public void setAudioTitle(String AudioTitle) {
                this.AudioTitle = AudioTitle;
            }

            public String getAudioContent() {
                return AudioContent;
            }

            public void setAudioContent(String AudioContent) {
                this.AudioContent = AudioContent;
            }

            public String getAudioSummary() {
                return AudioSummary;
            }

            public void setAudioSummary(String AudioSummary) {
                this.AudioSummary = AudioSummary;
            }

            public String getAudioType() {
                return AudioType;
            }

            public void setAudioType(String AudioType) {
                this.AudioType = AudioType;
            }

            public int getAdd_time() {
                return add_time;
            }

            public void setAdd_time(int add_time) {
                this.add_time = add_time;
            }
            public int getAudioCount() {
                return AudioCount;
            }

            public void setAudioCount(int AudioCount) {
                this.AudioCount = AudioCount;
            }

            public int getHits() {
                return Hits;
            }

            public void setHits(int Hits) {
                this.Hits = Hits;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }
        }
    }
}

package com.example.wenqujingdian.model;

/**
 * Create by kxliu on 2018/11/22
 */
public class OrgBean {
    private boolean result;
    private String msg;
    private LocalOrgBean org;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public LocalOrgBean getOrg() {
        return org;
    }

    public void setOrg(LocalOrgBean org) {
        this.org = org;
    }
}

package com.example.wenqujingdian.model;

/**
 * Create by kxliu on 2019/1/8
 * 具体分类子集
 */
public class VideoLocalDetailBean {

    /**
     * VideoId : 42
     * VideoTitle : 唐诗：白云泉 白居易
     * Cover : /Public/editor/attached/image/20180312/20180312144806_18217.jpg
     * VideoUrl : /Public/editor/attached/file/20180312/20180312144820_62143.mp4
     * ParentId : 3
     */

    private String VideoId;
    private String VideoTitle;
    private String Cover;
    private String VideoUrl;
    private String ParentId;

    public String getVideoId() {
        return VideoId;
    }

    public void setVideoId(String VideoId) {
        this.VideoId = VideoId;
    }

    public String getVideoTitle() {
        return VideoTitle;
    }

    public void setVideoTitle(String VideoTitle) {
        this.VideoTitle = VideoTitle;
    }

    public String getCover() {
        return Cover;
    }

    public void setCover(String Cover) {
        this.Cover = Cover;
    }

    public String getVideoUrl() {
        return VideoUrl;
    }

    public void setVideoUrl(String VideoUrl) {
        this.VideoUrl = VideoUrl;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String ParentId) {
        this.ParentId = ParentId;
    }
}

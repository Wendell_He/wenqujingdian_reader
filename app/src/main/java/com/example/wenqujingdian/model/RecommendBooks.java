package com.example.wenqujingdian.model;

import java.util.List;

/**
 * Created by to-explore-future on 2018/10/8
 */
public class RecommendBooks {


    /**
     * result : true
     * code :
     * msg :
     * draw : 1
     * recordsTotal : 19
     * recordsFiltered : 19
     * data : [{"BookId":46972,"BookTitle":"数字学习力","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"电子工业出版社","add_time":1529389813,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"杨杰，梁晓睿著","Cover":"/Public/editor/attached/image/20180619/20180619142848_59792.jpg","BookUrl":"/Public/editor/attached/file/20180619/20180619142957_96143.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"","PublishDate":"2012-01-01","Hits":10,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":46951,"BookTitle":"数字王国奇案\u2014\u2014UFO下蛋了","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"电子工业出版社","add_time":1529386522,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"林海燕著","Cover":"/Public/editor/attached/image/20180619/20180619133447_78730.jpg","BookUrl":"/Public/editor/attached/file/20180619/20180619133508_65527.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"","PublishDate":"2012-02-01","Hits":2,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":48838,"BookTitle":"汤姆叔叔的小屋","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"电子工业出版社","add_time":1527078920,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"(美)斯托夫人著","Cover":"/Public/editor/attached/image/20180523/20180523203446_31005.jpg","BookUrl":"/Public/editor/attached/file/20180523/20180523203501_38545.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"9787121138331","PublishDate":"2011-09-01","Hits":0,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":49606,"BookTitle":"北美校园英语","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"金盾出版社","add_time":1522924380,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"顾颖主编","Cover":"/Public/editor/attached/image/20180405/20180405183202_79315.jpg","BookUrl":"/Public/editor/attached/file/20180405/20180405183241_80666.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"9787508268972","PublishDate":"2011-06-01","Hits":0,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":679,"BookTitle":"清华大学","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"清华大学出版社","add_time":1522421817,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"朱文一，滕静茹，陈瑾羲编著","Cover":"/Public/editor/attached/image/20180330/20180330225655_80216.jpg","BookUrl":"/Public/editor/attached/file/20180330/20180330225620_85728.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"9787302254577","PublishDate":"2011-05-01","Hits":0,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":1587,"BookTitle":"自己拯救自己","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"浙江文艺出版社","add_time":1519015731,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"塞缪尔·斯迈尔斯 著,尘间 译,","Cover":"/Public/editor/attached/image/20180219/20180219124743_30571.jpg","BookUrl":"/Public/editor/attached/file/20180219/20180219124548_60309.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"9787533933456","PublishDate":"","Hits":0,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":1588,"BookTitle":"自己开创未来","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"地震出版社","add_time":1519015979,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"(美) 吉迪恩·鲍威尔","Cover":"/Public/editor/attached/image/20180219/20180219125203_12878.jpg","BookUrl":"/Public/editor/attached/file/20180219/20180219125051_45384.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"7502817425","PublishDate":"","Hits":0,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":4983,"BookTitle":"90后，有点苦有点涩","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"","add_time":1519119667,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"管理员","Cover":"/Public/editor/attached/image/20180220/20180220174053_99033.jpg","BookUrl":"/Public/editor/attached/file/20180404/20180404103009_27295.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"","PublishDate":"","Hits":0,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":5332,"BookTitle":"去香港上大学","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"广西人民出版社","add_time":1522565386,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"李子迟","Cover":"/Public/editor/attached/image/20180401/20180401144940_40690.jpg","BookUrl":"/Public/editor/attached/file/20180401/20180401144925_55705.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"97872190582989","PublishDate":"2007-04-01","Hits":0,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null},{"BookId":12389,"BookTitle":"信息化十讲","BookContent":"","BookSummary":"","PublishId":null,"Publisher":"电子工业出版社","add_time":1519217762,"CreateDate":null,"CreateId":null,"CreateName":null,"IP":null,"BookClass":"0-1-472","Author":"邹生主编","Cover":"/Public/editor/attached/image/20180221/20180221205349_34345.jpg","BookUrl":"/Public/editor/attached/file/20180221/20180221205520_63322.pdf","BookUrl2":null,"BookUrl3":null,"ISBN":"9787121075445","PublishDate":"","Hits":0,"StoreUpCount":0,"IsBold":0,"IsTop":false,"IsNew":false,"IsShow":true,"IsBlurb":true,"IsHot":false,"ROWSTAT":null}]
     * bookUrlPre : http://www.wenqujingdian.com
     */

    private boolean result;
    private String code;
    private String msg;
    private int draw;
    private int recordsTotal;
    private int recordsFiltered;
    private String bookUrlPre;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public String getBookUrlPre() {
        return bookUrlPre;
    }

    public void setBookUrlPre(String bookUrlPre) {
        this.bookUrlPre = bookUrlPre;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * BookId : 46972
         * BookTitle : 数字学习力
         * BookContent :
         * BookSummary :
         * PublishId : null
         * Publisher : 电子工业出版社
         * add_time : 1529389813
         * CreateDate : null
         * CreateId : null
         * CreateName : null
         * IP : null
         * BookClass : 0-1-472
         * Author : 杨杰，梁晓睿著
         * Cover : /Public/editor/attached/image/20180619/20180619142848_59792.jpg
         * BookUrl : /Public/editor/attached/file/20180619/20180619142957_96143.pdf
         * BookUrl2 : null
         * BookUrl3 : null
         * ISBN :
         * PublishDate : 2012-01-01
         * Hits : 10
         * StoreUpCount : 0
         * IsBold : 0
         * IsTop : false
         * IsNew : false
         * IsShow : true
         * IsBlurb : true
         * IsHot : false
         * ROWSTAT : null
         */

        private int BookId;
        private String BookTitle;
        private String BookContent;
        private String BookSummary;
        private String Publisher;
        private int add_time;
        private String BookClass;
        private String Author;
        private String Cover;
        private String BookUrl;
        private String ISBN;
        private String PublishDate;
        private int Hits;
        private int StoreUpCount;
        private int IsBold;
        private boolean IsTop;
        private boolean IsNew;
        private boolean IsShow;
        private boolean IsBlurb;
        private boolean IsHot;

        public int getBookId() {
            return BookId;
        }

        public void setBookId(int BookId) {
            this.BookId = BookId;
        }

        public String getBookTitle() {
            return BookTitle;
        }

        public void setBookTitle(String BookTitle) {
            this.BookTitle = BookTitle;
        }

        public String getBookContent() {
            return BookContent;
        }

        public void setBookContent(String BookContent) {
            this.BookContent = BookContent;
        }

        public String getBookSummary() {
            return BookSummary;
        }

        public void setBookSummary(String BookSummary) {
            this.BookSummary = BookSummary;
        }

        public String getPublisher() {
            return Publisher;
        }

        public void setPublisher(String Publisher) {
            this.Publisher = Publisher;
        }

        public int getAdd_time() {
            return add_time;
        }

        public void setAdd_time(int add_time) {
            this.add_time = add_time;
        }

        public String getBookClass() {
            return BookClass;
        }

        public void setBookClass(String BookClass) {
            this.BookClass = BookClass;
        }

        public String getAuthor() {
            return Author;
        }

        public void setAuthor(String Author) {
            this.Author = Author;
        }

        public String getCover() {
            return Cover;
        }

        public void setCover(String Cover) {
            this.Cover = Cover;
        }

        public String getBookUrl() {
            return BookUrl;
        }

        public void setBookUrl(String BookUrl) {
            this.BookUrl = BookUrl;
        }

        public String getISBN() {
            return ISBN;
        }

        public void setISBN(String ISBN) {
            this.ISBN = ISBN;
        }

        public String getPublishDate() {
            return PublishDate;
        }

        public void setPublishDate(String PublishDate) {
            this.PublishDate = PublishDate;
        }

        public int getHits() {
            return Hits;
        }

        public void setHits(int Hits) {
            this.Hits = Hits;
        }

        public int getStoreUpCount() {
            return StoreUpCount;
        }

        public void setStoreUpCount(int StoreUpCount) {
            this.StoreUpCount = StoreUpCount;
        }

        public int getIsBold() {
            return IsBold;
        }

        public void setIsBold(int IsBold) {
            this.IsBold = IsBold;
        }

        public boolean isIsTop() {
            return IsTop;
        }

        public void setIsTop(boolean IsTop) {
            this.IsTop = IsTop;
        }

        public boolean isIsNew() {
            return IsNew;
        }

        public void setIsNew(boolean IsNew) {
            this.IsNew = IsNew;
        }

        public boolean isIsShow() {
            return IsShow;
        }

        public void setIsShow(boolean IsShow) {
            this.IsShow = IsShow;
        }

        public boolean isIsBlurb() {
            return IsBlurb;
        }

        public void setIsBlurb(boolean IsBlurb) {
            this.IsBlurb = IsBlurb;
        }

        public boolean isIsHot() {
            return IsHot;
        }

        public void setIsHot(boolean IsHot) {
            this.IsHot = IsHot;
        }
    }
}

package com.example.wenqujingdian.model;


import com.chad.library.adapter.base.entity.SectionEntity;

public class VideoSectionBean extends SectionEntity<VideoSectionChildBean> {
    private boolean isMore;
    private String id;

    public VideoSectionBean(boolean isHeader, String header, String id, boolean isMroe) {
        super(isHeader, header);
        this.isMore = isMroe;
        this.id = id;
    }

    public VideoSectionBean(VideoSectionChildBean t) {
        super(t);
    }

    public boolean isMore() {
        return isMore;
    }

    public void setMore(boolean mroe) {
        isMore = mroe;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

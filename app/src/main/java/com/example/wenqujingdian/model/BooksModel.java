package com.example.wenqujingdian.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class BooksModel implements Parcelable {


    private List<BooksBean> books;

    protected BooksModel(Parcel in) {
    }

    public static final Creator<BooksModel> CREATOR = new Creator<BooksModel>() {
        @Override
        public BooksModel createFromParcel(Parcel in) {
            return new BooksModel(in);
        }

        @Override
        public BooksModel[] newArray(int size) {
            return new BooksModel[size];
        }
    };

    public List<BooksBean> getBooks() {
        return books;
    }

    public void setBooks(List<BooksBean> books) {
        this.books = books;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static class BooksBean implements Parcelable{
        /**
         * BookId : 132
         * BookTitle : 缤纷生物
         * Publish : null
         * Author : 管理员
         * ClassId : -
         * ClassName : null
         * Edition : 全民阅读
         * Cover : /Public/editor/attached/image/20180214/20180214151103_24705.jpg
         * BookUrl : /Public/editor/attached/file/20180214/20180214151056_47147.pdf
         * CreateDate : 21:08.4
         */

        private String BookId;
        private String BookTitle;
        private Object Publish;
        private String Author;
        private String ClassId;
        private Object ClassName;
        private String Edition;
        private String Cover;
        private String BookUrl;
        private String CreateDate;

        protected BooksBean(Parcel in) {
            BookId = in.readString();
            BookTitle = in.readString();
            Author = in.readString();
            ClassId = in.readString();
            Edition = in.readString();
            Cover = in.readString();
            BookUrl = in.readString();
            CreateDate = in.readString();
        }

        public static final Creator<BooksBean> CREATOR = new Creator<BooksBean>() {
            @Override
            public BooksBean createFromParcel(Parcel in) {
                return new BooksBean(in);
            }

            @Override
            public BooksBean[] newArray(int size) {
                return new BooksBean[size];
            }
        };

        public String getBookId() {
            return BookId;
        }

        public void setBookId(String BookId) {
            this.BookId = BookId;
        }

        public String getBookTitle() {
            return BookTitle;
        }

        public void setBookTitle(String BookTitle) {
            this.BookTitle = BookTitle;
        }

        public Object getPublish() {
            return Publish;
        }

        public void setPublish(Object Publish) {
            this.Publish = Publish;
        }

        public String getAuthor() {
            return Author;
        }

        public void setAuthor(String Author) {
            this.Author = Author;
        }

        public String getClassId() {
            return ClassId;
        }

        public void setClassId(String ClassId) {
            this.ClassId = ClassId;
        }

        public Object getClassName() {
            return ClassName;
        }

        public void setClassName(Object ClassName) {
            this.ClassName = ClassName;
        }

        public String getEdition() {
            return Edition;
        }

        public void setEdition(String Edition) {
            this.Edition = Edition;
        }

        public String getCover() {
            return Cover;
        }

        public void setCover(String Cover) {
            this.Cover = Cover;
        }

        public String getBookUrl() {
            return BookUrl;
        }

        public void setBookUrl(String BookUrl) {
            this.BookUrl = BookUrl;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(BookId);
            dest.writeString(BookTitle);
            dest.writeString(Author);
            dest.writeString(ClassId);
            dest.writeString(Edition);
            dest.writeString(Cover);
            dest.writeString(BookUrl);
            dest.writeString(CreateDate);
        }
    }
}

package com.example.wenqujingdian.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Create by kxliu on 2018/11/22
 */
public class LocalBookBean implements Parcelable {

    /**
     * BookId : 9
     * BookTitle : 1945～1949：中国共产党与自由主义力量
     * Author : null
     * Publish : null
     * ClassId : 59
     * ClassName : 综合性图书
     * Edition : 全民阅读
     * Cover : /Public/editor/attached/image/20180214/20180214131425_76368.jpg
     * BookUrl : /Public/editor/attached/file/20180214/20180214131431_72438.pdf
     * CreateDate : 2018-11-19 15:57:44.813000000
     */

    private String BookId;
    private String BookTitle;
    private String Author;
    private String Publish;
    private String ClassId;
    private String ClassName;
    private String Edition;
    private String Cover;
    private String BookUrl;
    private String CreateDate;
    private String BookExtension;
    public LocalBookBean(){}

    protected LocalBookBean(Parcel in) {
        BookId = in.readString();
        BookTitle = in.readString();
        Author = in.readString();
        Publish = in.readString();
        ClassId = in.readString();
        ClassName = in.readString();
        Edition = in.readString();
        Cover = in.readString();
        BookUrl = in.readString();
        CreateDate = in.readString();
        BookExtension = in.readString();
    }

    public static final Creator<LocalBookBean> CREATOR = new Creator<LocalBookBean>() {
        @Override
        public LocalBookBean createFromParcel(Parcel in) {
            return new LocalBookBean(in);
        }

        @Override
        public LocalBookBean[] newArray(int size) {
            return new LocalBookBean[size];
        }
    };

    public String getBookId() {
        return BookId;
    }

    public void setBookId(String BookId) {
        this.BookId = BookId;
    }

    public String getBookTitle() {
        return BookTitle;
    }

    public void setBookTitle(String BookTitle) {
        this.BookTitle = BookTitle;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String Author) {
        this.Author = Author;
    }

    public String getPublish() {
        return Publish;
    }

    public void setPublish(String Publish) {
        this.Publish = Publish;
    }

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String ClassId) {
        this.ClassId = ClassId;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String ClassName) {
        this.ClassName = ClassName;
    }

    public String getEdition() {
        return Edition;
    }

    public void setEdition(String Edition) {
        this.Edition = Edition;
    }

    public String getCover() {
        return Cover;
    }

    public void setCover(String Cover) {
        this.Cover = Cover;
    }

    public String getBookUrl() {
        return BookUrl;
    }

    public void setBookUrl(String BookUrl) {
        this.BookUrl = BookUrl;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public String getBookExtension() {
        return BookExtension;
    }

    public void setBookExtension(String BookExtension) {
        this.BookExtension = BookExtension;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(BookId);
        dest.writeString(BookTitle);
        dest.writeString(Author);
        dest.writeString(Publish);
        dest.writeString(ClassId);
        dest.writeString(ClassName);
        dest.writeString(Edition);
        dest.writeString(Cover);
        dest.writeString(BookUrl);
        dest.writeString(CreateDate);
        dest.writeString(BookExtension);
    }
}

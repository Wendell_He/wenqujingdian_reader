package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.bean.BookDetailBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaintDetailActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.iv_detail)
    ImageView iv_detail;

    private BookDetailBean bookDetailBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint_detail);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (intent != null){
            bookDetailBean = intent.getParcelableExtra("bookDetail");
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(bookDetailBean.news_tit);
        initData();
    }

    private void initData() {
        Glide.with(this).load(ApiConfig.API_URL + bookDetailBean.spic).into(iv_detail);
    }

}

package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.bean.BookDetailBean;
import com.example.wenqujingdian.utils.QRCodeUtil;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LocalBookDetailsActivity extends BaseActivity {
    private static final String TAG = "BookDetailsActivity";
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.book_image)
    ImageView mBookImage;
    @BindView(R.id.tv_name)
    TextView mTvName;
    @BindView(R.id.tv_author)
    TextView mTvAuthor;
    @BindView(R.id.btn_read)
    Button mBtnRead;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.qr_iv)
    ImageView qrIv;

    private BookDetailBean bookDetailBean;
    private String path;
    private Unbinder bind;
    private boolean isLand;
//    private boolean isWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        bind = ButterKnife.bind(this);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTitle.setText("详情");
        bookDetailBean = getIntent().getParcelableExtra("bookDetail");
        path = getIntent().getStringExtra("path");

        initView();
        initQRCode();

    }

    private void initQRCode() {
        String url = ApiConfig.API_URL + bookDetailBean.url +
                "?source=wqjd" +
                "&action=insertFavourite" +
                "&favouriteId=" + bookDetailBean.id +
                "&favouriteTitle=" + bookDetailBean.news_tit +
                "&favouriteCover=" + ApiConfig.API_URL + bookDetailBean.spic +
                "&categoryId=" + "1";
        Bitmap qrCodeBitmap = QRCodeUtil.createQRCodeBitmap(url, 280, 280);
        qrIv.setImageBitmap(qrCodeBitmap);
    }

    private void initView() {
        if (findViewById(R.id.tv_land) != null) {
            //横屏显示
            isLand = true;
        } else {
            //竖屏显示
            isLand = false;
        }
        Glide.with(this)
                .load(bookDetailBean.spic)
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
                .fitCenter()
                .into(mBookImage);

        mTvName.setText(bookDetailBean.news_tit);
        mTvAuthor.setText("作者：" + bookDetailBean.author);

        mBtnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String bookTitle = bookDetailBean.news_tit;
                if (TextUtils.isEmpty(bookTitle)){
                    Toast.makeText(LocalBookDetailsActivity.this, "图书名字为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(bookDetailBean.url)){
                    Toast.makeText(LocalBookDetailsActivity.this, "图书本地链接为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                File file = new File(path + bookDetailBean.url);
                if (file.exists()) {
                    Intent intent = new Intent(LocalBookDetailsActivity.this, OpenLocalBookTest.class);
                    intent.putExtra("pdfPath", file.toString());
                    intent.putExtra("bookTitle", bookTitle);
                    intent.putExtra("isLand", isLand);
                    startActivity(intent);
                } else {
                    Toast.makeText(LocalBookDetailsActivity.this, "文件没有找到", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }
}

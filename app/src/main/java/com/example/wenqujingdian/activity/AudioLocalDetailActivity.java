package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.AudioLocalDetailAdapter;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.api.JsonDataManager;
import com.example.wenqujingdian.model.AudioLocalDetailBean;
import com.example.wenqujingdian.utils.ToastUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 具体分类
 */
public class AudioLocalDetailActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.video_detail_recycler)
    RecyclerView recycler;
    @BindView(R.id.smart_refresh)
    SmartRefreshLayout smartRefresh;
    private Unbinder bind;
    private List<AudioLocalDetailBean> audioDetailList;
    private AudioLocalDetailAdapter mAdapter;
    private String audioId;
    private String path;

    private List<AudioLocalDetailBean> localAudioPlay = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        bind = ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        audioId = extras.getString("audioId");
        String videoTitle = extras.getString("audioTitle");
        path = extras.getString("path");
        title.setText(videoTitle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initView();
        initData();
    }

    private void initView() {
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        smartRefresh.setEnableRefresh(false);
        smartRefresh.setEnableLoadMore(false);
    }

    private void initData() {
        getAudioDetailList();
        mAdapter = new AudioLocalDetailAdapter(null);
        mAdapter.setOnItemClickListener(this);
        recycler.setAdapter(mAdapter);
    }

    private void getAudioDetailList() {
        JsonDataManager.getInstance().getAudioDetailData(this, Contants.AUDIOCHILD,
                new JsonDataManager.OnLocalListDataListener<AudioLocalDetailBean>() {
            @Override
            public void onSuccess(List<AudioLocalDetailBean> data) {
                inflaterData(data);
                mAdapter.setNewData(audioDetailList);
            }

            @Override
            public void onFail(String info) {

            }
        });
    }

    private void inflaterData(List<AudioLocalDetailBean > data) {
        audioDetailList = new ArrayList<>();
        for (AudioLocalDetailBean detailBean : data) {
            String parentId = detailBean.getParentId();
            if (audioId.equals(parentId)){
                audioDetailList.add(detailBean);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        AudioLocalDetailBean item = (AudioLocalDetailBean) adapter.getItem(position);
        String urlPath = path + item.getAudioUrl();
        File file = new File(urlPath);
        if (file.exists()){
            if (localAudioPlay.size() > 0)localAudioPlay.clear();
            List<AudioLocalDetailBean> data = mAdapter.getData();
            int index = data.indexOf(item);
            for (int i = index; i < audioDetailList.size(); i++) {
                localAudioPlay.add(audioDetailList.get(i));
            }
            Contants.localPlayAudioList = localAudioPlay;
            Intent intent = new Intent(this, AudioLocalPlayActivity.class);
            intent.putExtra("path",path);
            startActivity(intent);




        }else {
            ToastUtil.show("该文件不存在");
        }

    }
}

package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.FunctionModelAdapter;
import com.example.wenqujingdian.api.JsonDataManager;
import com.example.wenqujingdian.bean.BookDetailBean;
import com.example.wenqujingdian.model.LocalBookBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 首页功能模块跳转页面
 * Create by kxliu on 2018/11/22
 */
public class FunctionModelActivity extends BaseActivity {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.model_recycler)
    RecyclerView recycler;
    @BindView(R.id.empty)
    TextView empty;
    private Unbinder bind;
    private String jsonName;
    private FunctionModelAdapter mAdapter;
    private List<LocalBookBean> dataList;
    private String specificPath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.function_model_layout);
        bind = ButterKnife.bind(this);
        specificPath = JsonDataManager.getInstance().getSpecificPath();
//        specificPath = JsonDataManager.getInstance().getSpecificPath();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        jsonName = intent.getStringExtra("filename");
        title.setText(intent.getStringExtra("title"));

        initView();
        initData();
    }

    private void initData() {
        JsonDataManager.getInstance().getLocalBookData(jsonName,this,
                new JsonDataManager.OnLocalListDataListener<LocalBookBean>() {
                    @Override
                    public void onSuccess(List<LocalBookBean> data) {
                        mAdapter.setNewData(data);
                    }

                    @Override
                    public void onFail(String info) {
                        empty.setText(info);
                        empty.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void initView() {
        GridLayoutManager manager;
        if (findViewById(R.id.tv_book2_land) != null) {
            //横屏显示
            manager = new GridLayoutManager(this, 5);
        } else {
            //竖屏显示
            manager = new GridLayoutManager(this, 3);
        }
        recycler.setLayoutManager(manager);
        mAdapter = new FunctionModelAdapter(dataList, this);
        recycler.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                LocalBookBean item = mAdapter.getItem(position);
                BookDetailBean bookDetailBean = new BookDetailBean();
                Intent intent = new Intent(FunctionModelActivity.this, LocalBookDetailsActivity.class);
                bookDetailBean.id = item.getBookId();
                bookDetailBean.news_tit = item.getBookTitle();
                bookDetailBean.author = item.getAuthor();
                bookDetailBean.jianjie = item.getEdition();
                bookDetailBean.news_source = item.getEdition();
                bookDetailBean.spic = specificPath + item.getCover();
                bookDetailBean.url = item.getBookUrl();
                intent.putExtra("bookDetail", bookDetailBean);
                intent.putExtra("path",specificPath);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }
}

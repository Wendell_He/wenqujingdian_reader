package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.SectionAdapter;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.api.JsonDataManager;
import com.example.wenqujingdian.model.VideoHomeBean;
import com.example.wenqujingdian.model.VideoSectionBean;
import com.example.wenqujingdian.model.VideoSectionChildBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoLocalHomeActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener, SectionAdapter.HeaderCallBack {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.video_recycler)
    RecyclerView videoRecycler;
    @BindView(R.id.empty)
    TextView empty;

    private SectionAdapter sectionAdapter;
    private List<VideoSectionBean> mData = new ArrayList<>();
    private String specificPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_home);
        ButterKnife.bind(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        specificPath = JsonDataManager.getInstance().getSpecificPath();
        initView();
        title.setText(getIntent().getStringExtra("title"));
        initLocalData();
    }

    /**
     * 加载本地数据
     */
    private void initLocalData() {
        JsonDataManager.getInstance().getVideoClassData(Contants.VIDEOCLASS, this, new JsonDataManager.OnLocalDataListener<VideoHomeBean>() {
            @Override
            public void onSuccess(VideoHomeBean data) {
                List<VideoHomeBean.DataBean> data1 = data.getData();
                if (data1 == null || data1.size() == 0) return;
                getData(data1);
            }

            @Override
            public void onFail(String info) {
                empty.setVisibility(View.VISIBLE);
                empty.setText(info);
            }
        });
    }

    private void getData(List<VideoHomeBean.DataBean> data1) {
        for (VideoHomeBean.DataBean dataBean : data1) {
            mData.add(new VideoSectionBean(true, dataBean.getClassName(), String.valueOf(dataBean.getClassId()), true));
            List<VideoHomeBean.DataBean.VideoListBean> videoList = dataBean.getVideoList();
            if (videoList != null && videoList.size() > 0) {
                int size;
                if (findViewById(R.id.home_video_land) != null) {
                    size = videoList.size() > 5 ? 5 : videoList.size();
                    for (int i = 0; i < size; i++) {
                        mData.add(new VideoSectionBean(new VideoSectionChildBean(videoList.get(i).getVideoId(),
                                specificPath + videoList.get(i).getCover(),
                                videoList.get(i).getVideoTitle(),
                                false, videoList)));
                    }

                } else {
                    size = videoList.size() > 3 ? 3 : videoList.size();
                    for (int i = 0; i < size; i++) {
                        mData.add(new VideoSectionBean(new VideoSectionChildBean(videoList.get(i).getVideoId(),
                                specificPath + videoList.get(i).getCover(),
                                videoList.get(i).getVideoTitle(),
                                false, videoList)));
                    }

                }
                sectionAdapter.notifyDataSetChanged();
            }
        }
    }

    private void initView() {
        GridLayoutManager manager;
        if (findViewById(R.id.home_video_land) != null) {
            //横屏显示
            manager = new GridLayoutManager(this, 5);
        } else {
            //竖屏显示
            manager = new GridLayoutManager(this, 3);
        }
        videoRecycler.setLayoutManager(manager);
        sectionAdapter = new SectionAdapter(R.layout.item_book_list2, R.layout.def_section_head, mData);
        sectionAdapter.setOnItemClickListener(this);
        videoRecycler.setAdapter(sectionAdapter);
        sectionAdapter.setHeaderCallBack(this);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (!mData.get(position).isHeader) {
            Bundle bundle = new Bundle();
            VideoSectionBean videoSectionBean = mData.get(position);
            VideoSectionChildBean t = videoSectionBean.t;
            bundle.putString("videoTitle", t.getName());
            bundle.putInt("videoId", t.getId());
            bundle.putString("path",specificPath);
            Intent intent = new Intent(this, VideoLocalDetailActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @Override
    public void headerClick(String id, String title) {
        Intent intent = new Intent(this, VideoLocalMoreActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putInt("id", Integer.valueOf(id));
        bundle.putString("path",specificPath);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}

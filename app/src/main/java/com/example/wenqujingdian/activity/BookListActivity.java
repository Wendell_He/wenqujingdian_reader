package com.example.wenqujingdian.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.BookListAdapter;
import com.example.wenqujingdian.bean.BookDetailBean;
import com.example.wenqujingdian.bean.BookListBean;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookListActivity extends BaseActivity {
    private static final String TAG = "BookListActivity";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;

    private BookListAdapter mAdapter;

    private ProgressDialog progressDialog;

    private String flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        flag = getIntent().getStringExtra("flag");
        String url;
        if (flag.equals("9")) {
            title.setText("党政专题");
            url = "index.php/Jiekou/tushu/lm/439";
            initData(url);
        } else if (flag.equals("10")) {
            title.setText("国学新读");
            url = "index.php/Jiekou/tushu/lm/469";
            initData(url);
        } else if (flag.equals("11")) {
            title.setText("健康养生");
            url = "index.php/Jiekou/tushu/lm/440";
            initData(url);
        } else if (flag.equals("12")) {
            title.setText("创业创新");
            url = "index.php/Jiekou/tushu/lm/441";
            initData(url);
        }else if(flag.equals("113")){
            title.setText("新教育");
            url = "index.php/Jiekou/tushu/lm/471";
            initData(url);
        }

        initView();

    }

    private void initView() {
        final GridLayoutManager manager;
        if (findViewById(R.id.tv_land) != null) {
            Log.i(TAG, "initView: 横屏显示");
            manager = new GridLayoutManager(this, 5);
        } else {
            Log.i(TAG, "initView: 竖屏显示");
            manager = new GridLayoutManager(this, 3);
        }
        recyclerView.setLayoutManager(manager);
        mAdapter = new BookListAdapter(this, null);
        mAdapter.openLoadAnimation();
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Log.i(TAG, "onItemClick: position=" + position);

                BookListBean item = mAdapter.getItem(position);
                BookDetailBean bookDetailBean = new BookDetailBean();
                Intent intent = new Intent(BookListActivity.this, BookDetailsActivity.class);
                bookDetailBean.id = item.id;
                bookDetailBean.news_tit = item.news_tit;
                bookDetailBean.author = item.author;
                bookDetailBean.jianjie = item.jianjie;
                bookDetailBean.news_source = item.news_source;
                bookDetailBean.spic = item.spic;
                bookDetailBean.url = item.url;
                bookDetailBean.classId = item.classId;
                intent.putExtra("bookDetail", bookDetailBean);
                startActivity(intent);
            }
        });


    }

    private void initData(String url) {
        progressDialog.setMessage("加载中...");
        progressDialog.show();
        //获取创业创新图书列表数据
        ViseHttp.GET(url)
                .tag(TAG)
                .setHttpCache(true)
                .request(new ACallback<List<BookListBean>>() {
                    @Override
                    public void onSuccess(List<BookListBean> bookListBeans) {
                        progressDialog.dismiss();
                        if (bookListBeans != null && bookListBeans.size() > 0) {
                            Log.i(TAG, "onSuccess: bookListBeans= " + bookListBeans.size());
                            mAdapter.setNewData(bookListBeans);

                        } else {
                            Log.i(TAG, "onSuccess: bookListBeans is null");
                        }
                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {
                        progressDialog.dismiss();
                        //请求失败，errCode为错误码，errMsg为错误描述
                        Log.i(TAG, "onFail: errCode" + " errMsg=" + errMsg);
                    }
                });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ViseHttp.cancelTag(TAG);
    }
}

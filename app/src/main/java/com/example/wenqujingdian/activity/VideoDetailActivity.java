package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.VideoDetailAdapter;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.model.VideoDetailBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 具体分类
 */
public class VideoDetailActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.video_detail_recycler)
    RecyclerView videoDetailRecycler;
    @BindView(R.id.smart_refresh)
    SmartRefreshLayout smartRefresh;
    private Unbinder bind;
    private static final String TAG = VideoDetailActivity.class.getCanonicalName();
    private List<VideoDetailBean.DataBean.ListBean> videoDetailList;
    private VideoDetailAdapter mAdapter;
    private boolean isRefresh;
    private int currentPage = 1;
    private int videoId;

    /**
     * 待播放的视频集合
     */
    private List<VideoDetailBean.DataBean.ListBean> playVideoList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        bind = ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        videoId = extras.getInt("videoId");
        String videoTitle = extras.getString("videoTitle");
        title.setText(videoTitle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initView();
        initData();
    }

    private void initView() {
        videoDetailRecycler.setLayoutManager(new LinearLayoutManager(this));
        videoDetailRecycler.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
    }

    private void initData() {
        videoDetailList = new ArrayList<>();
        setRefresh();
        getVideoDetailList(1);
        mAdapter = new VideoDetailAdapter(null);
        mAdapter.setOnItemClickListener(this);
        videoDetailRecycler.setAdapter(mAdapter);
    }

    private void getVideoDetailList(int page) {
        ViseHttp.GET(String.format(ApiConfig.VIDEO_CHILD_URL, videoId, page))
                .tag(TAG)
                .setHttpCache(true)
                //配置读取超时时间，单位秒
                .readTimeOut(60)
                //配置写入超时时间，单位秒
                .writeTimeOut(60)
                //配置连接超时时间，单位秒
                .connectTimeOut(60)
                //配置请求失败重试次数
                .retryCount(3)
                //配置请求失败重试间隔时间，单位毫秒
                .retryDelayMillis(1000)
                .request(new ACallback<VideoDetailBean>() {
                    @Override
                    public void onSuccess(VideoDetailBean data) {
                        List<VideoDetailBean.DataBean> data1 = data.getData();
                        if (data1 == null) return;
                        VideoDetailBean.DataBean dataBean = data1.get(0);
                        if (isRefresh) {
                            videoDetailList = dataBean.getList();
                            mAdapter.replaceData(videoDetailList);
                        } else {
                            videoDetailList.addAll(dataBean.getList());
                            mAdapter.addData(dataBean.getList());
                        }
                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {
                        Log.d("cordon", "onFail: " + errCode + "errMsg:" + errMsg);
                    }
                });
    }

    private void setRefresh() {
        smartRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                autoRefresh();
                refreshLayout.finishRefresh(1000);
            }
        });
        smartRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMore();
                refreshLayout.finishLoadMore(1000);
            }
        });
    }

    private void loadMore() {
        isRefresh = false;
        currentPage++;
        getVideoDetailList(currentPage);
    }

    private void autoRefresh() {
        isRefresh = true;
        currentPage = 1;
        getVideoDetailList(currentPage);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bind.unbind();
        ViseHttp.cancelTag(TAG);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        VideoDetailBean.DataBean.ListBean item = (VideoDetailBean.DataBean.ListBean) adapter.getItem(position);
        int index = videoDetailList.indexOf(item);
        if (playVideoList.size() > 0) {
            playVideoList.clear();
        }
        for (int i = index; i < videoDetailList.size(); i++) {
            playVideoList.add(videoDetailList.get(i));
        }
        Contants.videoBean = playVideoList;
        startActivity(new Intent(this, VideoPlayActivity.class));
    }
}

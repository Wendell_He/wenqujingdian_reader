package com.example.wenqujingdian.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.utils.ToastUtil;
import com.example.wenqujingdian.utils.VersionUtils;
import com.vise.utils.assist.StringUtil;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import java.text.AttributedCharacterIterator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdminActivity extends AppCompatActivity {


    @BindView(R.id.register)
    EditText register;
    @BindView(R.id.agency)
    EditText agency;
    @BindView(R.id.info)
    EditText info;

    Intent intent=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        ButterKnife.bind(this);
        intent = getIntent();
    }


    /**
     * 退出软件
     * @param view
     */
    public void exit(View view) {
        finish();
        System.exit(0);
    }

    public void requestShouQuan(View view) {
        String agencyStr = agency.getText().toString();
        String registerStr = register.getText().toString();
        String infoStr = info.getText().toString();

        if (StringUtil.isBlank(agencyStr) || StringUtil.isBlank(registerStr) || StringUtil.isBlank(infoStr)) {
            ToastUtil.show("所有信息都要填写");
            return;
        }

        ViseHttp.GET("wenqu/requestShouQuan")
                .baseUrl(ApiConfig.SERVER_URL)
                .readTimeOut(60)
                //配置写入超时时间，单位秒
                .writeTimeOut(60)
                //配置连接超时时间，单位秒
                .connectTimeOut(60)
                //配置请求失败重试次数
                .retryCount(3)
                //配置请求失败重试间隔时间，单位毫秒
                .retryDelayMillis(1000)
                .addParam("deviceCode", intent.getStringExtra("deviceCode"))
                .addParam("register", registerStr)
                .addParam("agency", agencyStr)
                .addParam("info", infoStr)
                .addParam("appName", "阅读机")
                .addParam("version", VersionUtils.getVersionName(this))
                .request(new ACallback<String>() {
                    @Override
                    public void onSuccess(final String data) {
                        ToastUtil.show(data);
                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {
                        Log.d("cordon", "onFail: " + errMsg);
                    }
                });
    }
}
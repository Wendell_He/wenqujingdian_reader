package com.example.wenqujingdian.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.example.wenqujingdian.receiver.HomeReceiver;

public class BaseActivity extends AppCompatActivity {

    public static HomeReceiver innerReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        innerReceiver = new HomeReceiver();                                                        //注册广播
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(innerReceiver, intentFilter);
    }

    @Override
    protected void onPause() {                                                                                     //重写recent键方法
        super.onPause();
        for (int j = 0; j < 50; j++) {
            ActivityManager activityManager = (ActivityManager) getApplicationContext()
                    .getSystemService(Context.ACTIVITY_SERVICE);
            activityManager.moveTaskToFront(getTaskId(), 0);
        }
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(innerReceiver);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("广播", "广播未注册");
        }
        super.onDestroy();
    }
}

package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.AudioLocalMoreAdapter;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.api.JsonDataManager;
import com.example.wenqujingdian.model.AudioLocalMoreBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Create by kxliu on 2018/12/26
 */
public class AudioLocalMoreActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.video_detail_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.smart_refresh)
    SmartRefreshLayout smartRefresh;
    private Unbinder bind;

    private AudioLocalMoreAdapter mAdapter;
    private List<AudioLocalMoreBean> moreList;
    private String audioId;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        bind = ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        String videoTitle = extras.getString("title");
        audioId = extras.getString("id");
        path = extras.getString("path");

        title.setText(videoTitle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initView();
        initData();
    }

    private void initData() {
        getVideoMoreList();
        mAdapter = new AudioLocalMoreAdapter(this, null,path);
        mAdapter.openLoadAnimation();
        mAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(mAdapter);
    }

    private void getVideoMoreList() {
        JsonDataManager.getInstance().getLocalAudioList(Contants.AUDIOLIST, this,
                new JsonDataManager.OnLocalListDataListener<AudioLocalMoreBean>() {
            @Override
            public void onSuccess(List<AudioLocalMoreBean> data) {
                inflaterData(audioId,data);
                mAdapter.setNewData(moreList);
            }
            @Override
            public void onFail(String info) {

            }
        });
    }

    /**
     * 根据classID过滤掉数据
     * @param data  分类数据
     */
    private void inflaterData(String id,List<AudioLocalMoreBean> data) {
        moreList = new ArrayList<>();
        for (AudioLocalMoreBean listBean : data) {
            String cLassId = listBean.getCLassId();
            if (cLassId.equals(id)){
                moreList.add(listBean);
            }
        }
    }

    private void initView() {
        GridLayoutManager manager;
        if (findViewById(R.id.land_tv) != null) {
            manager = new GridLayoutManager(this, 5);
        } else {
            manager = new GridLayoutManager(this, 3);
        }
        recyclerView.setLayoutManager(manager);
        smartRefresh.setEnableLoadMore(false);
        smartRefresh.setEnableRefresh(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bind != null)
        bind.unbind();
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        AudioLocalMoreBean item = (AudioLocalMoreBean) adapter.getItem(position);
        Bundle bundle = new Bundle();
        bundle.putString("audioTitle", item.getAudioTitle());
        bundle.putString("audioId",item.getAudioId());
        bundle.putString("path",path);
        Intent intent = new Intent(this, AudioLocalDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}

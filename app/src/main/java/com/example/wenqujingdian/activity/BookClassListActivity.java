package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.LocalBookClassListAdapter;
import com.example.wenqujingdian.bean.BookDetailBean;
import com.example.wenqujingdian.bigdata.BigData;
import com.example.wenqujingdian.model.LocalBookBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookClassListActivity extends BaseActivity {
    private static final String TAG = "BookClassListActivity";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;
//    private ProgressDialog progressDialog;
    private LocalBookClassListAdapter mAdapter;


    private List<LocalBookBean> bookList;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_sort_list);
        ButterKnife.bind(this);
       // bookList = getIntent().getParcelableArrayListExtra("list");
        //静态成员防止数据过大
        bookList = BigData.bookByClassIdList;
        String typeName = getIntent().getStringExtra("typeName");
        path = getIntent().getStringExtra("path");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(typeName);
//        progressDialog = new ProgressDialog(this);


        initView();
//        initData();


    }

    private void initView() {
        GridLayoutManager manager;
        if (findViewById(R.id.tv_sort_list_land) != null) {
            //横屏显示
            manager = new GridLayoutManager(this, 5);
        } else {
            //竖屏显示
            manager = new GridLayoutManager(this, 3);
        }

        recyclerView.setLayoutManager(manager);
        mAdapter = new LocalBookClassListAdapter(this, bookList,path);
        mAdapter.openLoadAnimation();
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Log.i(TAG, "onItemClick: position=" + position);

                LocalBookBean item = mAdapter.getItem(position);
                BookDetailBean bookDetailBean = new BookDetailBean();
                Intent intent = new Intent(BookClassListActivity.this, LocalBookDetailsActivity.class);
                bookDetailBean.id = item.getBookId();
                bookDetailBean.news_tit = item.getBookTitle();
                bookDetailBean.author = item.getAuthor();
                bookDetailBean.jianjie = item.getEdition();
                bookDetailBean.news_source = item.getEdition();
                bookDetailBean.spic = path + item.getCover() ;
                bookDetailBean.url = /*path + */item.getBookUrl();
                intent.putExtra("bookDetail", bookDetailBean);
                intent.putExtra("path",path);
                startActivity(intent);

              /*  Intent intent = new Intent(BookSortListActivity.this, BookReadActivity.class);
                intent.putExtra("pdfurl", mAdapter.getItem(position).url);
                startActivity(intent);*/
            }
        });


    }

//    private void initData() {
//        progressDialog.setMessage("加载中...");
//        progressDialog.show();
//        //获取图书列表数据
//        ViseHttp.GET("index.php/Jiekou/tushu/lm/" + typeId)
//                .tag(TAG)
//                .setHttpCache(true)
//                //配置读取超时时间，单位秒
//                .readTimeOut(60)
//                //配置写入超时时间，单位秒
//                .writeTimeOut(60)
//                //配置连接超时时间，单位秒
//                .connectTimeOut(60)
//                //配置请求失败重试次数
//                .retryCount(3)
//                //配置请求失败重试间隔时间，单位毫秒
//                .retryDelayMillis(1000)
//                .request(new ACallback<List<BookListBean>>() {
//                    @Override
//                    public void onSuccess(List<BookListBean> bookListBeans) {
//                        progressDialog.dismiss();
//                        if (bookListBeans != null && bookListBeans.size() > 0) {
//                            Log.i(TAG, "onSuccess: bookListBeans= " + bookListBeans.size());
//                            mAdapter.setNewData(bookListBeans);
//
//                        } else {
//                            Log.i(TAG, "onSuccess: bookListBeans is null");
//                        }
//
//
//                    }
//
//                    @Override
//                    public void onFail(int errCode, String errMsg) {
//                        progressDialog.dismiss();
//                        //请求失败，errCode为错误码，errMsg为错误描述
//                        Log.i(TAG, "onFail: errCode" + " errMsg=" + errMsg);
//                    }
//                });
//
//
//    }
}

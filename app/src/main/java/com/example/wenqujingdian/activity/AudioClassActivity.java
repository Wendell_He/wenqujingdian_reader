package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.AudioSectionAdapter;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.model.AudioClassBean;
import com.example.wenqujingdian.model.AudioSectionBean;
import com.example.wenqujingdian.model.AudioSectionChildBean;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AudioClassActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener,
        AudioSectionAdapter.HeaderCallBack {

    private static final String TAG = AudioClassActivity.class.getCanonicalName();
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.video_recycler)
    RecyclerView videoRecycler;

    private AudioSectionAdapter sectionAdapter;
    private List<AudioSectionBean> mData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_home);
        ButterKnife.bind(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(getIntent().getStringExtra("flag"));

        initView();
        initData();
    }

    private void initData() {
        ViseHttp.GET(String.format(ApiConfig.AUDIO_CLASS_URL))
                .tag(TAG)
                .baseUrl(ApiConfig.ORG_URL)
                .setHttpCache(true)
                //配置读取超时时间，单位秒
                .readTimeOut(60)
                //配置写入超时时间，单位秒
                .writeTimeOut(60)
                //配置连接超时时间，单位秒
                .connectTimeOut(60)
                //配置请求失败重试次数
                .retryCount(3)
                //配置请求失败重试间隔时间，单位毫秒
                .retryDelayMillis(1000)
                .request(new ACallback<AudioClassBean>() {
                    @Override
                    public void onSuccess(AudioClassBean data) {
                        List<AudioClassBean.DataBean> data1 = data.getData();
                        if (data1 == null || data1.size() == 0)return;
                        getData(data1);
                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {

                    }
                });
    }

    private void getData(List<AudioClassBean.DataBean> data1) {
        for (AudioClassBean.DataBean dataBean : data1) {
            mData.add(new AudioSectionBean(true,dataBean.getClassName(),String.valueOf(dataBean.getClassId()),true));
            List<AudioClassBean.DataBean.AudioListBean> audioList = dataBean.getAudioList();
            if (audioList != null && audioList.size() > 0){
                int size;
                if (findViewById(R.id.home_video_land) != null){
                    size = audioList.size() > 5 ? 5 : audioList.size();
                    for (int i = 0; i < size; i++) {
                        mData.add(new AudioSectionBean(new AudioSectionChildBean(audioList.get(i).getAudioId(),
                                ApiConfig.API_URL + audioList.get(i).getCover(),
                                audioList.get(i).getAudioTitle(),
                                false, audioList)));
                    }
                }else {
                    size = audioList.size() > 3 ? 3 : audioList.size();
                    for (int i = 0; i < size; i++) {
                        mData.add(new AudioSectionBean(new AudioSectionChildBean(audioList.get(i).getAudioId(),
                                ApiConfig.API_URL + audioList.get(i).getCover(),
                                audioList.get(i).getAudioTitle(),
                                false, audioList)));
                    }
                }
                sectionAdapter.notifyDataSetChanged();
            }
        }
    }

    private void initView() {
        GridLayoutManager manager;
        if (findViewById(R.id.home_video_land) != null) {
            //横屏显示
            manager = new GridLayoutManager(this, 5);
        } else {
            //竖屏显示
            manager = new GridLayoutManager(this, 3);
        }
        videoRecycler.setLayoutManager(manager);
        sectionAdapter = new AudioSectionAdapter(R.layout.item_book_list2,R.layout.def_section_head,mData);
        sectionAdapter.setOnItemClickListener(this);
        videoRecycler.setAdapter(sectionAdapter);
        sectionAdapter.setHeaderCallBack(this);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (!mData.get(position).isHeader) {
            Bundle bundle = new Bundle();
            AudioSectionBean audioSectionBean = mData.get(position);
            AudioSectionChildBean t = audioSectionBean.t;
            bundle.putString("audioTitle", t.getName());
            bundle.putInt("audioId",t.getId());
            bundle.putString("imageUrl",t.getImg());
            Intent intent = new Intent(this, AudioDetailActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @Override
    public void headerClick(String id, String title) {
        Intent intent = new Intent(this, AudioMoreActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putInt("id", Integer.valueOf(id));
        intent.putExtras(bundle);
        startActivity(intent);
    }
}

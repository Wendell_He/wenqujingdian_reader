package com.example.wenqujingdian.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.bean.PaperHomeBean;
import com.example.wenqujingdian.paperAdapter.HomeAdapter;
import com.example.wenqujingdian.paperAdapter.MenuAdapter;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by kxliu on 2019/2/25
 */
public class PaperHomeActivity extends BaseActivity {

    private static final String TAG = PaperHomeActivity.class.getCanonicalName();
    private List<String> menuList = new ArrayList<>();
    private List<PaperHomeBean.DataBean> homeList = new ArrayList<>();
    private List<Integer> showTitle;

    private ListView lv_menu;
    private ListView lv_home;

    private TextView tv_title;
//    private TextView tv_more;
    private int currentItem;

    private MenuAdapter menuAdapter;
    private HomeAdapter homeAdapter;

    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_pager);
        progressDialog = new ProgressDialog(this);
        initView();
        initData();
    }

    private void initData() {
        showDialog();
        showTitle = new ArrayList<>();
        getPaperClassData();
    }

    /**
     * 获取报纸分类数据
     */
    private void getPaperClassData() {
        ViseHttp.GET(ApiConfig.ORG_URL + ApiConfig.PAPER_HOME_URL)
                .tag(TAG)
                .setHttpCache(true)
                //配置读取超时时间，单位秒
                .readTimeOut(60)
                //配置写入超时时间，单位秒
                .writeTimeOut(60)
                //配置连接超时时间，单位秒
                .connectTimeOut(60)
                //配置请求失败重试次数
                .retryCount(3)
                //配置请求失败重试间隔时间，单位毫秒
                .retryDelayMillis(1000)
                .request(new ACallback<PaperHomeBean>() {
                    @Override
                    public void onSuccess(PaperHomeBean data) {
                        progressDialog.dismiss();
                        if (null != data){
                            for (int i = 0; i < data.getData().size(); i++) {
                                PaperHomeBean.DataBean dataBean = data.getData().get(i);
                                String className = dataBean.getClassName();
                                if (className.equals("中央")){
                                    menuList.add(0,className);
                                    showTitle.add(0);
                                    homeList.add(0,dataBean);
                                }else {
                                    menuList.add(className);
                                    showTitle.add(i);
                                    homeList.add(dataBean);
                                }

                            }
                            tv_title.setText(menuList.get(0));

                            menuAdapter.notifyDataSetChanged();
                            homeAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {
                        progressDialog.dismiss();
                        Toast.makeText(PaperHomeActivity.this,errMsg,Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showDialog() {
        progressDialog.setMessage("加载中...");
        progressDialog.show();
    }

    private void initView() {
        lv_menu = (ListView) findViewById(R.id.lv_menu);
        tv_title = (TextView) findViewById(R.id.tv_titile);
        lv_home = (ListView) findViewById(R.id.lv_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.title)).setText("报纸");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        tv_more = ((TextView) findViewById(R.id.more));
//        tv_more.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String item = (String) menuAdapter.getItem(menuAdapter.getSelectItem());
//                Toast.makeText(PaperHomeActivity.this,item,Toast.LENGTH_SHORT).show();
//            }
//        });

        menuAdapter = new MenuAdapter(this,menuList);
        lv_menu.setAdapter(menuAdapter);

        homeAdapter = new HomeAdapter(this,homeList);
        lv_home.setAdapter(homeAdapter);

        lv_menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                menuAdapter.setSelectItem(position);
                menuAdapter.notifyDataSetInvalidated();
                tv_title.setText(menuList.get(position));
                lv_home.setSelection(showTitle.get(position));
            }
        });


        lv_home.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int scrollState;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.scrollState = scrollState;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    return;
                }

                int current = showTitle.indexOf(firstVisibleItem);
                if (currentItem != current && current >= 0) {
                    currentItem = current;
                    String text = menuList.get(currentItem);
                    tv_title.setText(text);
                    menuAdapter.setSelectItem(currentItem);
                    menuAdapter.notifyDataSetInvalidated();
                }
            }
        });
    }

}

package com.example.wenqujingdian.activity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wenqujingdian.R;
import com.example.wenqujingdian.utils.QRCodeUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Create by kxliu on 2019/12/29
 */
public class DownloadActivity extends BaseActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_android)
    ImageView ivAndroid;
    @BindView(R.id.iv_ios)
    ImageView ivIos;
    private Unbinder bind;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        bind = ButterKnife.bind(this);
        tv = findViewById(R.id.tv);
        SpannableString textSpanned1 = new SpannableString("浏览器扫描二维码下载安卓版本App");
        textSpanned1.setSpan(new ForegroundColorSpan(Color.RED),
                0, 3, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tv.setText(textSpanned1);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText("下载");
        initAndroidQRCode();
        initIOSQRCode();
    }

    private void initAndroidQRCode() {
        String url = "http://dl.wenqujingdian.com/dushu-new.apk";
        Bitmap qrCodeBitmap = QRCodeUtil.createQRCodeBitmap(url, 280, 280);
        ivAndroid.setImageBitmap(qrCodeBitmap);
    }

    private void initIOSQRCode() {
        String url = "https://apps.apple.com/cn/app/%E6%96%87%E6%9B%B2%E7%BB%8F%E5%85%B8/id1448246005";
        Bitmap qrCodeBitmap = QRCodeUtil.createQRCodeBitmap(url, 280, 280);
        ivIos.setImageBitmap(qrCodeBitmap);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bind != null) {
            bind.unbind();
        }
    }
}

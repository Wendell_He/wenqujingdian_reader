package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.bifan.txtreaderlib.main.TxtConfig;
import com.bifan.txtreaderlib.ui.HwTxtPlayActivity;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.utils.ToastUtil;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import java.io.File;

public class OpenLocalBookTest extends AppCompatActivity {

    private PDFView pdfView;
//    private TxtReaderView txtView;
    private String bookTitle;
    private String filePath;
    private boolean isLand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_local_book_test);

        Intent intent = getIntent();
        filePath = intent.getStringExtra("pdfPath");
        bookTitle = intent.getStringExtra("bookTitle");
        isLand = intent.getBooleanExtra("isLand",false);
        initView();
        startReadBook(filePath);
    }

    private void startReadBook(String filePath) {
        if (filePath != null && !filePath.equals("")){
            if (filePath.endsWith(".pdf")){
//                if (txtView.getVisibility() != View.GONE){
//                    txtView.setVisibility(View.GONE);
//                }
//                if (pdfView.getVisibility() == View.VISIBLE){
//                    pdfView.setVisibility(View.VISIBLE);
//                }
                File pdfFile = new File(filePath);
                if (!isLand){
                    pdfView.fromFile(pdfFile)
                            .defaultPage(0)
//                        .onPageChange(this)
                            .enableAnnotationRendering(true)
//                        .onLoad(this)
                            .scrollHandle(new DefaultScrollHandle(this))
                            .spacing(0) // in dp
//                        .onPageError(this)
                            .pageFitPolicy(FitPolicy.BOTH)
                            //增加屏幕渲染
                            .enableAntialiasing(true)
                            .swipeHorizontal(true)
                            .pageSnap(true)
                            .autoSpacing(true)
                            .pageFling(true)
                            .load();
                }else {
                    pdfView.fromFile(pdfFile)
                            .defaultPage(0)
                            .pageFitPolicy(FitPolicy.HEIGHT)
                            .enableSwipe(true) // allows to block changing pages using swipe //允许阻止改变页面使用刷卡
                            .swipeHorizontal(true)
                            .enableDoubletap(true)
                            .fitEachPage(true)
                            .enableAnnotationRendering(true)
                            .load();
                }

            }else if (filePath.endsWith(".txt")){
                TxtConfig.saveIsOnVerticalPageMode(OpenLocalBookTest.this,false);
                HwTxtPlayActivity.loadTxtFile(this,filePath);
                finish();
            }else {
                ToastUtil.show("文件格式错误");
            }
        }else {
            ToastUtil.show("找不到文件");
        }
    }

    private void initView() {
        View back = findViewById(R.id.open_local_book_test_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView bookName = findViewById(R.id.local_book_name);
        bookName.setText(bookTitle);
        pdfView = findViewById(R.id.open_local_book_test);
//        txtView = findViewById(R.id.local_book_txt_view);
    }

}

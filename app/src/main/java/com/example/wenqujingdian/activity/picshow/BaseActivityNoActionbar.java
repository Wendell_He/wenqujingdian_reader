package com.example.wenqujingdian.activity.picshow;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.example.wenqujingdian.R;


public abstract class BaseActivityNoActionbar extends AppCompatActivity {

    /**
     * 通知栏背景颜色
     */
    private int statusBackgroundClor = R.color.colorPrimaryDark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		long s=System.currentTimeMillis();
        setTranslucentStatus(translucentStatusEnable(), translucentNavigationBarEnable());
        setSystemBarTintColor();
        initLayout();
        initPerData(savedInstanceState);
        initView();
        initData();
//		long e=System.currentTimeMillis();
//		AndroidUtil.showToast(this, e-s+"");
    }

    protected void setSystemBarTintColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
//      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (translucentStatusEnable())
                window.setStatusBarColor(getResources().getColor(statusBackgroundClor));
            if (translucentNavigationBarEnable())
                window.setNavigationBarColor(getResources().getColor(statusBackgroundClor));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //使用SystemBarTint库使4.4版本状态栏变色，需要先将状态栏设置为透明
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            if (translucentStatusEnable()) {
                tintManager.setStatusBarTintEnabled(true);
                tintManager.setStatusBarTintResource(statusBackgroundClor);
            }
            if (translucentNavigationBarEnable()) {
                tintManager.setNavigationBarTintEnabled(true);
                tintManager.setNavigationBarTintResource(statusBackgroundClor);
            }
        }
    }

    /**
     * 是否设置通知栏颜色
     */
    protected boolean statusBarTintEnable() {
        return true;
    }

    /**
     * 是否设置導航栏颜色
     */
    protected boolean navigationBarTintEnable() {
        return true;
    }

    /**
     * 是否开启透明导航栏
     */
    protected boolean translucentStatusEnable() {
        return true;
    }

    /**
     * 是否开启透明导航栏
     */
    protected boolean translucentNavigationBarEnable() {
        return true;
    }


    /**
     * 设置通知栏是否变色
     */
    protected void setTranslucentStatus(boolean status, boolean navigationBar) {
        if (!status & !navigationBar)
            return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (status)
                window.setStatusBarColor(Color.TRANSPARENT);
            if (navigationBar)
                window.setNavigationBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            if (status)
                window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (navigationBar)
                window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
    }

    /**
     * 设置通知栏背景颜色
     **/
    protected void setStatusBgColor(int statusBgColor) {
        this.statusBackgroundClor = statusBgColor;
    }


    @Override
    public void finish() {
        super.finish();
    }

    /**
     * 子类自己去初始化自己的layout
     */
    protected abstract void initLayout();

    /**
     * 初始化数据
     */
    protected abstract void initPerData(Bundle savedInstanceState);

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 初始化控件
     */
    protected abstract void initView();

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }

    public void hideInputMethod() {
        View view = getWindow().peekDecorView();
        if (view != null && view.getWindowToken() != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}

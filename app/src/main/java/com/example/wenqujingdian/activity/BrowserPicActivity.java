package com.example.wenqujingdian.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.wenqujingdian.MyViewPager;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.activity.picshow.BaseActivityNoActionbar;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.bean.FileBean;

import java.io.File;
import java.util.List;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;


/**
 * 瀏覽圖片
 */
public class BrowserPicActivity extends BaseActivityNoActionbar {

    private MyViewPager viewPager;
    private TextView indicator;
    private List<FileBean> imgList = Contants.playBeans;
    private int index;

    @Override
    protected boolean translucentStatusEnable() {
        return true;
    }

    @Override
    protected boolean translucentNavigationBarEnable() {
        return true;
    }

    @Override
    protected boolean statusBarTintEnable() {
        return false;
    }

    @Override
    protected boolean navigationBarTintEnable() {
        return false;
    }

    public static Intent createIntent(Context context, int position) {
        Intent intent = new Intent(context, BrowserPicActivity.class);
        intent.putExtra("index", position);
        return intent;
    }


    @Override
    protected void initLayout() {
        setContentView(R.layout.activity_browser_pic);
    }


    @Override
    protected void initPerData(Bundle savedInstanceState) {
        index = getIntent().getIntExtra("index", 0);
        if (imgList == null || imgList.isEmpty()) {
            finish();
            return;
        }
    }

    @Override
    protected void initData() {
        if (imgList != null) {
            if (index > imgList.size()) {
                index = 0;
            }
            index = index < 0 ? 0 : index;
            indicator.setText((index + 1) + "/" + imgList.size());
            viewPager.setCurrentItem(index);
        }
    }

    @Override
    protected void initView() {
        indicator = (TextView) findViewById(R.id.indicator);
        viewPager = (MyViewPager) findViewById(R.id.viewpager);
        viewPager.setViewPagerEnable(true);
        viewPager.setAdapter(new ViewPagerAdapter(this));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                indicator.setText((position + 1) + "/" + imgList.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            viewPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
//            viewPager.setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Contants.playBeans = null;
    }

    class ViewPagerAdapter extends PagerAdapter {
        public ViewPagerAdapter(Context context) {
        }

        @Override
        public int getCount() {
            return imgList.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = LayoutInflater.from(BrowserPicActivity.this).inflate(R.layout.view_photoview, null);
            final PhotoView img = (PhotoView) view.findViewById(R.id.img);
            final ImageView tem = (ImageView) view.findViewById(R.id.tem);

            img.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
                @Override
                public void onPhotoTap(View view, float x, float y) {
//                    toggle();
                    finish();
                }

                @Override
                public void onOutsidePhotoTap() {
                    finish();
                }
            });

            Glide.with(BrowserPicActivity.this).load(new File(imgList.get(position).getFilePath())).addListener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    img.setZoomable(false);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    tem.setVisibility(View.GONE);
                    img.setZoomable(true);
                    return false;
                }
            }).into(img);
            img.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            container.addView(view);
            return view;
        }
    }


}

package com.example.wenqujingdian.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.BookModelListAdapter;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.bean.BookDetailBean;
import com.example.wenqujingdian.model.BookModelBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookSortListActivity extends BaseActivity {
    private static final String TAG = "BookSortListActivity";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.smart_refresh)
    SmartRefreshLayout smartRefresh;
    private ProgressDialog progressDialog;

    private BookModelListAdapter mAdapter;

    private List<BookModelBean.DataBean.ListBean> bookList;
    private boolean isRefresh;
    private int currentPage;
    private int typeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_sort_list);
        ButterKnife.bind(this);
        typeId = getIntent().getIntExtra("typeId", -1);
        String typeName = getIntent().getStringExtra("typeName");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(typeName);
        progressDialog = new ProgressDialog(this);


        initView();
        initData(typeId);


    }

    private void initView() {
        GridLayoutManager manager;
        if (findViewById(R.id.tv_sort_list_land) != null) {
            //横屏显示
            manager = new GridLayoutManager(this, 5);
        } else {
            //竖屏显示
            manager = new GridLayoutManager(this, 3);
        }

        recyclerView.setLayoutManager(manager);
        mAdapter = new BookModelListAdapter(this, null);
        mAdapter.openLoadAnimation();
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Log.i(TAG, "onItemClick: position=" + position);

                BookModelBean.DataBean.ListBean item = mAdapter.getItem(position);
                BookDetailBean bookDetailBean = new BookDetailBean();
                Intent intent = new Intent(BookSortListActivity.this, BookDetailsActivity.class);
                bookDetailBean.id = String.valueOf(item.getBookId());
                bookDetailBean.news_tit = item.getBookTitle();
                bookDetailBean.author = item.getAuthor();
//                bookDetailBean.jianjie = item.jianjie;
                bookDetailBean.news_source = item.getPublisher();
                bookDetailBean.spic = item.getCover();
                bookDetailBean.url = item.getBookUrl();
                bookDetailBean.classId = item.getClassId();
                intent.putExtra("bookDetail", bookDetailBean);
                startActivity(intent);

              /*  Intent intent = new Intent(BookSortListActivity.this, BookReadActivity.class);
                intent.putExtra("pdfurl", mAdapter.getItem(position).url);
                startActivity(intent);*/
            }
        });


    }

    private void initData(int typeId) {
        progressDialog.setMessage("加载中...");
        progressDialog.show();
        setRefresh();
        bookList = new ArrayList<>();
        getClassBook(typeId, currentPage);

    }

    private void setRefresh() {
        smartRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                autoRefresh();
                refreshLayout.finishRefresh(1000);
            }
        });
        smartRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMore();
                refreshLayout.finishLoadMore(1000);
            }
        });
    }

    //获取分类下的图书
    private void getClassBook(int typeId, int currentPage) {

        //获取图书列表数据
        ViseHttp.GET(String.format(ApiConfig.YUN_CLASS_DETAIL, typeId, currentPage))
                .tag(TAG)
                .setHttpCache(true)
                //配置读取超时时间，单位秒
                .readTimeOut(60)
                //配置写入超时时间，单位秒
                .writeTimeOut(60)
                //配置连接超时时间，单位秒
                .connectTimeOut(60)
                //配置请求失败重试次数
                .retryCount(3)
                //配置请求失败重试间隔时间，单位毫秒
                .retryDelayMillis(1000)
                .request(new ACallback<BookModelBean>() {
                    @Override
                    public void onSuccess(BookModelBean bookListBeans) {
                        progressDialog.dismiss();
                        if (bookListBeans != null) {
                            List<BookModelBean.DataBean> data = bookListBeans.getData();

                            if (data != null && data.size() > 0) {
                                BookModelBean.DataBean dataBean = data.get(0);
                                if (isRefresh) {
                                    bookList = dataBean.getList();
                                    mAdapter.replaceData(bookList);
                                } else {
                                    bookList.addAll(dataBean.getList());
                                    mAdapter.addData(dataBean.getList());
                                }

                            }
                        }

                    }
                    @Override
                    public void onFail(int errCode, String errMsg) {
                        progressDialog.dismiss();
                        //请求失败，errCode为错误码，errMsg为错误描述
                        Log.i(TAG, "onFail: errCode" + " errMsg=" + errMsg);
                    }
                });

    }

    private void loadMore() {
        isRefresh = false;
        currentPage++;
        getClassBook(typeId, currentPage);
    }

    private void autoRefresh() {
        isRefresh = true;
        currentPage = 0;
        getClassBook(typeId, currentPage);
    }
}

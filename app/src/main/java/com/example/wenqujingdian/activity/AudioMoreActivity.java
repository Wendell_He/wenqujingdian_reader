package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.AudioMoreAdapter;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.model.AudioMoreBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Create by kxliu on 2018/12/26
 */
public class AudioMoreActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.video_detail_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.smart_refresh)
    SmartRefreshLayout smartRefresh;
    private Unbinder bind;

    private AudioMoreAdapter mAdapter;
    private List<AudioMoreBean.DataBean.ListBean> moreList;
    private int audioId;

    private boolean isRefresh;
    private int currentPage = 1;
    private static final String TAG = AudioMoreActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        bind = ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        String videoTitle = extras.getString("title");
        audioId = extras.getInt("id");
        title.setText(videoTitle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initView();
        initData();
    }

    private void initData() {
        moreList = new ArrayList<>();
        setRefresh();
        getVideoMoreList(1);
        mAdapter = new AudioMoreAdapter(this, null);
        mAdapter.openLoadAnimation();
        mAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(mAdapter);
    }

    private void getVideoMoreList(int pageIndex) {
        ViseHttp.GET(String.format(ApiConfig.AUDIO_MORE_URL, audioId, pageIndex))
                .tag(TAG)
                .baseUrl(ApiConfig.ORG_URL)
                .setHttpCache(true)
                //配置读取超时时间，单位秒
                .readTimeOut(60)
                //配置写入超时时间，单位秒
                .writeTimeOut(60)
                //配置连接超时时间，单位秒
                .connectTimeOut(60)
                //配置请求失败重试次数
                .retryCount(3)
                //配置请求失败重试间隔时间，单位毫秒
                .retryDelayMillis(1000)
                .request(new ACallback<AudioMoreBean>() {
                    @Override
                    public void onSuccess(AudioMoreBean data) {
                        List<AudioMoreBean.DataBean> data1 = data.getData();
                        if (data1 == null) return;
                        AudioMoreBean.DataBean dataBean = data1.get(0);
                        if (isRefresh) {
                            moreList = dataBean.getList();
                            mAdapter.replaceData(moreList);
                        } else {
                            moreList.addAll(dataBean.getList());
                            mAdapter.addData(dataBean.getList());
                        }
                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {
                        Log.d("cordon", "onFail: " + errCode + "errMsg:" + errMsg);
                    }
                });
    }

    private void setRefresh() {
        smartRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                autoRefresh();
                refreshLayout.finishRefresh(1000);
            }
        });
        smartRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMore();
                refreshLayout.finishLoadMore(1000);
            }
        });
    }

    private void loadMore() {
        isRefresh = false;
        currentPage++;
        getVideoMoreList(currentPage);
    }

    private void autoRefresh() {
        isRefresh = true;
        currentPage = 1;
        getVideoMoreList(currentPage);
    }

    private void initView() {
        GridLayoutManager manager;
        if (findViewById(R.id.land_tv) != null) {
            manager = new GridLayoutManager(this, 5);
        } else {
            manager = new GridLayoutManager(this, 3);
        }
        recyclerView.setLayoutManager(manager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bind != null)
        bind.unbind();
        ViseHttp.cancelTag(TAG);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        AudioMoreBean.DataBean.ListBean item = (AudioMoreBean.DataBean.ListBean) adapter.getItem(position);
        Bundle bundle = new Bundle();
        bundle.putString("audioTitle", item.getAudioTitle());
        bundle.putInt("audioId",item.getAudioId());
        bundle.putString("imageUrl",item.getCover());
        Intent intent = new Intent(this, AudioDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}

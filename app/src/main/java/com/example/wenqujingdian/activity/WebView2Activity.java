package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.utils.MD5Util;
import com.example.wenqujingdian.utils.ToastUtil;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebView2Activity extends BaseActivity {
    private static final String TAG = "WebViewActivity";
    @BindView(R.id.webView)
    WebView webView;
    private String flag;
    private String linkAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       /* if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }*/

        initView();

    }


    private String webUrl = "";

    private void initView() {

        flag = getIntent().getStringExtra("flag");
        linkAddress = getIntent().getStringExtra("linkAddress");
        if (linkAddress != null) {
            initWebView(linkAddress);
            return;
        }

        switch (flag) {
            case "1":
                String url = getIntent().getStringExtra("url");
//                webUrl = "http://m.wenqujingdian.com/pages/download/";
                initWebView("http://m.wenqujingdian.com" + url);
                break;
            case "2":
                webUrl = ApiConfig.POLICY_URL;
                initWebView(webUrl);
                break;
            case "3":
                Log.i(TAG, "onViewClicked: 风彩展示");
                webUrl = "http://www.wenqujingdian.com/index.php/Fengcai";
                initWebView(webUrl);
                break;
            case "13":
                Log.i(TAG, "onViewClicked: 阅读活动");
                //http://wqjd.wowxue.com/   http://www.wenqujingdian.com/index.php/Huodong
                webUrl = "http://www.wenqujingdian.com/index.php/Tushu/index/lm/469.shtml";
                initWebView(webUrl);
                break;
           /* case "4":
                Log.i(TAG, "onViewClicked: 图书");
                webUrl = "http://wenqujingdian.com/index.php/Tushu";
                break;*/
            case "5"://期刊
                webUrl = "http://vertical.591adb.com/wqjd.html";
//                if (findViewById(R.id.tv_land) == null) {
//                    Log.i(TAG, "onViewClicked: 期刊 竖屏");
//                    webUrl = "http://vertical.183read.cc/wqjd.html";
//                    //webUrl = "http://yuedu.183read.cc/organization/wqjd.html?user_id=123";
//                } else {
//                    Log.i(TAG, "onViewClicked: 期刊 横屏");
////                    webUrl = "http://screen.183read.cc/chinesetwo/category/index/app_key/fed82efeba2963ddb349a351726a33b2";
//                    webUrl = "http://vertical.183read.cc/wqjd.html";
//                }
                initWebView(webUrl);
                break;
            case "6":
                Log.i(TAG, "onViewClicked: 音频");
                webUrl = "http://wenqujingdian.com/index.php/Yinpin";
                //shouQuan();
                initWebView(webUrl);
                break;
            case "7":
                Log.i(TAG, "onViewClicked: 报纸");
                webUrl = "http://wenqujingdian.com/index.php/Baozhi";
                initWebView(webUrl);
                break;
            case "8":
                Log.i(TAG, "onViewClicked: 视频");
                webUrl = "http://wenqujingdian.com/index.php/Video";
                initWebView(webUrl);
                break;
            case "9":
                Log.i(TAG, "onViewClicked: 党政专题");
                webUrl = "http://wenqujingdian.com/index.php/Tushu/index/lm/439.shtml";
                initWebView(webUrl);
                break;
            case "10":
                Log.i(TAG, "onViewClicked: 国学新读");
                webUrl = "http://wenqujingdian.com/index.php/Guoxue";
                initWebView(webUrl);
                break;
            case "11":
                Log.i(TAG, "onViewClicked: 健康养生");
                webUrl = "http://wenqujingdian.com/index.php/Tushu/index/lm/440.shtml";
                initWebView(webUrl);
                break;
            case "12":
                Log.i(TAG, "onViewClicked: 创业创新");
                webUrl = "http://wenqujingdian.com/index.php/Tushu/index/lm/441.shtml";
                initWebView(webUrl);
                break;

            case "23"://书香校园版 纠错宝
//                webUrl = "http://ceping.wenqujingdian.com/pailei/step1.html";
                webUrl = "http://ceping.wenqujingdian.com/api.aspx";
                initWebView(webUrl);
                break;
            case "24"://书香校园版 新教学
                webUrl = "http://www.wenqujingdian.com/index.php/Tushu/index/lm/471.shtml";
                initWebView(webUrl);
                break;
            case "25"://书香校园版 微课
                webUrl = "http://wqjd.wowxue.com/";
                initWebView(webUrl);
                break;
            case "55":
                webUrl = "http://wenqujingdian.com/index.php/Tushu/index/lm/472.shtml";
                initWebView(webUrl);
                break;
            case "111":
                webUrl = "http://wqjd.80community.com/api.aspx";
                initWebView(webUrl);
                break;
            case "112":
                webUrl = "http://wqjd.wowxue.com/";
                initWebView(webUrl);
                break;
            case "113":
                webUrl = "http://www.wenqujingdian.com/index.php/Tushu/index/lm/471.shtml";
                initWebView(webUrl);
                break;
            case "1057":
                webUrl = "http://wap.cnki.net/touch/web/guide";
                initWebView(webUrl);
                break;
            case "1058":
                webUrl = "http://www.baidu.com";
                initWebView(webUrl);
                break;

        }


    }

    private String App_key = "fed82efeba2963ddb349a351726a33b2";
    private String Identify_key = "3d6c567b5ea821a17df9ecd26a42a98e";
    private int Type_id = 1;
    private String userId = "123";
    private String sign_code;

    private void shouQuan() {
        ViseHttp.CONFIG().baseUrl("http://api.183read.cc");

        sign_code = MD5Util.getMd5(MD5Util.getMd5(Identify_key) + MD5Util.getMd5(userId));
        //Log.i(TAG, "initView: sign_code=" + sign_code);


        String url = "/open_api/rest2.php?act=module.user.session.great&param={" +
                "\"identify_key\":" + "\"" + Identify_key + "\"," +
                "\"app_key\":" + "\"" + App_key + "\"," +
                "\"user_id\":" + "\"" + userId + "\"," +
                "\"sign_code\":" + "\"" + sign_code + "\"," +
                "\"type_id\":" + "\"" + Type_id + "\"" +
                "}";


        ViseHttp.GET(url)
                .tag(TAG)
                //.addParam("username", strPhone)
                // .addParam("password", strPassword)
                .request(new ACallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        // NiceDialogUtils.dismissNiceDialog();
                        Log.i(TAG, "onSuccess: result=" + result);

                        webUrl = "http://wenqujingdian.com/index.php/Yinpin?user_id=" + userId;
                        initWebView(webUrl);
                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {
                        // NiceDialogUtils.dismissNiceDialog();
                        //请求失败，errCode为错误码，errMsg为错误描述
                        ToastUtil.showToast("errCode=" + errCode + " errMsg=" + errMsg);
                    }
                });
    }

    private void startSystemExplorer(Uri uri) {

        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    //private List<String> loadHistoryUrls = new ArrayList<String>();

    private void initWebView(final String webUrl) {


        webView.loadUrl(webUrl);
        Log.i(TAG, webUrl+"-----------------");
        WebSettings webSettings = webView.getSettings();

        webSettings.setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }
        webSettings.setMediaPlaybackRequiresUserGesture(true);
        webSettings.setAllowFileAccess(true); //设置可以访问文件
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true); //支持通过JS打开新窗口
        // 开启支持视频
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setInitialScale(100);   //100代表不缩放


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "shouldOverrideUrlLoading: url=" + url);
                //loadHistoryUrls.add(url);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                //view.loadUrl(String.format(Locale.CHINA, "javascript:document.body.style.paddingTop='%fpx'; void 0", DensityUtil.px2dp(webView.getPaddingTop())));
                //ToastUtil.showToast(url);
                if (url.contains("vertical.591adb.cn/journal")) {
                   // ToastUtil.show("注入语音");
                    view.loadUrl("javascript:" +
                            "var scripts = document.createElement(\"script\");\n" +
                            "scripts.textContent = 'var btn = \\'&nbsp;&nbsp;&nbsp;<button id=\"playBtn\">语音朗读</button>\\';\\n' +\n" +
                            "    'var title = document.getElementsByClassName(\"ja-title\")[0];\\n' +\n" +
                            "    'console.log(title.innerHTML);\\n' +\n" +
                            "    'title.innerHTML += btn;\\n' +\n" +
                            "    '//添加监听\\n' +\n" +
                            "    'let playbtn = document.getElementById(\"playBtn\");\\n' +\n" +
                            "    'playbtn.onclick = function () {\\n' +\n" +
                            "    '    //获取文本\\n' +\n" +
                            "    '    var p = document.getElementsByClassName(\"content\")[0].getElementsByTagName(\"p\")[1];\\n' +\n" +
                            "    '    let str = p.innerHTML.replace(RegExp(\"<br>\", \"g\"), \\'\\');\\n' +\n" +
                            "    '    //字符分段,定义每段字数\\n' +\n" +
                            "    '    let count = 0;\\n' +\n" +
                            "    '    let textNum = 200;\\n' +\n" +
                            "    '    var strArr = []; var n = textNum; for (var i = 0, l = str.length; i < l/n; i++) { var a = str.slice(n*i, n*(i+1)); strArr.push(a); } console.log(strArr);\\n' +\n" +
                            "    '    play();\\n' +\n" +
                            "    '    function play() {\\n' +\n" +
                            "    '        //分割完毕字符串\\n' +\n" +
                            "    '        let text = strArr[count++];\\n' +\n" +
                            "    '        var url = \"http://tsn.baidu.com/text2audio?cuid=22983786&lan=zh&tok=24.bd158572eaa83267ff097c7f104ea950.2592000.1612666746.282335-22983786&ctp=1&per=0&spd=5&tex=\" + encodeURI(text);\\n' +\n" +
                            "    '        var audio = new Audio(url);\\n' +\n" +
                            "    '        audio.load();\\n' +\n" +
                            "    '        audio.play();\\n' +\n" +
                            "    '        console.log(url)\\n' +\n" +
                            "    '        audio.oncanplay = function () {\\n' +\n" +
                            "    '            if (count<strArr.length){\\n' +\n" +
                            "    '                let time = audio.duration*1000;\\n' +\n" +
                            "    '                setTimeout(function () {\\n' +\n" +
                            "    '                    play()\\n' +\n" +
                            "    '                },time)\\n' +\n" +
                            "    '                console.log(time + \"秒后播放:\" + text);\\n' +\n" +
                            "    '            }\\n' +\n" +
                            "    '        }\\n' +\n" +
                            "    '    }\\n' +\n" +
                            "    '\\n' +\n" +
                            "    '}';\n" +
                            "document.body.appendChild(scripts);");



                }
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            if (flag.equals("7")) {
                super.onBackPressed();
            } else {
                webView.goBack();
            }

        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.destroy();
        }

    }
}

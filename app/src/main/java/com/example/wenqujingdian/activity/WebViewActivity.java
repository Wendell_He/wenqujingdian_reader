package com.example.wenqujingdian.activity;

import android.nfc.Tag;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wenqujingdian.CustomerWebView;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.utils.LogUtils;
import com.example.wenqujingdian.utils.ToastUtil;


public class WebViewActivity extends AppCompatActivity {

    private CustomerWebView progressWebview;
    private Toolbar mToolbar;
    private TextView toolbar_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_webview);

        initView();

    }

    private void initView() {
        String url = getIntent().getStringExtra("url");
        String title = getIntent().getStringExtra("title");
        mToolbar = findViewById(R.id.toolbar);
        toolbar_tv = findViewById(R.id.title);
        progressWebview = findViewById(R.id.web_view);
        progressWebview.loadUrl(url);
        setUpToolbar(title);
        WebViewClient webViewClient = new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //loadHistoryUrls.add(url);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                //view.loadUrl(String.format(Locale.CHINA, "javascript:document.body.style.paddingTop='%fpx'; void 0", DensityUtil.px2dp(webView.getPaddingTop())));
//                ToastUtil.showToast("webView1_URL  ~~~ : " + url);
                view.loadUrl("javascript:" +
                        "var scripts = document.createElement(\"script\");\n" +
                        "scripts.textContent = 'function getText(dom) {\\n' +\n" +
                        "    '    var index = 0, html = dom.innerHTML;\\n' +\n" +
                        "    '    while (dom.children.length && index < dom.children.length) {\\n' +\n" +
                        "    '        var chtml = dom.children[index].outerHTML;\\n' +\n" +
                        "    '        html = dom.innerHTML.replace(chtml, \\'\\');\\n' +\n" +
                        "    '        index++;\\n' +\n" +
                        "    '    }\\n' +\n" +
                        "    '    return html;\\n' +\n" +
                        "    '}\\n' +\n" +
                        "    '\\n' +\n" +
                        "    'var array = \\'\\';\\n' +\n" +
                        "    'var tags = document.body.getElementsByTagName(\\'p\\');\\n' +\n" +
                        "    'for (var i = 0; i < tags.length; i++) {\\n' +\n" +
                        "    '    array += getChineseAndPoint(getText(tags[i]));\\n' +\n" +
                        "    '}\\n' +\n" +
                        "    'console.log(array);\\n' +\n" +
                        "    '//匹配网页\\n' +\n" +
                        "    '//添加按钮的元素\\n' +\n" +
                        "    'var btn = \\'&nbsp;&nbsp;&nbsp;<img src=\"http://image.wenqujingdian.com/m/wq/images/read.jpg\" id=\"playBtn\" style=\"width: 40px;height: 40px;right: 0px;top: 100px;position:absolute;z-index:100;\"/>\\';\\n' +\n" +
                        "    'document.getElementsByTagName(\\'html\\')[0].innerHTML += btn;\\n' +\n" +
                        "    'let str = array;\\n' +\n" +
                        "    '\\n' +\n" +
                        "    '//添加监听\\n' +\n" +
                        "    'let playbtn = document.getElementById(\"playBtn\");\\n' +\n" +
                        "    'playbtn.onclick = function () {\\n' +\n" +
                        "    '//获取文本\\n' +\n" +
                        "    '//字符分段,定义每段字数\\n' +\n" +
                        "    '    let count = 0;\\n' +\n" +
                        "    '    let textNum = 200;\\n' +\n" +
                        "    '    var strArr = [];\\n' +\n" +
                        "    '    var n = textNum;\\n' +\n" +
                        "    '    for (var i = 0, l = str.length; i < l / n; i++) {\\n' +\n" +
                        "    '        var a = str.slice(n * i, n * (i + 1));\\n' +\n" +
                        "    '        strArr.push(a);\\n' +\n" +
                        "    '    }\\n' +\n" +
                        "    '    console.log(strArr);\\n' +\n" +
                        "    '//分割完毕字符串\\n' +\n" +
                        "    '    let text = strArr[count++];\\n' +\n" +
                        "    '    var url = \"http://tsn.baidu.com/text2audio?cuid=22983786&lan=zh&tok=24.bd158572eaa83267ff097c7f104ea950.2592000.1612666746.282335-22983786&ctp=1&per=0&spd=5&tex=\" + encodeURI(text);\\n' +\n" +
                        "    '    var audio = new Audio(url);\\n' +\n" +
                        "    '    audio.load();\\n' +\n" +
                        "    '    audio.play();\\n' +\n" +
                        "    '    console.log(url)\\n' +\n" +
                        "    '    audio.oncanplay = function () {\\n' +\n" +
                        "    '        if (count < strArr.length) {\\n' +\n" +
                        "    '            let time = audio.duration * 1000;\\n' +\n" +
                        "    '            setTimeout(function () {\\n' +\n" +
                        "    '                play()\\n' +\n" +
                        "    '            }, time)\\n' +\n" +
                        "    '            console.log(time + \"毫秒后播放:\" + text);\\n' +\n" +
                        "    '        }\\n' +\n" +
                        "    '    }\\n' +\n" +
                        "    '};\\n' +\n" +
                        "    '\\n' +\n" +
                        "    'function getChineseAndPoint(strValue) {\\n' +\n" +
                        "    '//匹配中文字符以及这些中文标点符号 。 ？ ！ ， 、 ； ： “ ” ‘ \\' （ ） 《 》 〈 〉 【 】 『 』 「 」 ﹃ ﹄ 〔 〕 … — ～ ﹏ ￥\\n' +\n" +
                        "    '    var reg = /[\\u4e00-\\u9fa5|\\u3002|\\uff1f|\\uff01|\\uff0c|\\u3001|\\uff1b|\\uff1a|\\u201c|\\u201d|\\u2018|\\u2019|\\uff08|\\uff09|\\u300a|\\u300b|\\u3008|\\u3009|\\u3010|\\u3011|\\u300e|\\u300f|\\u300c|\\u300d|\\ufe43|\\ufe44|\\u3014|\\u3015|\\u2026|\\u2014|\\uff5e|\\ufe4f|\\uffe5]/g;\\n' +\n" +
                        "    '    var tempValue = strValue.match(reg); //匹配结果，可能为空，还需要进行判断\\n' +\n" +
                        "    '    var resultStr = \"\";\\n' +\n" +
                        "    '    if (tempValue != null) {\\n' +\n" +
                        "    '        resultStr = tempValue.join(\"\");\\n' +\n" +
                        "    '    }\\n' +\n" +
                        "    '    return resultStr; //返回匹配的字符串\\n' +\n" +
                        "    '};';\n" +
                        "document.body.appendChild(scripts);");
            }
        };
        progressWebview.setWebViewClient(webViewClient);

    }

    private void setUpToolbar(String title) {
        mToolbar.setTitle("");
        toolbar_tv.setText(title);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   if (progressWebview.canGoBack()) {
                    progressWebview.goBack();//返回上一页面
                } else {
                    finish();
                }*/
                progressWebview.destroy();
                finish();
            }
        });

    }

    /**
     * 方法描述：改写物理按键——返回的逻辑
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (progressWebview.canGoBack()) {
                progressWebview.goBack();//返回上一页面
                return true;
            } else {
                finish();
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}

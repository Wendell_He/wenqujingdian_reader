package com.example.wenqujingdian.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import com.dou361.ijkplayer.widget.PlayStateParams;
import com.dou361.ijkplayer.widget.PlayerView;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.model.VideoLocalDetailBean;

import tv.danmaku.ijk.media.player.IMediaPlayer;

/**
 * 视频播放
 */
public class LocalVideoPlayActivity extends BaseActivity {

    private PlayerView playerView;

    private int videoIndex;
    private VideoLocalDetailBean listBean;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_paly);
        path = getIntent().getStringExtra("path");
        if (path != null){
            listBean = Contants.LocalVideoBean.get(videoIndex);
            setupPlayer(listBean.getVideoTitle(), path + listBean.getVideoUrl());
        }
    }

    private void setupPlayer(String title, String url) {
//        String uriString = ApiConfig.API_URL + url;
        playerView = new PlayerView(this);
        if (findViewById(R.id.land_tv) != null){
            playerView.setOnlyFullScreen(true);
        }
                playerView.setTitle(title)
                .setScaleType(PlayStateParams.fitparent)
                .forbidTouch(false)
                .hideSteam(true)
//                .setForbidDoulbeUp(true)
                .hideMenu(true)
                .hideRotation(true)
//                .hideCenterPlayer(true)
                .setProcessDurationOrientation(PlayStateParams.PROCESS_CENTER)
                .setPlaySource(url)
//                .setChargeTie(true, 60)
                .startPlay();
        playerView.getFullScreenView().setVisibility(View.GONE);

        playerView.setOnInfoListener(new IMediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(IMediaPlayer iMediaPlayer, int i, int i1) {
                if (i == PlayStateParams.STATE_COMPLETED && videoIndex != Contants.videoBean.size() - 1) {
                    listBean = Contants.LocalVideoBean.get(++videoIndex);
                    setupPlayer(listBean.getVideoTitle(), path + listBean.getVideoUrl());
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (playerView != null) {
            playerView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (playerView != null) {
            playerView.onPause();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (playerView != null) {
            playerView.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onBackPressed() {
        if (playerView != null && playerView.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (playerView != null) {
            playerView.onDestroy();
        }
        Contants.LocalVideoBean = null;
    }
}

package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.VideoLocalDetailAdapter;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.api.JsonDataManager;
import com.example.wenqujingdian.model.VideoLocalDetailBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 具体分类
 */
public class VideoLocalDetailActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.video_detail_recycler)
    RecyclerView videoDetailRecycler;
    @BindView(R.id.smart_refresh)
    SmartRefreshLayout smartRefresh;
    private Unbinder bind;
    private List<VideoLocalDetailBean> videoDetailList;
    private VideoLocalDetailAdapter mAdapter;
    private String path;

    /**
     * 待播放的视频集合
     */
    private List<VideoLocalDetailBean> playVideoList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        bind = ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        int videoId = extras.getInt("videoId");
        String videoTitle = extras.getString("videoTitle");
        path = extras.getString("path");
        title.setText(videoTitle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initView();
        initData(videoId);
    }

    private void initView() {
        videoDetailRecycler.setLayoutManager(new LinearLayoutManager(this));
        videoDetailRecycler.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        smartRefresh.setEnableRefresh(false);
        smartRefresh.setEnableLoadMore(false);
    }

    private void initData(int videoId) {
        getLocalData(videoId);
        mAdapter = new VideoLocalDetailAdapter(null);
        mAdapter.setOnItemClickListener(this);
        videoDetailRecycler.setAdapter(mAdapter);
    }

    private void getLocalData(final int videoId) {
       JsonDataManager.getInstance().getVideoChildList(this, Contants.VIDEOCHILD, new JsonDataManager.OnLocalListDataListener<VideoLocalDetailBean>() {
           @Override
           public void onSuccess(List<VideoLocalDetailBean> data) {
               inflaterData(videoId,data);
               mAdapter.setNewData(videoDetailList);
           }

           @Override
           public void onFail(String info) {

           }
       });
    }

    private void inflaterData(int videoId, List<VideoLocalDetailBean> data) {
        videoDetailList = new ArrayList<>();
        for (VideoLocalDetailBean detailBean : data) {
            String parentId = detailBean.getParentId();
            if (String.valueOf(videoId).equals(parentId)){
                videoDetailList.add(detailBean);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        VideoLocalDetailBean item = (VideoLocalDetailBean) adapter.getItem(position);
        int index = videoDetailList.indexOf(item);
        if (playVideoList.size() > 0){
            playVideoList.clear();
        }
        for (int i = index; i < videoDetailList.size(); i++) {
            playVideoList.add(videoDetailList.get(i));
        }
        Contants.LocalVideoBean = playVideoList;
        Intent intent = new Intent(this, LocalVideoPlayActivity.class);
        intent.putExtra("path",path);
        startActivity(intent);

    }
}

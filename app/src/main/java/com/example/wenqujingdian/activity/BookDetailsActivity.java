package com.example.wenqujingdian.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.bean.BookDetailBean;
import com.example.wenqujingdian.utils.QRCodeUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BookDetailsActivity extends BaseActivity {
    private static final String TAG = "BookDetailsActivity";
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.book_image)
    ImageView mBookImage;
    @BindView(R.id.tv_name)
    TextView mTvName;
    @BindView(R.id.tv_author)
    TextView mTvAuthor;
    @BindView(R.id.btn_read)
    Button mBtnRead;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
//    @BindView(R.id.webView)
//    WebView mWebView;
    @BindView(R.id.qr_iv)
    ImageView qrIv;
    private BookDetailBean bookDetailBean;
    private Unbinder unbinder;
    private boolean isLand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        unbinder = ButterKnife.bind(this);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTitle.setText("详情");
        bookDetailBean = getIntent().getParcelableExtra("bookDetail");

        initView();
        initQRCode();
    }

    private void initView() {

        if (findViewById(R.id.tv_land) != null) {
            //横屏显示
           isLand = true;
        } else {
            //竖屏显示
            isLand = false;
        }

        Glide.with(this)
                .load(ApiConfig.APP_URL + bookDetailBean.spic)
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
                .fitCenter()
                .into(mBookImage);

        mTvName.setText(bookDetailBean.news_tit);
        mTvAuthor.setText("作者：" + bookDetailBean.author);

        Log.i(TAG, "initView: bookId=" + bookDetailBean.id);
//        mWebView.getSettings().setJavaScriptEnabled(true);
//        mWebView.loadUrl(ApiConfig.APP_URL + "index.php/Jiekou/getimg/id/" + bookDetailBean.id);
        mBtnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookDetailsActivity.this, BookReadActivity.class);
                intent.putExtra("pdfurl", bookDetailBean.url);
                intent.putExtra("isLand",isLand);
                startActivity(intent);
            }
        });

    }

    private void initQRCode() {
        //http://www.wenqujingdian.com/Public/editor/attached/file/2/A2015980.pdf?source=wqjd&action=insertFavourite&favouriteId=62596&favouriteTitle=赚钱真的很简单（1）&favouriteCover=http://www.wenqujingdian.com/Public/editor/attached/image/FM/fm15/c_340846.jpg&categoryId=1
        String url = ApiConfig.API_URL + bookDetailBean.url +
                "?source=wqjd" +
                "&action=insertFavourite" +
                "&favouriteId=" + bookDetailBean.id +
                "&favouriteTitle=" + bookDetailBean.news_tit +
                "&favouriteCover=" + ApiConfig.API_URL + bookDetailBean.spic +
                "&categoryId=" + "1";
        Bitmap qrCodeBitmap = QRCodeUtil.createQRCodeBitmap(url, 280, 280);
        qrIv.setImageBitmap(qrCodeBitmap);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}

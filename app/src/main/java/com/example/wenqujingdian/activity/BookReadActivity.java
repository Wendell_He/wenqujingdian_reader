package com.example.wenqujingdian.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bifan.txtreaderlib.ui.HwTxtPlayActivity;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.utils.ToastUtil;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;
import com.vise.xsnow.http.mode.DownProgress;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookReadActivity extends BaseActivity {
    private static final String TAG = "BookReadActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;

    String pdfurl;
    @BindView(R.id.pdfView)
    PDFView pdfView;

    String fielPath;
    @BindView(R.id.tip)
    TextView tip;
    /*  @BindView(R.id.activity_hwtxtplay_readerView)
      TxtReaderView mTxtReaderView;*/
    private ProgressDialog progressDialog;
    private String fileNmae;

    private int fileType = 0; //1：pdf ;2:txt; 0:格式错误

    private boolean isDestroy;
    private boolean isDownLoadFinish;
    private boolean isLand;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_read);
        ButterKnife.bind(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText("阅读");
        progressDialog = new ProgressDialog(this);
        Intent intent = getIntent();
        if (intent == null) return;
        pdfurl = intent.getStringExtra("pdfurl");
        isLand = intent.getBooleanExtra("isLand",false);
        Log.i(TAG, "onCreate: pdfurl=" + pdfurl);

        if (!TextUtils.isEmpty(pdfurl)) {
            tip.setVisibility(View.GONE);
            fileNmae = pdfurl;
            if (!TextUtils.isEmpty(fileNmae)) {
                if (fileNmae.contains(".pdf")) {
                    Log.i(TAG, "onCreate: pdf格式的文件");
                    fileType = 1;
                    int index = fileNmae.lastIndexOf("/");
                    fileNmae = fileNmae.substring(index + 1, fileNmae.length() - 4);
                    Log.i(TAG, "onCreate: fileNmae=" + fileNmae);

                } else if (fileNmae.contains(".txt")) {
                    Log.i(TAG, "onCreate: txt格式的文件");
                    fileType = 2;
                    int index = fileNmae.lastIndexOf("/");
                    fileNmae = fileNmae.substring(index + 1, fileNmae.length() - 4);
                    Log.i(TAG, "onCreate: fileNmae=" + fileNmae);

                } else {
                    fileType = 0;
                    Toast.makeText(this, "文件格式错误", Toast.LENGTH_SHORT).show();
                    return;
                }
            }


            fielPath = "sdcard" + Environment.getExternalStorageDirectory().getPath();
            Log.i(TAG, "onCreate: fielPath=" + fielPath);
            File file = new File(fielPath + "/download", fileNmae);
            if (file.exists()) {
                Log.i(TAG, "onCreate: 文件已存在");
                switch (fileType) {
                    case 1:
                        openPDFDocument(file);
                        break;
                    case 2:
                        loadTxtFile(file.getPath());
                        break;
                    case 0:
                        Log.i(TAG, "onCreate: 这里应该执行不到");
                        Toast.makeText(this, "格式文件错误", Toast.LENGTH_SHORT).show();
                        break;
                }


            } else {
                Log.i(TAG, "onCreate: 还没有改文件去下载 ");

                switch (fileType) {
                    case 1:
                        downPDFFile();
                        break;
                    case 2:
                        downTxtFile();
                        break;
                    case 0:
                        Toast.makeText(this, "格式文件错误,无法下载", Toast.LENGTH_SHORT).show();
                        break;
                }

            }


        } else {
            tip.setVisibility(View.VISIBLE);
        }


    }

    private void downTxtFile() {
        progressDialog.setMessage("加载中...");
        progressDialog.show();
        ViseHttp.DOWNLOAD(pdfurl)
                .baseUrl(ApiConfig.APP_URL)
                .setFileName(fileNmae)
                .setRootName(fielPath)
                .request(new ACallback<DownProgress>() {
                    @Override
                    public void onSuccess(DownProgress downProgress) {
                        Log.i(TAG, "onSuccess: txt文件下载成功");
                        progressDialog.dismiss();
                        File file = new File(fielPath + "/download", fileNmae);
                        if (file.exists()) {
                            Log.i(TAG, "onSuccess: file is  exists  fielPath= " + file.getPath());
                            loadTxtFile(file.getPath());
                        } else {
                            Log.i(TAG, "onSuccess: file is not exists");
                        }
                    }


                    @Override
                    public void onFail(int errCode, String errMsg) {
                        Log.i(TAG, "downTxtFile onFail: errCode=" + errCode + " errMsg=" + errMsg);
                        progressDialog.dismiss();
                        if (errCode == 1006) {
                            ToastUtil.show("请求超时");
                        } else {
                            ToastUtil.show("网络请求超时");
                        }


                    }
                });
    }


    private void downPDFFile() {
        showDialog();
        ViseHttp.DOWNLOAD(pdfurl)
                .tag(TAG)
                .baseUrl(ApiConfig.APP_URL)
                .setFileName(fileNmae)
                .setRootName(fielPath)
                .request(new ACallback<DownProgress>() {
                    @Override
                    public void onSuccess(DownProgress downProgress) {
                        if (!isDestroy) {
                            if (downProgress == null || downProgress.getPercent() == null) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ToastUtil.show("下载出错，请重新下载");
                                        progressDialog.dismiss();
                                        deleteUnFinishFile();
                                        finish();
                                    }
                                });
                                return;
                            }
                            String percent = downProgress.getPercent();

                            long bytes_downloaded = downProgress.getDownloadSize();
                            long bytes_total = downProgress.getTotalSize();
                            int progress = (int) ((bytes_downloaded * 100) / bytes_total);

                            progressDialog.setProgress(progress);

                            if (percent != null && percent.equals("100.00%")) {
                                isDownLoadFinish = true;
                                progressDialog.dismiss();
                                Toast.makeText(BookReadActivity.this, "下载完成", Toast.LENGTH_SHORT).show();
                                File file = new File(fielPath + "/download", fileNmae);
                                if (file.exists()) {
                                    Log.i(TAG, "downPDFFile onSuccess: file is exists");
                                    openPDFDocument(file);
                                } else {
                                    Log.i(TAG, " downPDFFile onSuccess: file is not exists");
                                }
                            }
                        }


                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {
                        Log.i(TAG, "downPDFFile onFail: errCode=" + errCode + " errMsg=" + errMsg);
                    }
                });
    }


    private void openPDFDocument(File file) {
        if (!isLand){
            pdfView.fromFile(file)
                    .defaultPage(0)
                    .scrollHandle(new DefaultScrollHandle(this))
                    .spacing(0) // in dp
                    .pageFitPolicy(FitPolicy.BOTH)
                    .swipeHorizontal(true)
                    .enableAntialiasing(true)
                    .fitEachPage(true)
                    .autoSpacing(true)
                    .pageFling(true)
                    .load();
        }else {
            pdfView.fromFile(file)
                    .defaultPage(0)
                    .pageFitPolicy(FitPolicy.HEIGHT)
                    .enableSwipe(true) // allows to block changing pages using swipe //允许阻止改变页面使用刷卡
                    .swipeHorizontal(true)
                    .enableDoubletap(true)
                    .fitEachPage(true)
                    .enableAnnotationRendering(true)
                    .load();
        }

    }

    private void loadTxtFile(String FilePath) {
        HwTxtPlayActivity.loadTxtFile(this, FilePath);
        finish();
       /* mTxtReaderView.loadTxtFile(FilePath, new ILoadListener() {
            @Override
            public void onSuccess() {
                //加载成功回调
            }

            @Override
            public void onFail(TxtMsg txtMsg) {
                //加载失败回调
            }

            @Override
            public void onMessage(String message) {
                //加载过程信息回调
            }
        });*/

    }


    public void showDialog() {

        // 设置对话框参数
        progressDialog.setIcon(R.mipmap.ic_launcher);
        //progressDialog.setTitle("软件下载");
        progressDialog.setMessage("下载中...");
        progressDialog.setCancelable(false);
        // 设置进度条参数
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(100);
        // progressDialog.incrementProgressBy(0);
        progressDialog.setIndeterminate(false); // 填false表示是明确显示进度的 填true表示不是明确显示进度的
       /* progressDialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });*/
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                deleteUnFinishFile();
                finish();
            }
        });
        progressDialog.show();
    }

    private void deleteUnFinishFile() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isDownLoadFinish != true) {
                    File file = new File(fielPath + "/download/" + fileNmae);
                    if (file.exists()){
                        boolean delete = file.delete();
                    }
                }
            }
        }).start();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
        isDestroy = true;
//        ViseHttp.cancelTag(TAG);
    }
}

package com.example.wenqujingdian.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.adapter.PaintListAdapter;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.bean.BookDetailBean;
import com.example.wenqujingdian.bean.PaintBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.vise.xsnow.http.ViseHttp;
import com.vise.xsnow.http.callback.ACallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaintActivity extends BaseActivity {
    private static final String TAG = "BookActivity";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.refresh)
    SmartRefreshLayout smartRefresh;

    private PaintListAdapter mAdapter;
    private ProgressDialog progressDialog;
    private List<PaintBean.DataBean.ListBean> bookList;
    private boolean isRefresh;
    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);
        ButterKnife.bind(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText("艺术");

        progressDialog = new ProgressDialog(this);
        initView();
        initData();
    }

    private void initView() {
        GridLayoutManager manager;
        if (findViewById(R.id.tv_book2_land) != null) {
            //横屏显示
            manager = new GridLayoutManager(this, 5);
        } else {
            //竖屏显示
            manager = new GridLayoutManager(this, 3);
        }
        recyclerView.setLayoutManager(manager);
        mAdapter = new PaintListAdapter(this, null);
        mAdapter.openLoadAnimation();
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                PaintBean.DataBean.ListBean item = mAdapter.getItem(position);
                BookDetailBean bookDetailBean = new BookDetailBean();
                Intent intent = new Intent(PaintActivity.this, PaintDetailActivity.class);
                bookDetailBean.id = String.valueOf(item.getPaintingId());
                bookDetailBean.news_tit = item.getShortName();
                bookDetailBean.author = item.getAuthor();
                bookDetailBean.spic = item.getCover();
                intent.putExtra("bookDetail", bookDetailBean);
                startActivity(intent);
            }
        });

    }


    private void initData() {
        progressDialog.setMessage("加载中...");
        progressDialog.show();
//        final long time1 = System.currentTimeMillis();
        setRefresh();
        bookList = new ArrayList<>();
        getBookListData(0);
    }

    private void setRefresh() {
        smartRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                autoRefresh();
                refreshLayout.finishRefresh(1000);
            }
        });
        smartRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMore();
                refreshLayout.finishLoadMore(1000);
            }
        });
    }

    private void loadMore() {
        isRefresh = false;
        currentPage++;
        getBookListData(currentPage);
    }

    private void autoRefresh() {
        isRefresh = true;
        currentPage = 0;
        getBookListData(currentPage);
    }

    //获取图书列表数据
    private void getBookListData(final int pageIndex) {
        ViseHttp.GET(String.format(ApiConfig.PAINT_URL, pageIndex))///90
                .tag(TAG)
                .baseUrl(ApiConfig.ORG_URL)
                .setHttpCache(true)
                //配置读取超时时间，单位秒
                .readTimeOut(60)
                //配置写入超时时间，单位秒
                .writeTimeOut(60)
                //配置连接超时时间，单位秒
                .connectTimeOut(60)
                //配置请求失败重试次数
                .retryCount(3)
                //配置请求失败重试间隔时间，单位毫秒
                .retryDelayMillis(1000)
                .request(new ACallback<PaintBean>() {
                    @Override
                    public void onSuccess(PaintBean bookListBeans) {
                        progressDialog.dismiss();
                        List<PaintBean.DataBean> data = bookListBeans.getData();
                        if (data == null) return;
                        PaintBean.DataBean dataBean = data.get(0);
                        if (dataBean == null) return;
                        if (isRefresh) {
                            bookList = dataBean.getList();
                            mAdapter.replaceData(bookList);
                        } else {
                            bookList.addAll(dataBean.getList());
                            mAdapter.addData(dataBean.getList());
                        }

                    }

                    @Override
                    public void onFail(int errCode, String errMsg) {
                        progressDialog.dismiss();
                        //请求失败，errCode为错误码，errMsg为错误描述
                        Log.i(TAG, "onFail: errCode" + " errMsg=" + errMsg);
                    }
                });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ViseHttp.cancelTag(TAG);
    }
}

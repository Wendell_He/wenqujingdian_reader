package com.example.wenqujingdian.api;

/**
 * Created by 29083 on 2018/3/28.
 */

public class ApiConfig {
    //每页显示的item的个数
    private static final int pageSize = 50;
    /**
     * 纬度
     */
    public static String LATITUDE = "";
    /**
     * 经度
     */
    public static String LONGITUDE = "";
    //新服务url
    public static final String SERVER_URL = "http://gms.wenqujingdian.com/";
    //检查更新的URL
    public static final String UPDATE_URL = "http://gms.wenqujingdian.com/";
    public static final String ORG_URL = "http://service.wenqujingdian.com/";
    public static final String APP_URL = "http://www.wenqujingdian.com/";

    public static final String API_URL = "http://www.wenqujingdian.com";
    //检查更新的URL
    //云图书
    public static final String YUN_BOOK_LIST = "wqjd/app/GetBookList?searchVal=%s&pageIndex=%d&pageSize=" + pageSize;
    //云图书分类
    public static final String YUN_BOOK_CLASS = "wqjd/app/GetBookClassList";
    //云图书详情
    public static final String YUN_BOOK_DETAIL = "wqjd/app/GetBookDetails?bookId=%d";
    //图书分类详情
    public static final String YUN_CLASS_DETAIL = "wqjd/app/GetBookList?classId=%1d&pageIndex=%2d&pageSize=" + pageSize;
    //推荐图书分类
    public static final String TUIJIAN_BOOK = "wqjd/app/GetBookList?classId=%1d&pageIndex=%2d&pageSize=" + pageSize;

    //推荐图书
    public static final String RECOMMEND_BOOK = "wqjd/app/GetBlurbBookList";
    //机构
    public static final String ORG_INFO = "wqjd/app/GetOrgInfo";
    //退出应用
    public static final String ORG_LOGOUT = "wqjd/app/OrgLogOut";

    //视频分类
    public static final String VIDEO_CLASS_URL = "wqjd/app/GetVideoClassList";
    //获取视频
    public static final String VIDEO_MORE_URL = "wqjd/app/GetVideoListByPage?classId=%1d&pageIndex=%2d&pageSize=" + pageSize;
    //分类列表子目录
    public static final String VIDEO_CHILD_URL = "wqjd/app/GetVideoChildList?parentId=%1d&pageIndex=%2d&pageSize=" + pageSize;
    //五个图书模块
    public static final String MODUL_URL = "wqjd/app/GetBookList?classId=%1d&pageIndex=%2d&pageSize=" + pageSize;
    //音频分类
    public static final String AUDIO_CLASS_URL = "wqjd/app/GetAudioClassList";
    //音频分类列表子目录
    public static final String AUDIO_CHILD_URL = "wqjd/app/GetAudioChildList?parentId=%1d&pageIndex=%2d&pageSize=" + pageSize;
    //更多音频
    public static final String AUDIO_MORE_URL = "wqjd/app/GetAudioListByPage?classId=%1d&pageIndex=%2d&pageSize=" + pageSize;
    /**
     * 报纸首页
     */
    public static final String PAPER_HOME_URL = "wqjd/app/GetNewsPaperClassList";

    public static final String PAINT_URL = "wqjd/app/GetPaintingList?pageIndex=%d&pageSize= " + pageSize;

    /**
     * 两学一做网站
     */
    public static final String POLICY_URL = "http://cpc.people.com.cn/xuexi/";



}

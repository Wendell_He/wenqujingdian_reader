package com.example.wenqujingdian.api;

import com.example.wenqujingdian.bean.FileBean;
import com.example.wenqujingdian.model.AudioDetailBean;
import com.example.wenqujingdian.model.AudioLocalDetailBean;
import com.example.wenqujingdian.model.VideoDetailBean;
import com.example.wenqujingdian.model.VideoLocalDetailBean;

import java.util.List;

/**
 * Create by kxliu on 2018/11/21
 */
public class Contants {
    /**
     * SD卡目录
     */
    public static String SPICIFYPATH = "";

    /**
     * 外置存储设备根目录中的文件夹（存放本地json数据）
     */
    public final static String ROOTDIR = "offline.wenqujingdian.com";
    /**
     * 获取机构的数据
     */
    public final static String ORG_JSON = "org.json";

    public final static String SETTINGS_JSON = "settings.json";

    public final static int PERMISSION_STORAGE = 12;
    public final static int PERMISSION_MAP = 13;

    /**
     * 存放json数据
     */
    public final static String CONFIG = "config";
    /**
     * 本地图书数据
     */
    public final static String LOCAL_BOOK_JSON = "book.json";
    /**
     * 本地图书分类数据
     */
    public final static String BOOK_CLASS_JSON = "bookclass.json";
    /**
     * 风采展示文件
     */
    public final static String IMAGE = "picture";
    public final static String VIDEO = "video";
    public final static String FILE = "file";
    /**
     * 党政专题	book65.json
     * 健康养生	book66.json
     * 创业创新	book67.json
     * 国学专题	book79.json
     * 新教育   book80.json
     */
    public final static String DANGZHENG = "book65.json";
    public final static String JIANGKANG = "book66.json";
    public final static String CHUANGYE = "book67.json";
    public final static String GUOXUE = "book79.json";
    public final static String XINJIAOYU = "book80.json";

    public final static String VIDEOCLASS = "videoclass.json";
    public final static String VIDEOCHILD = "videochild.json";
    public final static String VIDEOLIST = "videolist.json";

    public final static String AUDIOCHILD = "audiochild.json";
    public final static String AUDIOCLASS = "audioclass.json";
    public final static String AUDIOLIST = "audiolist.json";

    public static List<VideoDetailBean.DataBean.ListBean> videoBean;
    public static List<VideoLocalDetailBean> LocalVideoBean;

    public static List<AudioDetailBean.DataBean.ListBean> playAudioList;
    public static List<AudioLocalDetailBean> localPlayAudioList;

    public static List<FileBean> playBeans;
}

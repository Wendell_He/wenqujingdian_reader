package com.example.wenqujingdian.paperAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.bean.PaperHomeBean;

import java.util.List;


public class HomeItemAdapter extends BaseAdapter {

    private Context context;
    private List<PaperHomeBean.DataBean.ListBean> listBeans;

    public HomeItemAdapter(Context context, List<PaperHomeBean.DataBean.ListBean> listBeans) {
        this.context = context;
        this.listBeans = listBeans;
    }


    @Override
    public int getCount() {
        if (listBeans != null) {
            return listBeans.size();
        } else {
            return 10;
        }
    }

    @Override
    public Object getItem(int position) {
        return listBeans.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PaperHomeBean.DataBean.ListBean subcategory = listBeans.get(position);
        ViewHold viewHold = null;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_home_category, null);
            viewHold = new ViewHold();
            viewHold.tv_name = (TextView) convertView.findViewById(R.id.item_home_name);
            viewHold.iv_icon = (ImageView) convertView.findViewById(R.id.item_image);
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
        }
        viewHold.tv_name.setText(subcategory.getPaperTitle());
        Glide.with(context)
                .load(ApiConfig.APP_URL + subcategory.getCover())
                .error(R.drawable.ic_default_image)
                .placeholder(R.drawable.ic_default_image)
                .into(viewHold.iv_icon);
        return convertView;


    }

    private static class ViewHold {
        private TextView tv_name;
        private ImageView iv_icon;
    }

}

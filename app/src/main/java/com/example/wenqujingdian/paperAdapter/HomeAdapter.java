package com.example.wenqujingdian.paperAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.wenqujingdian.GridViewForScrollView;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.activity.WebViewActivity;
import com.example.wenqujingdian.bean.PaperHomeBean;

import java.util.List;

public class HomeAdapter extends BaseAdapter/* implements View.OnClickListener*/ {

    private Context context;
    private List<PaperHomeBean.DataBean> listDatas;
//    private OnTitleItemClickListener listener;

    public HomeAdapter(Context context, List<PaperHomeBean.DataBean> listDatas) {
        this.context = context;
        this.listDatas = listDatas;
    }

    @Override
    public int getCount() {
        if (listDatas != null) {
            return listDatas.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return listDatas.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PaperHomeBean.DataBean dataBean = listDatas.get(position);
        final List<PaperHomeBean.DataBean.ListBean> dataList = dataBean.getList();
        ViewHold viewHold = null;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_home, null);
            viewHold = new ViewHold();
            viewHold.gridView = (GridViewForScrollView) convertView.findViewById(R.id.gridView);
            viewHold.blank = (TextView) convertView.findViewById(R.id.blank);
//            viewHold.more = ((TextView) convertView.findViewById(R.id.more));

            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
        }
        HomeItemAdapter adapter = new HomeItemAdapter(context, dataList);
        viewHold.blank.setText(dataBean.getClassName());
//        viewHold.more.setTag(position);
//        viewHold.more.setOnClickListener(this);
        viewHold.gridView.setAdapter(adapter);
        viewHold.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context,WebViewActivity.class);
                intent.putExtra("url",dataList.get(position).getPaperUrl());
                intent.putExtra("title",dataList.get(position).getPaperTitle());
                context.startActivity(intent);
            }
        });
        return convertView;
    }

//    @Override
//    public void onClick(View v) {
//        int tag = (int) v.getTag();
//        Toast.makeText(context,listDatas.get(tag).getClassName(),Toast.LENGTH_SHORT).show();
//    }

    private static class ViewHold {
        private GridViewForScrollView gridView;
        private TextView blank/*,more*/;
    }
}

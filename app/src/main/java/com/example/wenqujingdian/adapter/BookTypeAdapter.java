package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.bean.BookSortBean;

import java.util.List;

/**
 * Created by 29083 on 2018/3/28.
 */

public class BookTypeAdapter extends BaseQuickAdapter<BookSortBean.DataBean, BaseViewHolder> {

    private Context context;

    public BookTypeAdapter(Context context, @Nullable List<BookSortBean.DataBean> data) {
        super(R.layout.item_book_sort, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, BookSortBean.DataBean item) {

      /*  Glide.with(context)
                .load(ApiConfig.APP_URL + item.spic)
                .error(R.mipmap.logo)
                .placeholder(R.mipmap.logo)
                .centerCrop()
                .into((ImageView) helper.getView(R.id.image));
*/
        helper.setText(R.id.book_name, item.getClassName());

    }
}


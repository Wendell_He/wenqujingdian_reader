package com.example.wenqujingdian.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.model.AudioDetailBean;

import java.util.List;

/**
 * Create by kxliu on 2018/12/5
 */
public class AudioDetailAdapter extends BaseQuickAdapter<AudioDetailBean.DataBean.ListBean, BaseViewHolder> {

    public AudioDetailAdapter(@Nullable List<AudioDetailBean.DataBean.ListBean> data) {
        super(R.layout.autio_list_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AudioDetailBean.DataBean.ListBean item) {

        helper.setText(R.id.book_name, item.getAudioTitle());
        helper.addOnClickListener(R.id.tv_read);
    }
}

package com.example.wenqujingdian.adapter;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.model.VideoSectionChildBean;
import com.example.wenqujingdian.model.VideoSectionBean;

import java.util.List;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class SectionAdapter extends BaseSectionQuickAdapter<VideoSectionBean, BaseViewHolder> {

    public SectionAdapter(int layoutResId, int sectionHeadResId, List data) {
        super(layoutResId, sectionHeadResId, data);
    }

    @Override
    protected void convertHead(BaseViewHolder helper, final VideoSectionBean item) {
        helper.setText(R.id.header, item.header);
        helper.setVisible(R.id.more, item.isMore());
//        helper.addOnClickListener(R.id.more);
        helper.getView(R.id.item_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headerCallBack.headerClick(item.getId(), item.header);
            }
        });
    }


    @Override
    protected void convert(BaseViewHolder helper, final VideoSectionBean item) {
        final VideoSectionChildBean video = (VideoSectionChildBean) item.t;
        Glide.with(mContext)
                .load(video.getImg())
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
                .centerCrop()
                .into((ImageView) helper.getView(R.id.image));
        helper.setText(R.id.book_name, video.getName());
    }

    private HeaderCallBack headerCallBack;

    public void setHeaderCallBack(HeaderCallBack headerCallBack) {
        this.headerCallBack = headerCallBack;
    }

    public interface HeaderCallBack {
        void headerClick(String id, String title);
    }
}

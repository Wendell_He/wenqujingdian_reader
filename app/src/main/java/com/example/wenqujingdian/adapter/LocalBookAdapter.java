package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.model.BooksModel;

import java.util.List;

/**
 * Created by 29083 on 2018/3/28.
 */

public class LocalBookAdapter extends BaseQuickAdapter<BooksModel.BooksBean, BaseViewHolder> {

    private Context context;
    private String path;

    public LocalBookAdapter(Context context, @Nullable List<BooksModel.BooksBean> data,String path) {
        super(R.layout.item_book_list, data);
        this.context = context;
        this.path = path;
    }

    @Override
    protected void convert(BaseViewHolder helper, BooksModel.BooksBean item) {
        String coverpath = path + item.getCover();
        Glide.with(context)
                .load(coverpath)
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
                .centerCrop()
                .into((ImageView) helper.getView(R.id.image));

        helper.setText(R.id.book_name, item.getBookTitle());//  .setText(R.id.author, item.author)

    }
}


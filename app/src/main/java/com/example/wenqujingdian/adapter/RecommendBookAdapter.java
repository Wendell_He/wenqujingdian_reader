package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.model.LocalBookBean;
import com.example.wenqujingdian.utils.NetWorkUtils;

import java.util.List;

/**
 * Create by kxliu on 2018/11/24
 */
public class RecommendBookAdapter extends BaseQuickAdapter<LocalBookBean,BaseViewHolder> {
//    private String path;
    private Context context;
    public RecommendBookAdapter(@Nullable List<LocalBookBean> data,/* String path,*/ Context context) {
        super(R.layout.list_view_item, data);
//        this.path = path;
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, LocalBookBean item) {
        helper.setText(R.id.book_title,item.getBookTitle());
        if (NetWorkUtils.isNetworkAvailable(context)){
            Glide.with(context)
                    .load(ApiConfig.APP_URL + item.getCover())
                    .error(R.mipmap.ic_default_image)
                    .placeholder(R.mipmap.ic_default_image)
//                    .centerCrop()
                    .into((ImageView) helper.getView(R.id.book_image));
        }else {
            String coverpath = Contants.SPICIFYPATH + item.getCover();
            Glide.with(context)
                    .load(coverpath)
                    .error(R.mipmap.ic_default_image)
                    .placeholder(R.mipmap.ic_default_image)
//                    .centerCrop()
                    .into((ImageView) helper.getView(R.id.book_image));
        }

    }
}

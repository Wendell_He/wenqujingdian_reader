package com.example.wenqujingdian.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.model.VideoLocalDetailBean;

import java.util.List;

/**
 * Create by kxliu on 2018/12/5
 */
public class VideoLocalDetailAdapter extends BaseQuickAdapter<VideoLocalDetailBean, BaseViewHolder> {

    public VideoLocalDetailAdapter(@Nullable List<VideoLocalDetailBean> data) {
        super(R.layout.video_list_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, VideoLocalDetailBean item) {

        helper.setText(R.id.book_name, item.getVideoTitle());
    }
}

package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.bean.BookListBean;

import java.util.List;

/**
 * Created by 29083 on 2018/3/28.
 */

public class BookListAdapter extends BaseQuickAdapter<BookListBean, BaseViewHolder> {

    private Context context;

    public BookListAdapter(Context context, @Nullable List<BookListBean> data) {
        super(R.layout.item_book_list2, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, BookListBean item) {

        Glide.with(context)
                .load(ApiConfig.APP_URL + item.spic)
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
                .centerCrop()
                .into((ImageView) helper.getView(R.id.image));

        //helper.addOnClickListener(R.id.btn_start_read);
        helper.setText(R.id.book_name, item.news_tit);
                /*.setText(R.id.author, "作者：" + item.author)*/

    }
}


package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.ApiConfig;
import com.example.wenqujingdian.model.BookModelBean;

import java.util.List;

/**
 * Created by 29083 on 2018/3/28.
 */

public class BookModelListAdapter extends BaseQuickAdapter<BookModelBean.DataBean.ListBean, BaseViewHolder> {

    private Context context;

    public BookModelListAdapter(Context context, @Nullable List<BookModelBean.DataBean.ListBean> data) {
        super(R.layout.item_book_list2, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, BookModelBean.DataBean.ListBean item) {

        Glide.with(context)
                .load(ApiConfig.API_URL + item.getCover())
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
//                .centerCrop()
                .into((ImageView) helper.getView(R.id.image));

        helper.setText(R.id.book_name, item.getBookTitle());

    }
}


package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.model.LocalBookBean;

import java.io.File;
import java.util.List;

/**
 * 功能模块跳转页面的适配器
 * Create by kxliu on 2018/11/22
 */
public class FunctionModelAdapter extends BaseQuickAdapter<LocalBookBean,BaseViewHolder> {

    private Context context;

//    private String path;

    public FunctionModelAdapter(@Nullable List<LocalBookBean> data,Context context) {
        super(R.layout.item_local_book, data);
        this.context = context;
//        this.path = path;
    }

    @Override
    protected void convert(BaseViewHolder helper, LocalBookBean item) {
        String coverpath = Contants.SPICIFYPATH + item.getCover();
        Glide.with(context)
                .load(new File(coverpath))
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
                .centerCrop()
                .into((ImageView) helper.getView(R.id.image));
        helper.setText(R.id.book_name,item.getBookTitle());
    }
}

package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.model.AudioLocalMoreBean;

import java.util.List;

/**
 * Created by 29083 on 2018/3/28.
 */

public class AudioLocalMoreAdapter extends BaseQuickAdapter<AudioLocalMoreBean, BaseViewHolder> {

    private Context context;
    private String path;

    public AudioLocalMoreAdapter(Context context, @Nullable List<AudioLocalMoreBean> data,String path) {
        super(R.layout.item_book_list2, data);
        this.context = context;
        this.path = path;
    }

    @Override
    protected void convert(BaseViewHolder helper, AudioLocalMoreBean item) {

        Glide.with(context)
                .load(path + item.getCover())
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
                .centerCrop()
                .into((ImageView) helper.getView(R.id.image));

        helper.setText(R.id.book_name, item.getAudioTitle());

    }
}


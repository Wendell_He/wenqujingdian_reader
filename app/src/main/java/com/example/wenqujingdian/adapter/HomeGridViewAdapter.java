package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.api.Contants;
import com.example.wenqujingdian.model.LocalOrgBean;
import com.example.wenqujingdian.utils.NetWorkUtils;

import java.io.File;
import java.util.List;

/**
 * Create by kxliu on 2018/11/22
 */
public class HomeGridViewAdapter extends BaseQuickAdapter<LocalOrgBean.ModuleListBean,BaseViewHolder> {

    private Context context;
//    private String path;

    public HomeGridViewAdapter(int layoutResId,Context context,/*String path,*/ @Nullable List<LocalOrgBean.ModuleListBean> data) {
        super(layoutResId, data);
        this.context = context;
//        this.path = path;
    }

    @Override
    protected void convert(BaseViewHolder helper, LocalOrgBean.ModuleListBean item) {
        helper.setText(R.id.item_tv,item.getModuleName());
        if (NetWorkUtils.isNetworkAvailable(context)){
            if (item.getModuleId() == 111){
                Glide.with(context)
                        .load(R.mipmap.tech7)
                        .error(R.mipmap.logo)
                        .placeholder(R.mipmap.logo)
                        .centerCrop()
                        .into((ImageView) helper.getView(R.id.item_icon));
            }else {
                Glide.with(context)
                        .load(item.getImgUrl())
                        .error(R.mipmap.logo)
                        .placeholder(R.mipmap.logo)
                        .centerCrop()
                        .into((ImageView) helper.getView(R.id.item_icon));
            }

        }else {
            Glide.with(context)
                    .load(new File(Contants.SPICIFYPATH + item.getImgUrl()))
                    .error(R.mipmap.logo)
                    .placeholder(R.mipmap.logo)
                    .centerCrop()
                    .into((ImageView) helper.getView(R.id.item_icon));
        }

    }
}

package com.example.wenqujingdian.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.model.AudioLocalDetailBean;

import java.util.List;

/**
 * Create by kxliu on 2018/12/5
 */
public class AudioLocalDetailAdapter extends BaseQuickAdapter<AudioLocalDetailBean, BaseViewHolder> {

    public AudioLocalDetailAdapter(@Nullable List<AudioLocalDetailBean> data) {
        super(R.layout.video_list_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AudioLocalDetailBean item) {

        helper.setText(R.id.book_name, item.getAudioTitle());
    }
}

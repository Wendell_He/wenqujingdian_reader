package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.bean.FileBean;

import java.util.List;

/**
 * Created by 29083 on 2018/3/28.
 */

public class CustomerShowAdapter extends BaseQuickAdapter<FileBean, BaseViewHolder> {

    private Context context;
    private int type;

    public CustomerShowAdapter(Context context, @Nullable List<FileBean> data) {
        super(R.layout.item_book_list2, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, FileBean item) {
//        Bitmap thumbnail = null;
//        if (type == 1){
//            Bitmap bitmap = BitmapFactory.decodeFile(item.getFilePath());
//            thumbnail = ThumbnailUtils.extractThumbnail(bitmap, 150, 150);
//        }else if (type == 2){
//            thumbnail = ThumbnailUtils.createVideoThumbnail(item.getFilePath(),MINI_KIND);
//        }
        if (item.getBitmap() != null){
            helper.itemView.findViewById(R.id.image).setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(item.getBitmap())
                    .error(R.mipmap.ic_default_image)
                    .placeholder(R.mipmap.ic_default_image)
                    .centerCrop()
                    .into((ImageView) helper.getView(R.id.image));

        }else {
            helper.itemView.findViewById(R.id.image).setVisibility(View.GONE);
        }


        helper.setText(R.id.book_name,item.getFileName());

    }

    public void setFileType(int type){
        this.type = type;
    }
}


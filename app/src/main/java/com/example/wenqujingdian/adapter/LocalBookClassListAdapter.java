package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.model.LocalBookBean;

import java.util.List;

/**
 * Created by 29083 on 2018/3/28.
 */

public class LocalBookClassListAdapter extends BaseQuickAdapter<LocalBookBean, BaseViewHolder> {

    private Context context;
    private String path;

    public LocalBookClassListAdapter(Context context, @Nullable List<LocalBookBean> data,String path) {
        super(R.layout.item_book_list2, data);
        this.context = context;
        this.path = path;
    }

    @Override
    protected void convert(BaseViewHolder helper, LocalBookBean item) {

        Glide.with(context)
                .load(path + item.getCover())
                .error(R.mipmap.ic_default_image)
                .placeholder(R.mipmap.ic_default_image)
                .centerCrop()
                .into((ImageView) helper.getView(R.id.image));

        //helper.addOnClickListener(R.id.btn_start_read);
        helper.setText(R.id.book_name, item.getBookTitle());
                /*.setText(R.id.author, "作者：" + item.author)*/

    }
}


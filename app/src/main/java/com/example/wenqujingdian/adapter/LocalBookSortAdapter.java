package com.example.wenqujingdian.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.wenqujingdian.R;
import com.example.wenqujingdian.model.BookClassBean;

import java.util.List;

/**
 * Created by 29083 on 2018/3/28.
 */

public class LocalBookSortAdapter extends BaseQuickAdapter<BookClassBean, BaseViewHolder> {

    private Context context;

    public LocalBookSortAdapter(Context context, @Nullable List<BookClassBean> data) {
        super(R.layout.item_book_sort, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, BookClassBean item) {

        helper.setText(R.id.book_name, item.getClassName());

    }
}


package com.example.wenqujingdian.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.wenqujingdian.R;

import java.lang.ref.WeakReference;

/**
 * Description: Dialog View的辅助处理类
 */
class DialogViewHelper {

    private View mContentView = null;
    // 防止霸气侧漏
    private SparseArray<WeakReference<View>> mViews;

    private Context context;

    public DialogViewHelper(Context context, int layoutResId) {
        this();
        this.context = context;
        mContentView = LayoutInflater.from(context).inflate(layoutResId, null);
//        ViewCalculateUtils.setLinearLayoutParams(mContentView,
//                LinearLayout.LayoutParams.WRAP_CONTENT,400,
//                0,0,0,0,false);
    }


    public DialogViewHelper() {
        mViews = new SparseArray<>();
    }

    /**
     * 设置布局View
     *
     * @param contentView
     */
    public void setContentView(View contentView) {
        this.mContentView = contentView;
    }

    /**
     * 设置文本
     *
     * @param viewId
     * @param text
     */
    public void setText(int viewId, CharSequence text) {
        // 每次都 findViewById   减少findViewById的次数
        TextView tv = getView(viewId);
        if (tv != null) {
            tv.setText(text);
        }
    }

    public void setQrCode(int viewId, Bitmap bitmap){
        ImageView qrIv = getView(viewId);
//        ViewCalculateUtils.setFrameLayoutParams(qrIv, UiUtils.getInstance().getWidth(420),
//                UiUtils.getInstance().getHeight(420) ,0,0,0,0,true);
        if (qrIv != null){
            qrIv.setImageBitmap(bitmap);
        }
    }

    public void setBookCover(int viewId, String url) {
        ImageView book = getView(viewId);
        if (book != null) {
            Glide.with(context).load(url).error(R.drawable.ic_default_image)
                    .placeholder(R.drawable.ic_default_image).centerCrop().into(book);
        }
    }

    public <T extends View> T getView(int viewId) {
        WeakReference<View> viewReference = mViews.get(viewId);
        // 侧漏的问题  到时候到这个系统优化的时候再去介绍
        View view = null;
        if (viewReference != null) {
            view = viewReference.get();
        }

        if (view == null) {
            view = mContentView.findViewById(viewId);
            if (view != null) {
                mViews.put(viewId, new WeakReference<>(view));
            }
        }
        return (T) view;
    }

    /**
     * 设置点击事件
     *
     * @param viewId
     * @param listener
     */
    public void setOnclickListener(int viewId, View.OnClickListener listener) {
        View view = getView(viewId);
        if (view != null) {
            view.setOnClickListener(listener);
        }
    }

    /**
     * 获取ContentView
     *
     * @return
     */
    public View getContentView() {
        return mContentView;
    }
}

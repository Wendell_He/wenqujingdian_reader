package com.example.wenqujingdian.bean;

import java.util.List;

/**
 * Create by kxliu on 2019/10/6
 */
public class PaintBean {

    /**
     * result : true
     * msg :
     * data : [{"pageCount":160,"recordCount":799,"list":[{"PaintingId":1000,"ShortName":"世界名画建筑物001","FullName":"世界名画建筑物001","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231120_41404.jpg","CompletionDate":null,"SortCode":1000,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:20","ROWSTAT":null},{"PaintingId":1001,"ShortName":"世界名画建筑物002","FullName":"世界名画建筑物002","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231120_60840.jpg","CompletionDate":null,"SortCode":1001,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:20","ROWSTAT":null},{"PaintingId":1002,"ShortName":"世界名画建筑物003","FullName":"世界名画建筑物003","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231121_25113.jpg","CompletionDate":null,"SortCode":1002,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:21","ROWSTAT":null},{"PaintingId":1003,"ShortName":"世界名画建筑物004","FullName":"世界名画建筑物004","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231121_67123.jpg","CompletionDate":null,"SortCode":1003,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:21","ROWSTAT":null},{"PaintingId":1004,"ShortName":"世界名画建筑物005","FullName":"世界名画建筑物005","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231121_12257.jpg","CompletionDate":null,"SortCode":1004,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:21","ROWSTAT":null}]}]
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pageCount : 160
         * recordCount : 799
         * list : [{"PaintingId":1000,"ShortName":"世界名画建筑物001","FullName":"世界名画建筑物001","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231120_41404.jpg","CompletionDate":null,"SortCode":1000,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:20","ROWSTAT":null},{"PaintingId":1001,"ShortName":"世界名画建筑物002","FullName":"世界名画建筑物002","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231120_60840.jpg","CompletionDate":null,"SortCode":1001,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:20","ROWSTAT":null},{"PaintingId":1002,"ShortName":"世界名画建筑物003","FullName":"世界名画建筑物003","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231121_25113.jpg","CompletionDate":null,"SortCode":1002,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:21","ROWSTAT":null},{"PaintingId":1003,"ShortName":"世界名画建筑物004","FullName":"世界名画建筑物004","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231121_67123.jpg","CompletionDate":null,"SortCode":1003,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:21","ROWSTAT":null},{"PaintingId":1004,"ShortName":"世界名画建筑物005","FullName":"世界名画建筑物005","FullText":null,"Author":null,"Cover":"/Public/editor/attached/file/20190924/20190924231121_12257.jpg","CompletionDate":null,"SortCode":1004,"ParentId":0,"Childs":0,"CreateDate":"2019-09-24T23:11:21","ROWSTAT":null}]
         */

        private int pageCount;
        private int recordCount;
        private List<ListBean> list;

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * PaintingId : 1000
             * ShortName : 世界名画建筑物001
             * FullName : 世界名画建筑物001
             * FullText : null
             * Author : null
             * Cover : /Public/editor/attached/file/20190924/20190924231120_41404.jpg
             * CompletionDate : null
             * SortCode : 1000
             * ParentId : 0
             * Childs : 0
             * CreateDate : 2019-09-24T23:11:20
             * ROWSTAT : null
             */

            private int PaintingId;
            private String ShortName;
            private String FullName;
            private Object FullText;
            private String Author;
            private String Cover;
            private Object CompletionDate;
            private int SortCode;
            private int ParentId;
            private int Childs;
            private String CreateDate;
            private Object ROWSTAT;

            public int getPaintingId() {
                return PaintingId;
            }

            public void setPaintingId(int PaintingId) {
                this.PaintingId = PaintingId;
            }

            public String getShortName() {
                return ShortName;
            }

            public void setShortName(String ShortName) {
                this.ShortName = ShortName;
            }

            public String getFullName() {
                return FullName;
            }

            public void setFullName(String FullName) {
                this.FullName = FullName;
            }

            public Object getFullText() {
                return FullText;
            }

            public void setFullText(Object FullText) {
                this.FullText = FullText;
            }

            public String getAuthor() {
                return Author;
            }

            public void setAuthor(String Author) {
                this.Author = Author;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }

            public Object getCompletionDate() {
                return CompletionDate;
            }

            public void setCompletionDate(Object CompletionDate) {
                this.CompletionDate = CompletionDate;
            }

            public int getSortCode() {
                return SortCode;
            }

            public void setSortCode(int SortCode) {
                this.SortCode = SortCode;
            }

            public int getParentId() {
                return ParentId;
            }

            public void setParentId(int ParentId) {
                this.ParentId = ParentId;
            }

            public int getChilds() {
                return Childs;
            }

            public void setChilds(int Childs) {
                this.Childs = Childs;
            }

            public String getCreateDate() {
                return CreateDate;
            }

            public void setCreateDate(String CreateDate) {
                this.CreateDate = CreateDate;
            }

            public Object getROWSTAT() {
                return ROWSTAT;
            }

            public void setROWSTAT(Object ROWSTAT) {
                this.ROWSTAT = ROWSTAT;
            }
        }
    }
}

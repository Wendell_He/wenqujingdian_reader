package com.example.wenqujingdian.bean;

/**
 * Created by 29083 on 2018/3/28.
 */

public class BookListBean {

    /**
     * author : 何凤娣编著
     * cbdate :
     * id : 76
     * jianjie :
     * news_djs : 0
     * news_source : 清华大学出版社
     * news_tit : 做自己的心理咨询师
     * spic :
     * tuij : 0
     * url : /Public/editor/attached/file/20180213/20180213230151_34120.pdf
     * url2 :
     * url3 :
     */

    public String author;//作者
    public String cbdate;
    public String id;
    public String jianjie;//简介
    public String news_djs;
    public String news_source;//出版社
    public String news_tit;//书名
    public String spic;//图片地址
    public String tuij;
    public String url;  //pdf 下载地址1 (一般先取这个)
    public String classId;//pdf 下载地址2
    public String url3;//pdf 下载地址3


}

package com.example.wenqujingdian.bean;

/**
 * Create by kxliu on 2020/1/4
 */
public class UpDateBean {

    /**
     * ResultCode : 0
     * Msg :
     * Total : 0
     * Data : {"update":"true","url":"/app/app-debug.apk","intro":"1、增加图书；2、解决bug；"}
     */

    private int ResultCode;
    private String Msg;
    private String Total;
    private DataBean Data;

    public int getResultCode() {
        return ResultCode;
    }

    public void setResultCode(int ResultCode) {
        this.ResultCode = ResultCode;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String Msg) {
        this.Msg = Msg;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String Total) {
        this.Total = Total;
    }

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * update : true
         * url : /app/app-debug.apk
         * intro : 1、增加图书；2、解决bug；
         */

        private boolean update;
        private String url;
        private String intro;

        public boolean getUpdate() {
            return update;
        }

        public void setUpdate(boolean update) {
            this.update = update;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "update=" + update +
                    ", url='" + url + '\'' +
                    ", intro='" + intro + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UpDateBean{" +
                "ResultCode=" + ResultCode +
                ", Msg='" + Msg + '\'' +
                ", Total='" + Total + '\'' +
                ", Data=" + Data +
                '}';
    }
}

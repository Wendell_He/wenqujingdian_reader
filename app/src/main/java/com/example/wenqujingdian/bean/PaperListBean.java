package com.example.wenqujingdian.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Create by kxliu on 2019/2/25
 */
public class PaperListBean implements Parcelable {

    /**
     * result : true
     * msg :
     * data : [{"pageCount":2,"recordCount":3,"list":[{"NewspaperId":48,"PaperTitle":"山西日报","PaperContent":"<a href=\"http://epaper.sxrb.com\" target=\"_blank\">山西日报<\/a>","PaperSummary":"","Hits":0,"Cover":"/Public/editor/attached/image/20180307/20180307103347_98851.jpg","PaperUrl":"http://epaper.sxrb.com","ROWSTAT":null},{"NewspaperId":110,"PaperTitle":"山西法制报","PaperContent":"","PaperSummary":"","Hits":0,"Cover":"/Public/editor/attached/image/20180312/20180312113749_98972.jpg","PaperUrl":"http://epaper.sxrb.com/index_sxfzb.shtml","ROWSTAT":null}]}]
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    protected PaperListBean(Parcel in) {
        result = in.readByte() != 0;
        msg = in.readString();
    }

    public static final Creator<PaperListBean> CREATOR = new Creator<PaperListBean>() {
        @Override
        public PaperListBean createFromParcel(Parcel in) {
            return new PaperListBean(in);
        }

        @Override
        public PaperListBean[] newArray(int size) {
            return new PaperListBean[size];
        }
    };

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (result ? 1 : 0));
        dest.writeString(msg);
    }

    public static class DataBean implements Parcelable{
        /**
         * pageCount : 2
         * recordCount : 3
         * list : [{"NewspaperId":48,"PaperTitle":"山西日报","PaperContent":"<a href=\"http://epaper.sxrb.com\" target=\"_blank\">山西日报<\/a>","PaperSummary":"","Hits":0,"Cover":"/Public/editor/attached/image/20180307/20180307103347_98851.jpg","PaperUrl":"http://epaper.sxrb.com","ROWSTAT":null},{"NewspaperId":110,"PaperTitle":"山西法制报","PaperContent":"","PaperSummary":"","Hits":0,"Cover":"/Public/editor/attached/image/20180312/20180312113749_98972.jpg","PaperUrl":"http://epaper.sxrb.com/index_sxfzb.shtml","ROWSTAT":null}]
         */

        private int pageCount;
        private int recordCount;
        private List<ListBean> list;

        protected DataBean(Parcel in) {
            pageCount = in.readInt();
            recordCount = in.readInt();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public int getRecordCount() {
            return recordCount;
        }

        public void setRecordCount(int recordCount) {
            this.recordCount = recordCount;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(pageCount);
            dest.writeInt(recordCount);
        }

        public static class ListBean implements Parcelable{
            /**
             * NewspaperId : 48
             * PaperTitle : 山西日报
             * PaperContent : <a href="http://epaper.sxrb.com" target="_blank">山西日报</a>
             * PaperSummary :
             * Hits : 0
             * Cover : /Public/editor/attached/image/20180307/20180307103347_98851.jpg
             * PaperUrl : http://epaper.sxrb.com
             */

            private int NewspaperId;
            private String PaperTitle;
            private String PaperContent;
            private String PaperSummary;
            private int Hits;
            private String Cover;
            private String PaperUrl;

            protected ListBean(Parcel in) {
                NewspaperId = in.readInt();
                PaperTitle = in.readString();
                PaperContent = in.readString();
                PaperSummary = in.readString();
                Hits = in.readInt();
                Cover = in.readString();
                PaperUrl = in.readString();
            }

            public static final Creator<ListBean> CREATOR = new Creator<ListBean>() {
                @Override
                public ListBean createFromParcel(Parcel in) {
                    return new ListBean(in);
                }

                @Override
                public ListBean[] newArray(int size) {
                    return new ListBean[size];
                }
            };

            public int getNewspaperId() {
                return NewspaperId;
            }

            public void setNewspaperId(int NewspaperId) {
                this.NewspaperId = NewspaperId;
            }

            public String getPaperTitle() {
                return PaperTitle;
            }

            public void setPaperTitle(String PaperTitle) {
                this.PaperTitle = PaperTitle;
            }

            public String getPaperContent() {
                return PaperContent;
            }

            public void setPaperContent(String PaperContent) {
                this.PaperContent = PaperContent;
            }

            public String getPaperSummary() {
                return PaperSummary;
            }

            public void setPaperSummary(String PaperSummary) {
                this.PaperSummary = PaperSummary;
            }

            public int getHits() {
                return Hits;
            }

            public void setHits(int Hits) {
                this.Hits = Hits;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }

            public String getPaperUrl() {
                return PaperUrl;
            }

            public void setPaperUrl(String PaperUrl) {
                this.PaperUrl = PaperUrl;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(NewspaperId);
                dest.writeString(PaperTitle);
                dest.writeString(PaperContent);
                dest.writeString(PaperSummary);
                dest.writeInt(Hits);
                dest.writeString(Cover);
                dest.writeString(PaperUrl);
            }
        }
    }
}

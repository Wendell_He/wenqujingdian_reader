package com.example.wenqujingdian.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 29083 on 2018/4/7.
 */

public class BookDetailBean implements Parcelable {

    public String author;//作者
    public String cbdate;
    public String id;
    public String jianjie;//简介
    public String news_djs;
    public String news_source;//出版社
    public String news_tit;//书名
    public String spic;//图片地址
    public String tuij;
    public String url;  //pdf 下载地址1 (一般先取这个)
    public String url2;//pdf 下载地址2
    public String url3;//pdf 下载地址3
    public String classId;


    public BookDetailBean() {

    }

    protected BookDetailBean(Parcel in) {
        author = in.readString();
        cbdate = in.readString();
        id = in.readString();
        jianjie = in.readString();
        news_djs = in.readString();
        news_source = in.readString();
        news_tit = in.readString();
        spic = in.readString();
        tuij = in.readString();
        url = in.readString();
        url2 = in.readString();
        url3 = in.readString();
        classId = in.readString();
    }

    public static final Creator<BookDetailBean> CREATOR = new Creator<BookDetailBean>() {
        @Override
        public BookDetailBean createFromParcel(Parcel in) {
            return new BookDetailBean(in);
        }

        @Override
        public BookDetailBean[] newArray(int size) {
            return new BookDetailBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(author);
        dest.writeString(cbdate);
        dest.writeString(id);
        dest.writeString(jianjie);
        dest.writeString(news_djs);
        dest.writeString(news_source);
        dest.writeString(news_tit);
        dest.writeString(spic);
        dest.writeString(tuij);
        dest.writeString(url);
        dest.writeString(url2);
        dest.writeString(url3);
        dest.writeString(classId);
    }
}

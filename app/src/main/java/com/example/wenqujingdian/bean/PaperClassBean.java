package com.example.wenqujingdian.bean;

import java.util.List;

/**
 * Create by kxliu on 2019/2/25
 */
public class PaperClassBean {


    /**
     * data : [{"ClassId":12,"ClassName":"全国 中央","ParentId":4,"SortId":54},
     * {"ClassId":24,"ClassName":"广东 深圳 福建","ParentId":4,"SortId":55,"Remark":null,"Childs":{}},{"ClassId":62,"ClassName":"北京 天津 河北","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":436,"Remark":null,"Childs":{}},{"ClassId":63,"ClassName":"海外版、少数民族报","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":437,"Remark":null,"Childs":{}},{"ClassId":64,"ClassName":"辽宁 吉林 黑龙江","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":438,"Remark":null,"Childs":{}},{"ClassId":73,"ClassName":"山西 陕西 甘肃 青海","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":460,"Remark":null,"Childs":{}},{"ClassId":60,"ClassName":"湖南 湖北","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":461,"Remark":null,"Childs":{}},{"ClassId":74,"ClassName":"外语报、英语报","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":462,"Remark":null,"Childs":{}},{"ClassId":75,"ClassName":"上海 江苏 浙江","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":463,"Remark":null,"Childs":{}},{"ClassId":23,"ClassName":"重庆 四川 云南","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":465,"Remark":null,"Childs":{}},{"ClassId":77,"ClassName":"山东 河南 安徽","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":467,"Remark":null,"Childs":{}},{"ClassId":78,"ClassName":"江西 广西 海南 贵州","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":468,"Remark":null,"Childs":{}},{"ClassId":61,"ClassName":"西藏 新疆 内蒙古 宁夏","ParentId":4,"ClassCode":null,"ClassImg":null,"ClassType":null,"Cover":null,"SortId":470,"Remark":null,"Childs":{}}]
     * result : true
     * msg : success
     */

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ClassId : 12
         * ClassName : 全国 中央
         * ParentId : 4
         * SortId : 54
         */

        private int ClassId;
        private String ClassName;
        private int ParentId;
        private int SortId;

        public int getClassId() {
            return ClassId;
        }

        public void setClassId(int ClassId) {
            this.ClassId = ClassId;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String ClassName) {
            this.ClassName = ClassName;
        }

        public int getParentId() {
            return ParentId;
        }

        public void setParentId(int ParentId) {
            this.ParentId = ParentId;
        }

        public int getSortId() {
            return SortId;
        }

        public void setSortId(int SortId) {
            this.SortId = SortId;
        }
    }
}

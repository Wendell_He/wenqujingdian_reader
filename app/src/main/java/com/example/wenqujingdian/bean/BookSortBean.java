package com.example.wenqujingdian.bean;

import java.util.List;

/**
 * Created by 29083 on 2018/3/30.
 */

public class BookSortBean {

    private boolean result;
    private String msg;
    private List<DataBean> data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ClassId : 40
         * ClassName : 马克思主义、列宁主义、毛泽东思想、邓小平理论
         * ParentId : 1
         * ClassCode : null
         * ClassImg : null
         * ClassType : null
         * Cover : null
         * SortId : 86
         * Remark : null
         * BookList : [{"BookId":409,"BookTitle":"唯物史观的形成和发展史纲要","Publisher":"中央编译出版社","Author":"冯景源","Cover":"/Public/editor/attached/image/20180603/20180603200609_10991.jpg","BookUrl":"/Public/editor/attached/file/20180603/20180603200229_98893.pdf","ISBN":"9787511721341","PublishDate":"1905-07-06 16:48:00","Hits":8,"StoreUpCount":0},{"BookId":49176,"BookTitle":"\u201c马克思主义基本原理概论\u201d课案例式专题教学教师用书","Publisher":"中国人民大学出版社","Author":"梁山","Cover":"/Public/editor/attached/image/20180227/20180227124553_53398.jpg","BookUrl":"/Public/editor/attached/file/20180227/20180227124457_68710.pdf","ISBN":"9787531038566","PublishDate":"2008年11月","Hits":6,"StoreUpCount":0},{"BookId":21962,"BookTitle":"分析马克思","Publisher":"重庆大学出版社","Author":"张静","Cover":"/Public/editor/attached/image/20180403/20180403110651_47891.jpg","BookUrl":"/Public/editor/attached/file/20180403/20180403110659_28492.pdf","ISBN":"9787562480501","PublishDate":"2014-04-01","Hits":5,"StoreUpCount":0},{"BookId":11438,"BookTitle":"给你的生活减减肥 ","Publisher":"中国商业出版社","Author":"马银春编著 ","Cover":"/Public/editor/attached/image/20180811/20180811165056_74036.jpg","BookUrl":"/Public/editor/attached/file/20180811/20180811165038_14224.pdf","ISBN":"9787504475336","PublishDate":"2012-02-01","Hits":4,"StoreUpCount":0},{"BookId":2242,"BookTitle":"改变世界的哲学","Publisher":"吉林出版集团有限责任公司","Author":"张艳辉，魏月","Cover":"/Public/editor/attached/image/20180406/20180406111040_87228.jpg","BookUrl":"/Public/editor/attached/file/20180406/20180406111102_26182.pdf","ISBN":"9787553425832","PublishDate":"2013-09-01","Hits":3,"StoreUpCount":0}]
         * Childs : {}
         */

        private int ClassId;
        private String ClassName;
        private int ParentId;
        private int SortId;
        private List<BookListBean> BookList;

        public int getClassId() {
            return ClassId;
        }

        public void setClassId(int ClassId) {
            this.ClassId = ClassId;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String ClassName) {
            this.ClassName = ClassName;
        }

        public int getParentId() {
            return ParentId;
        }

        public void setParentId(int ParentId) {
            this.ParentId = ParentId;
        }

        public int getSortId() {
            return SortId;
        }

        public void setSortId(int SortId) {
            this.SortId = SortId;
        }

        public List<BookListBean> getBookList() {
            return BookList;
        }

        public void setBookList(List<BookListBean> BookList) {
            this.BookList = BookList;
        }

        public static class BookListBean {
            /**
             * BookId : 409
             * BookTitle : 唯物史观的形成和发展史纲要
             * Publisher : 中央编译出版社
             * Author : 冯景源
             * Cover : /Public/editor/attached/image/20180603/20180603200609_10991.jpg
             * BookUrl : /Public/editor/attached/file/20180603/20180603200229_98893.pdf
             * ISBN : 9787511721341
             * PublishDate : 1905-07-06 16:48:00
             * Hits : 8
             * StoreUpCount : 0
             */

            private int BookId;
            private String BookTitle;
            private String Publisher;
            private String Author;
            private String Cover;
            private String BookUrl;
            private String ISBN;
            private String PublishDate;
            private int Hits;
            private int StoreUpCount;

            public int getBookId() {
                return BookId;
            }

            public void setBookId(int BookId) {
                this.BookId = BookId;
            }

            public String getBookTitle() {
                return BookTitle;
            }

            public void setBookTitle(String BookTitle) {
                this.BookTitle = BookTitle;
            }

            public String getPublisher() {
                return Publisher;
            }

            public void setPublisher(String Publisher) {
                this.Publisher = Publisher;
            }

            public String getAuthor() {
                return Author;
            }

            public void setAuthor(String Author) {
                this.Author = Author;
            }

            public String getCover() {
                return Cover;
            }

            public void setCover(String Cover) {
                this.Cover = Cover;
            }

            public String getBookUrl() {
                return BookUrl;
            }

            public void setBookUrl(String BookUrl) {
                this.BookUrl = BookUrl;
            }

            public String getISBN() {
                return ISBN;
            }

            public void setISBN(String ISBN) {
                this.ISBN = ISBN;
            }

            public String getPublishDate() {
                return PublishDate;
            }

            public void setPublishDate(String PublishDate) {
                this.PublishDate = PublishDate;
            }

            public int getHits() {
                return Hits;
            }

            public void setHits(int Hits) {
                this.Hits = Hits;
            }

            public int getStoreUpCount() {
                return StoreUpCount;
            }

            public void setStoreUpCount(int StoreUpCount) {
                this.StoreUpCount = StoreUpCount;
            }
        }
    }
}





package com.example.wenqujingdian;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Administrator on 2018/4/26.
 */

public class CustomDialog extends Dialog{
    private Button cancel,open;
    private TextView title,message;
    private String titleStr,messageStr;
    private String confirmStr, cancelStr;
    private OnCansalLinstener cansalLinstener;
    private OnConfirmLinstener confirmLinstener;
    private boolean isShowTitle;
    private View line;
    private boolean hasCansalButton = true;

    public CustomDialog(@NonNull Context context) {
        super(context, R.style.dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);
        setCanceledOnTouchOutside(false);
        initView();
        initData();
        initEvent();
    }

    /**
     * 初始化界面的确定和取消监听器
     */
    private void initEvent() {
        //设置确定按钮被点击后，向外界提供监听
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmLinstener != null) {
                    confirmLinstener.onConfirmClick();
                }
            }
        });
        //设置取消按钮被点击后，向外界提供监听
        if (hasCansalButton) {
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cansalLinstener != null) {
                        cansalLinstener.onCancalClick();
                    }
                }
            });
        }
    }

    /**
     * 初始化界面控件的显示数据
     */
    private void initData() {
        //如果用户自定了title和message
        if (isShowTitle && title != null){
            title.setVisibility(View.VISIBLE);
            title.setText(titleStr);
        }
        if (messageStr != null) {
            message.setText(messageStr);
        }
        //如果设置按钮的文字
        if (confirmStr != null) {
            open.setText(confirmStr);
        }
        if (hasCansalButton && cancelStr != null) {
            cancel.setText(cancelStr);
        }
    }
    private void initView() {
        open = findViewById(R.id.dialog_open);
        cancel = findViewById(R.id.dialog_cancel);
        title = findViewById(R.id.title);
        message = findViewById(R.id.message);
        line = findViewById(R.id.dialog_line);
        if (!hasCansalButton){
            cancel.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
        }

    }

    public void setTitle(String titleStr,boolean visible) {
        this.titleStr = titleStr;
        isShowTitle = visible;
    }

    public void setMessage(String message) {
        messageStr = message;
    }

    public interface OnCansalLinstener{
        void onCancalClick();
    }

    public interface OnConfirmLinstener {
        void onConfirmClick();
    }

    public void setonCansalLinstener(String canselStr,OnCansalLinstener cansalLinstener){
        if (canselStr != null){
            this.cancelStr = canselStr;
        }
        this.cansalLinstener = cansalLinstener;
    }

    public void setonConfirmLinstener(String confirmStr, OnConfirmLinstener confirmLinstener) {
        if (confirmStr != null) {
            this.confirmStr = confirmStr;
        }
        this.confirmLinstener = confirmLinstener;
    }

    public void hasCansalButton(boolean hasCansalButton){
        this.hasCansalButton = hasCansalButton;
    }
}

package com.example.wenqujingdian;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Process;

import com.example.wenqujingdian.api.ApiConfig;
import com.vise.xsnow.http.ViseHttp;

import java.util.List;


/**
 * Created by 29083 on 2018/3/28.
 */

public class MyApplication extends Application {

    public static MyApplication mContext;

//    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        String processName = getProcessName(Process.myPid());
        if (!processName.endsWith("remote")){
            ViseHttp.init(this);
            //配置请求主机地址
            ViseHttp.CONFIG().baseUrl(ApiConfig.ORG_URL);
//            setupNetworkListener(true);

//            GreenDaoHelper helper = new GreenDaoHelper(this,"book.db",null);
//            DaoMaster daoMaster = new DaoMaster(helper.getWritableDb());
//            daoSession = daoMaster.newSession();
        }
    }

    private String getProcessName(int pid) {
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        if (am == null) return null;
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        if (processInfos == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
            if (processInfo.pid == pid) {
                return processInfo.processName;
            }
        }
        return null;
    }

//    private void setupNetworkListener(boolean on) {
//        if (on) {
//            NetWorkUtils.getInstance().startConnectionTrack(this);
//        } else {
//            NetWorkUtils.getInstance().stopConnectionTrack(this);
//        }
//    }
}
